package com.gennext.rwa.fragment.profile;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.rwa.MainActivity;
import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.model.AddressModel;
import com.gennext.rwa.model.JsonModel;
import com.gennext.rwa.model.SpinnerAdapter;
import com.gennext.rwa.util.AppAnimation;
import com.gennext.rwa.util.AppSettings;
import com.gennext.rwa.util.AppTokens;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.FieldValidation;
import com.gennext.rwa.util.GCMTokens;
import com.gennext.rwa.util.HttpReq;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.NotificationUtils;
import com.gennext.rwa.util.RWA;
import com.gennext.rwa.util.Utility;
import com.google.firebase.iid.FirebaseInstanceId;

import com.gennext.rwa.util.internet.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhijit on 13-Oct-16.
 */

public class UpdateProfile extends CompactFragment implements View.OnFocusChangeListener {
    private static final int TASK_UPDATE_PROFILE = 1, TASK_LOGIN = 2;
    private Spinner spColony;//spFloor
    private String[] profile = new String[7];
    private boolean[] check = {false, false, false, false, true};
    private AssignTask assignTask;
    private EditText etName, etEmail, etHouseNo, etFloor, etAddress;
    private Button btnSubmit;
    private ProgressBar progressBar;
    private PopulateSpinnerTask spinnerTask;
    private String sltColony;
    private String floor;
    private String Colony;
    private String address;
    private ArrayList<AddressModel> societyList;
    private String sltColonyId;
    private BroadcastReceiver mHandleMessageReceiver;

    public static UpdateProfile newInstance() {
        UpdateProfile fragment = new UpdateProfile();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
        if (spinnerTask != null) {
            spinnerTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
        if (spinnerTask != null) {
            spinnerTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_update_profile, container, false);
        setActionBarOptionCustom(v, "Update Profile");
        InitUI(v);
        return v;
    }

    public void setActionBarOptionCustom(View v, String Title) {
        LinearLayout ActionBack;
        ActionBack = (LinearLayout) v.findViewById(R.id.ll_actionbar_back);
        TextView tvTitle = (TextView) v.findViewById(R.id.actionbar_title);

        tvTitle.setText(Title);
        ActionBack.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View arg0) {
                getActivity().finish();
            }
        });

    }

    private void InitUI(View v) {
//        spFloor = (Spinner) v.findViewById(R.id.sp_update_profile_floor);
        etFloor = (EditText) v.findViewById(R.id.et_update_profile_floor);
        spColony = (Spinner) v.findViewById(R.id.sp_update_profile_colony);
        etName = (EditText) v.findViewById(R.id.et_update_profile_name);
        etEmail = (EditText) v.findViewById(R.id.et_update_profile_email);
        etHouseNo = (EditText) v.findViewById(R.id.et_update_profile_house_no);
        etAddress = (EditText) v.findViewById(R.id.et_update_profile_address);
        btnSubmit = (Button) v.findViewById(R.id.btn_update_profile);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);

        etName.setOnFocusChangeListener(this);
        etEmail.setOnFocusChangeListener(this);
        etHouseNo.setOnFocusChangeListener(this);
        etFloor.setOnFocusChangeListener(this);
        etAddress.setOnFocusChangeListener(this);

        profile = AppUser.getProfileData(getActivity());
        etName.setText(profile[1]);
        if (!profile[1].equals("")) {
            check[0] = true;
            etName.requestFocus();
        }
        etEmail.setText(profile[2]);
        if (!profile[2].equals("")) {
            check[1] = true;
            etEmail.requestFocus();
        }

        etHouseNo.setText(profile[3]);
        if (!profile[3].equals("")) {
            check[2] = true;
            etHouseNo.requestFocus();
        }

        floor = profile[4];
        Colony = profile[5];
        address = profile[6];
        etFloor.setText(floor != null ? floor : "");
        if (!floor.equals("")) {
            check[3] = true;
            etFloor.requestFocus();
        }
        etAddress.setText(address != null ? address : "");
        if (!address.equals("")) {
            check[4] = true;
//            etAddress.requestFocus();
        }

        spColony.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View arg1, int position, long arg3) {
                // TODO Auto-generated method stub
                if (!adapter.getSelectedItem().toString().equals("Select Society")) {
//                    sltColony = adapter.getSelectedItem().toString();
                    if (societyList != null) {
                        sltColony = societyList.get(position).getSociety();
                        sltColonyId = societyList.get(position).getSocietyId();
                    }
                    etName.requestFocus();
                    etEmail.requestFocus();
                    etHouseNo.requestFocus();
                    etFloor.requestFocus();
//                    etAddress.requestFocus();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etName.requestFocus();
                etEmail.requestFocus();
                etHouseNo.requestFocus();
                etFloor.requestFocus();
                hideKeybord(getActivity());
                executeTask(TASK_UPDATE_PROFILE);
            }
        });

        spinnerTask = new PopulateSpinnerTask(getActivity());
        spinnerTask.execute();


        mHandleMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(GCMTokens.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
//                    FirebaseMessaging.getInstance().subscribeToTopic(GCMTokens.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(GCMTokens.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getActivity(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                }
            }
        };


    }

    private void displayFirebaseRegId() {
        String token = FirebaseInstanceId.getInstance().getToken();
        if (token != null && !token.equals("")) {
            GCMTokens.setGCM(getActivity(), token);
//            Toast.makeText(getActivity(), "Push notification: " + token, Toast.LENGTH_LONG).show();

        }
    }


//    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String newMessage = intent.getExtras().getString(GCMTokens.EXTRA_MESSAGE);
//            // Waking up mobile if it is sleeping
//            WakeLocker.acquire(getActivity());
//            // checking for type intent filter
//            if (intent.getAction().equals(GCMTokens.REGISTRATION_COMPLETE)) {
//                // gcm successfully registered
//                // now subscribe to `global` topic to receive app wide notifications
////                FirebaseMessaging.getInstance().subscribeToTopic(GCMTokens.TOPIC_GLOBAL);
//// Get token
//                String token = FirebaseInstanceId.getInstance().getToken();
//                if(token!=null && !token.equals("")){
//                    GCMTokens.setGCM(getActivity(),token);
//                }
//
//
//            } else if (intent.getAction().equals(GCMTokens.PUSH_NOTIFICATION)) {
//                // new push notification is received
//
//                String message = intent.getStringExtra("message");
//
//                Toast.makeText(getActivity(), "Push notification: " + message, Toast.LENGTH_LONG).show();
//            }
//
//            Toast.makeText(getActivity(), "GCM: " + newMessage, Toast.LENGTH_LONG)
//                    .show();
//
//            WakeLocker.release();
//        }
//    };

    @Override
    public void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mHandleMessageReceiver,
                new IntentFilter(GCMTokens.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mHandleMessageReceiver,
                new IntentFilter(GCMTokens.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mHandleMessageReceiver);
    }

    //    @Override
//    public void onDestroy() {
//        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mHandleMessageReceiver);
//        super.onPause();
//    }


    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if (view == etName) {
            if (!hasFocus) {
                check[0] = FieldValidation.validate(getActivity(), etName, FieldValidation.NAME, true);
            }
        } else if (view == etEmail) {
            if (!hasFocus) {
                check[1] = FieldValidation.validate(getActivity(), etEmail, FieldValidation.EMAIL, true);
            }
        } else if (view == etHouseNo) {
            if (!hasFocus) {
                check[2] = FieldValidation.validate(getActivity(), etHouseNo, FieldValidation.STRING, false);
            }
        } else if (view == etFloor) {
            if (!hasFocus) {
                check[3] = FieldValidation.validate(getActivity(), etFloor, FieldValidation.STRING, false);
            }
        }
//        else if (view == etAddress) {
//            if (!hasFocus) {
//                FieldValidation.validate(getActivity(), etAddress, FieldValidation.STRING, false);
//            }
//        }
    }

    private void executeTask(int task) {
        if (sltColony != null) {
            if (task == TASK_UPDATE_PROFILE) {
                if (FieldValidation.validateAllFields(check)) {
                    assignTask = new AssignTask(getActivity(), task, etName.getText().toString()
                            , etEmail.getText().toString(), etHouseNo.getText().toString(), etFloor.getText().toString()
                            , sltColony, etAddress.getText().toString());
                    assignTask.execute(AppSettings.UpdateProfile);
                } else {
                    Toast.makeText(getActivity(), "Please fill mandatory fields.", Toast.LENGTH_LONG).show();
                }
            } else if (task == TASK_LOGIN) {
                assignTask = new AssignTask(getActivity(), task, etName.getText().toString()
                        , etEmail.getText().toString(), etHouseNo.getText().toString(), etFloor.getText().toString()
                        , sltColony, etAddress.getText().toString());
                assignTask.execute(AppSettings.Login);
            }
        } else {
            Toast.makeText(getActivity(), "Please select colony.", Toast.LENGTH_LONG).show();
        }

    }


    private class AssignTask extends AsyncTask<String, Void, JsonModel> {
        private final int task;
        Activity activity;
        private String Name, Mobile, Email, HouseNo, Floor, Colony, Address;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity, int task, String Name, String Email, String HouseNo,
                          String Floor, String Colony, String Address) {
            this.activity = activity;
            this.task = task;
            this.Name = Name;
            this.Email = Email;
            this.HouseNo = HouseNo;
            this.Floor = Floor;
            this.Colony = Colony;
            this.Address = Address;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            btnSubmit.setVisibility(View.GONE);
        }

        @Override
        protected JsonModel doInBackground(String... urls) {
            String response;
//            MultipartEntity entity = new MultipartEntity();
//            entity.addPart("playerId", new StringBody(LoadPref(Buddy.PlayerId)));
            if (task == TASK_UPDATE_PROFILE) {
                List<BasicNameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair("name", Name));
                if (activity != null) {
                    Mobile = AppUser.getMOBILE(activity);
                    String gcmId = GCMTokens.getGCM(activity);
                    params.add(new BasicNameValuePair("gcmId", gcmId));
                    params.add(new BasicNameValuePair("userId", AppUser.getRegistrationId(activity)));
                }
                params.add(new BasicNameValuePair("mobile", Mobile));
                params.add(new BasicNameValuePair("emailAddress", Email));
//              params.add(new BasicNameValuePair("image", ""));
                params.add(new BasicNameValuePair("houseNumber", HouseNo));
                params.add(new BasicNameValuePair("floor", Floor));
                params.add(new BasicNameValuePair("society", Colony));
                params.add(new BasicNameValuePair("address", Address));
                params.add(new BasicNameValuePair("occupation", ""));
                HttpReq ob = new HttpReq();
                response = ob.makeConnection(urls[0], HttpReq.POST, params);
                JsonParser jsonParser = new JsonParser();
                return jsonParser.parseUpdateProfileJson(response);
            } else if (task == TASK_LOGIN) {
                List<BasicNameValuePair> params = new ArrayList<>();
                if (activity != null) {
                    Mobile = AppUser.getMOBILE(activity);
                    params.add(new BasicNameValuePair("mobile", Mobile));
                    params.add(new BasicNameValuePair("userId", AppUser.getRegistrationId(activity)));
                }
                HttpReq ob = new HttpReq();
                response = ob.makeConnection(urls[0], HttpReq.POST, params);
                JsonParser jsonParser = new JsonParser();
                return jsonParser.SaveTempData(activity, response);
            }
            return null;
        }


        @Override
        protected void onPostExecute(JsonModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
            btnSubmit.setVisibility(View.VISIBLE);
            if (result != null) {
                if (result.getOutput().equalsIgnoreCase("success")) {
                    if (task == TASK_UPDATE_PROFILE) {
                        AppUser.setRWAData(activity, AppUser.getRegistrationId(activity), Name, Mobile, Email
                                , "Image", HouseNo, Floor, Colony, Address, "Free");
                        executeTask(TASK_LOGIN);
                    } else if (task == TASK_LOGIN) {
                        Toast.makeText(getActivity(), "Update record successfully...", Toast.LENGTH_LONG).show();
                        Utility.SavePref(activity, AppTokens.SessionProfile, "success");
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                } else {
                    Toast.makeText(getActivity(), result.getOutputMsg(), Toast.LENGTH_LONG).show();
                }
            } else {
                Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                retry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideBaseServerErrorAlertBox();
                        executeTask(TASK_UPDATE_PROFILE);
                    }
                });
            }
        }
    }

    private class PopulateSpinnerTask extends AsyncTask<Void, Void, AddressModel> {
        Activity activity;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public PopulateSpinnerTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            showProgressBar();
            showProgressDialog(activity, "Getting data, Please wait...");
        }

        @Override
        protected AddressModel doInBackground(Void... urls) {
            JsonParser jsonParser = new JsonParser();
            if (activity != null) {
//                return jsonParser.getAddressDropDownInfo(RWA.getSOCIETY(activity));
                return new AddressModel();
            }
            return null;
        }

        @Override
        protected void onPostExecute(AddressModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressDialog();
            if (result != null) {
//                if (result.getOutput().equals("success")) {
//                    SpinnerAdapter adapter = new SpinnerAdapter(getActivity(), R.layout.slot_spinner);
//                    adapter.addAll(result.getList());
//                    adapter.add("Select Society");
//                    spColony.setAdapter(adapter);
//                    spColony.setSelection(adapter.getCount());
//                    societyList = result.getAddressList();
//                    if (!Colony.equals("")) {
//                        int pos = result.getList().indexOf(Colony);
//                        spColony.setSelection(pos);
//                    }
//                }
                SpinnerAdapter adapter = new SpinnerAdapter(getActivity(), R.layout.slot_spinner);
                ArrayList<String>list =new ArrayList<>();
                list.add(Colony);
                adapter.addAll(list);
                spColony.setAdapter(adapter);
                spColony.setSelection(0);

            } else {
                Toast.makeText(getActivity(), JsonParser.ERRORMESSAGE, Toast.LENGTH_LONG).show();
            }
        }
    }
}
