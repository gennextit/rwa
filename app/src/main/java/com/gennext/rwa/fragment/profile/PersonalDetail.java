package com.gennext.rwa.fragment.profile;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.model.AddressModel;
import com.gennext.rwa.model.SpinnerAdapter;
import com.gennext.rwa.util.AppAnimation;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.FieldValidation;
import com.gennext.rwa.util.GCMTokens;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.RWA;

import java.util.ArrayList;

/**
 * Created by Abhijit on 13-Oct-16.
 */

public class PersonalDetail extends CompactFragment implements View.OnFocusChangeListener {
    Spinner spColony;//spFloor
    String[] profile = new String[7];
    private boolean[] check = {false, false, false, false, true};
    //    AssignTask assignTask;
    EditText etName, etEmail, etHouseNo, etFloor, etAddress;
    Button btnSubmit;
    ProgressBar progressBar;
    PopulateSpinnerTask spinnerTask;
    private String sltColony;
    private String floor;
    private String Colony;
    private String address;
    private UpdateProfileAllFields parentRefrence;
    private ArrayList<AddressModel> societyList;
    private String sltColonyId;


    public static PersonalDetail newInstance(UpdateProfileAllFields parentRefrence) {
        PersonalDetail fragment = new PersonalDetail();
        fragment.parentRefrence = parentRefrence;
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
//        if (assignTask != null) {
//            assignTask.onAttach(activity);
//        }
        if (spinnerTask != null) {
            spinnerTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
//        if (assignTask != null) {
//            assignTask.onDetach();
//        }
        if (spinnerTask != null) {
            spinnerTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_personal_detail, container, false);
//        setActionBarOption(v, "Profile Details");
        InitUI(v);
        return v;
    }


    private void InitUI(View v) {
//        spFloor = (Spinner) v.findViewById(R.id.sp_update_profile_floor);
        etFloor = (EditText) v.findViewById(R.id.et_update_profile_floor);
        spColony = (Spinner) v.findViewById(R.id.sp_update_profile_colony);
        etName = (EditText) v.findViewById(R.id.et_update_profile_name);
        etEmail = (EditText) v.findViewById(R.id.et_update_profile_email);
        etHouseNo = (EditText) v.findViewById(R.id.et_update_profile_house_no);
        etAddress = (EditText) v.findViewById(R.id.et_update_profile_address);
        btnSubmit = (Button) v.findViewById(R.id.btn_update_profile);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);

        etName.setOnFocusChangeListener(this);
        etEmail.setOnFocusChangeListener(this);
        etHouseNo.setOnFocusChangeListener(this);
        etFloor.setOnFocusChangeListener(this);
        etAddress.setOnFocusChangeListener(this);

        String gcmId = GCMTokens.getGCM(getActivity());

        profile = AppUser.getProfileData(getActivity());
        etName.setText(profile[1]);
        if (!profile[1].equals("")) {
            check[0] = true;
            etName.requestFocus();
        }
        etEmail.setText(profile[2]);
        if (!profile[2].equals("")) {
            check[1] = true;
            etEmail.requestFocus();
        }

        etHouseNo.setText(profile[3]);
        if (!profile[3].equals("")) {
            check[2] = true;
            etHouseNo.requestFocus();
        }

        floor = profile[4];
        Colony = profile[5];
        address = profile[6];
        etFloor.setText(floor != null ? floor : "");
        if (!floor.equals("")) {
            check[3] = true;
            etFloor.requestFocus();
        }
        etAddress.setText(address != null ? address : "");
        if (!address.equals("")) {
            check[4] = true;
//            etAddress.requestFocus();
        }


        spColony.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View arg1, int position, long arg3) {
                // TODO Auto-generated method stub
                if (!adapter.getSelectedItem().toString().equals("Select Society")) {
                    sltColony = adapter.getSelectedItem().toString();
                    if (societyList != null) {
                        sltColony = societyList.get(position).getSociety();
                        sltColonyId = societyList.get(position).getSocietyId();

                    }
                    etName.requestFocus();
                    etEmail.requestFocus();
                    etHouseNo.requestFocus();
                    etFloor.requestFocus();
//                    etAddress.requestFocus();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etName.requestFocus();
                etEmail.requestFocus();
                etHouseNo.requestFocus();
                etFloor.requestFocus();
                hideKeybord(getActivity());
                executeTask();
            }
        });

        spinnerTask = new PopulateSpinnerTask(getActivity());
        spinnerTask.execute();

    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if (view == etName) {
            if (!hasFocus) {
                check[0] = FieldValidation.validate(getActivity(), etName, FieldValidation.NAME, true);
            }
        } else if (view == etEmail) {
            if (!hasFocus) {
                check[1] = FieldValidation.validate(getActivity(), etEmail, FieldValidation.EMAIL, true);
            }
        } else if (view == etHouseNo) {
            if (!hasFocus) {
                check[2] = FieldValidation.validate(getActivity(), etHouseNo, FieldValidation.STRING, false);
            }
        } else if (view == etFloor) {
            if (!hasFocus) {
                check[3] = FieldValidation.validate(getActivity(), etFloor, FieldValidation.STRING, false);
            }
        }
//        else if (view == etAddress) {
//            if (!hasFocus) {
//                FieldValidation.validate(getActivity(), etAddress, FieldValidation.STRING, false);
//            }
//        }
    }

    private void executeTask() {
        if (sltColony != null) {
            if (FieldValidation.validateAllFields(check)) {

                setProfileData(getActivity(), etName.getText().toString()
                        , etEmail.getText().toString(), etHouseNo.getText().toString(), etFloor.getText().toString()
                        , sltColony, etAddress.getText().toString());
            } else {
                Toast.makeText(getActivity(), "Please fill mandatory fields.", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getActivity(), "Please select society.", Toast.LENGTH_LONG).show();
        }

    }

    private void setProfileData(Activity activity, String Name, String Email, String HouseNo,
                                String Floor, String Colony, String Address) {
//      UpdateProfileAllFields frag= (UpdateProfileAllFields) getFragmentManager().findFragmentByTag("updateProfileAllFields");
        if (parentRefrence != null) {
            parentRefrence.setProfileDetail(Name, Email, HouseNo, Floor, Colony, Address);
        }
    }


    private class PopulateSpinnerTask extends AsyncTask<Void, Void, AddressModel> {
        Activity activity;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public PopulateSpinnerTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getActivity(), "Getting data, Please wait...");
        }

        @Override
        protected AddressModel doInBackground(Void... urls) {
            JsonParser jsonParser = new JsonParser();
//            return jsonParser.getAddressDropDownInfo(RWA.getSOCIETY(activity));
            return new AddressModel();
        }

        @Override
        protected void onPostExecute(AddressModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressDialog();
            if (result != null) {
//                if (result.getOutput().equals("success")) {
//                    SpinnerAdapter adapter = new SpinnerAdapter(getActivity(), R.layout.slot_spinner);
//                    adapter.addAll(result.getList());
//                    adapter.add("Select Society");
//                    spColony.setAdapter(adapter);
//                    spColony.setSelection(adapter.getCount());
//                    societyList=result.getAddressList();
//                    if (!Colony.equals("")) {
//                        int pos = result.getList().indexOf(Colony);
//                        spColony.setSelection(pos);
//                    }
//                  }
                SpinnerAdapter adapter = new SpinnerAdapter(getActivity(), R.layout.slot_spinner);
                ArrayList<String>list =new ArrayList<>();
                list.add(Colony);
                adapter.addAll(list);
                spColony.setAdapter(adapter);
                spColony.setSelection(0);

            } else {
                Toast.makeText(getActivity(), JsonParser.ERRORMESSAGE, Toast.LENGTH_LONG).show();
            }
        }
    }
}
