package com.gennext.rwa.fragment.serviceProvider;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class Feedback extends CompactFragment {
    public static final int ACTIVITY = 1, FRAGMENT = 2, POPUP_DIALOG = 3;
    private TextView tvTitle, tvDesc;
    private Button btnOK;
    private String title;
    private FragmentManager manager;
    private int finishType;
    private String imageUrl, categoryId, serviceProviderId;
    ServiceProvider serviceProvider;

    public void setFeedDetail(String imageUrl, String categoryId, String serviceProviderId, ServiceProvider serviceProvider) {
        this.imageUrl = imageUrl;
        this.categoryId = categoryId;
        this.serviceProviderId = serviceProviderId;
        this.serviceProvider = serviceProvider;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_popup_alert_feedback, container, false);
        manager = getFragmentManager();
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        tvTitle = (TextView) v.findViewById(R.id.tv_popup_title);
        btnOK = (Button) v.findViewById(R.id.btn_popup);
        ImageView ivImage = (ImageView) v.findViewById(R.id.iv_profile);
        LinearLayout llClose = (LinearLayout) v.findViewById(R.id.ll_whitespace);
        final EditText etFeedback = (EditText) v.findViewById(R.id.et_alert_feedback);
        final RatingBar ratingBar = (RatingBar) v.findViewById(R.id.ratingBar);

//        tvTitle.setText(title);

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                manager.popBackStack();
            }
        });


        Glide.with(getContext())
                .load(imageUrl)
                .placeholder(R.drawable.profile)
                .error(R.drawable.profile)
                .into(ivImage);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (finishType == ACTIVITY) {
//                    getActivity().finish();
//                } else  if (finishType == POPUP_DIALOG) {
                hideKeybord(getActivity());
                manager.popBackStack();
                serviceProvider.sendFeedBack(categoryId, serviceProviderId, String.valueOf(ratingBar.getRating()), etFeedback.getText().toString());
//                }else {
//                    manager.popBackStack();
//                    manager.popBackStack();
//                }
            }
        });
    }


}