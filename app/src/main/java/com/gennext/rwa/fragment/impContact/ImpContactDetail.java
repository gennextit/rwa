package com.gennext.rwa.fragment.impContact;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.util.AnimationClass;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class ImpContactDetail extends CompactFragment {
    private TextView tvTitle, tvDetail,tvAvailable;
    private FragmentManager manager;
    private String name, image, designation,detail,availableFrom;
    private AnimationClass anim;
    private RelativeLayout RLView;


    public void setData(String name, String image, String designation, String detail, String availableFrom) {
        this.name=name;
        this.image=image;
        this.designation=designation;
        this.detail=detail;
        this.availableFrom=availableFrom;

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_popup_alert_impcontact, container, false);
        manager = getFragmentManager();
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        RLView = (RelativeLayout) v.findViewById(R.id.llappFeedback);
        Button btnDone = (Button) v.findViewById(R.id.btn_popup);
        ImageView ivImage = (ImageView) v.findViewById(R.id.iv_popup_image);
        LinearLayout llClose = (LinearLayout) v.findViewById(R.id.ll_whitespace);

        tvTitle= (TextView) v.findViewById(R.id.tv_popup_title);
        tvDetail= (TextView) v.findViewById(R.id.tv_popup_detail);
        tvAvailable= (TextView) v.findViewById(R.id.tv_popup_availibility);

        tvTitle.setText(name!=null?name:"");
        if(designation!=null && !designation.equals("")) {
            tvDetail.setText(designation);
        }else {
            tvDetail.setVisibility(View.GONE);
        }
        if(availableFrom!=null && !availableFrom.equals("")) {
            tvAvailable.setText("Available from\n"+availableFrom);
        }else {
            tvAvailable.setVisibility(View.GONE);
        }

        anim=new AnimationClass();
        anim.setPopupAnimation(RLView);


        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                manager.popBackStack();
            }
        });
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                manager.popBackStack();
            }
        });


        Glide.with(getContext())
                .load(image)
                .placeholder(R.drawable.profile)
                .error(R.drawable.profile)
                .into(ivImage);
    }

}