package com.gennext.rwa.fragment;


import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.gennext.rwa.LoadData;
import com.gennext.rwa.LoginActivity;
import com.gennext.rwa.MainActivity;
import com.gennext.rwa.R;
import com.gennext.rwa.SplashScreen;
import com.gennext.rwa.UpdateProfileActivity;
import com.gennext.rwa.util.AppTokens;
import com.gennext.rwa.util.WidgetAnimation;

public class SplashWindow extends CompactFragment {
    private static final long SPLASH_TIME_OUT = 1000;
    static String TAG = "splAshAcreen";
    // Splash screen timer
    private ImageView ivSplashBackground,ivSplashLogo;

    public static Fragment newInstance() {
        SplashWindow fragment=new SplashWindow();
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v= inflater.inflate(R.layout.activity_splash_screen, container, false);

        this.ivSplashBackground = (ImageView) v.findViewById(R.id.iv_splash_background);
        this.ivSplashLogo = (ImageView) v.findViewById(R.id.iv_splash_logo);

        WidgetAnimation.zoomInForSplash(ivSplashBackground, new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if(getContext()!=null) {
                    startTimerForMainActivity(getActivity(), ivSplashLogo);
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        return v;
    }
    private void startTimerForMainActivity(final Activity activity, View view) {
        // TODO Auto-generated method stub
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if(getContext()==null){
                    return;
                }
                if (!LoadPref(AppTokens.SessionSignup).equals("")) {
                    if (!LoadPref(AppTokens.SessionProfile).equals("")) {
                        Intent intent = new Intent(activity, LoadData.class);
                        activity.startService(intent);
                        Intent i = new Intent(activity, MainActivity.class);
                        startActivity(i);
                        activity.finish();
                    } else {
                        Intent i = new Intent(activity, UpdateProfileActivity.class);
                        i.putExtra(UpdateProfileActivity.UPDATE_PROFILE,UpdateProfileActivity.LOGIN_PROCESS);
                        startActivity(i);
                        activity.finish();
                    }
                } else {
                    ((LoginActivity)getActivity()).setInitialScreen(ivSplashLogo);
                }
            }
        }, SPLASH_TIME_OUT);
    }






    public String LoadPref(String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String data = sharedPreferences.getString(key, "");
        return data;
    }
}
