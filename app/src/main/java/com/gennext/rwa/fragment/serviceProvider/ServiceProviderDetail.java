package com.gennext.rwa.fragment.serviceProvider;


import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.model.ServiceProviderModel;

import java.util.ArrayList;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class ServiceProviderDetail extends CompactFragment {
    private String name, designation, description;
    private FragmentManager manager;
    private ArrayList<ServiceProviderModel> reviewList;
    private String imageUrl, categoryId, serviceProviderId;
    private ServiceProvider serviceProvider;
    private DetailPopup detailPopup;
    private Feedback feedback;

    public void setFeedDetail(String imageUrl, String categoryId, String serviceProviderId,
                              ServiceProvider serviceProvider, String name, String designation,
                              String description,ArrayList<ServiceProviderModel> reviewList) {
        this.imageUrl = imageUrl;
        this.categoryId = categoryId;
        this.serviceProviderId = serviceProviderId;
        this.serviceProvider = serviceProvider;
        this.name = name;
        this.designation = designation;
        this.description = description;
        this.reviewList=reviewList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_popup_sp, container, false);
        manager = getFragmentManager();
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        detailPopup =new DetailPopup();
        feedback=new Feedback();
        setDetailAlert();
    }

    private void setDetailAlert() {
        detailPopup.setFeedDetail(imageUrl, categoryId, serviceProviderId, serviceProvider,
                name,designation,description,reviewList,this);
        setUnSetFragment(getFragmentManager(),R.id.llspcontainer,detailPopup,"detailPopup",feedback);
    }

    public void setFeedbackAlert() {
        feedback.setFeedDetail(imageUrl, categoryId, serviceProviderId,serviceProvider);
        setUnSetFragment(getFragmentManager(),R.id.llspcontainer,feedback,"feedback",detailPopup);
    }
}