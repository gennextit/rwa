package com.gennext.rwa.fragment.vehicle;

import android.animation.Animator;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.fragment.PopupAlert;
import com.gennext.rwa.model.VehicleAdapter;
import com.gennext.rwa.model.VehicleModel;
import com.gennext.rwa.util.AppSettings;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.HttpReq;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.RWA;

import com.gennext.rwa.util.internet.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import static android.R.id.list;
import static android.R.id.switchInputMethod;

/**
 * Created by Abhijit on 15-Oct-16.
 */

public class VehicleList extends CompactFragment {
    private static final int TASK_LOADLIST = 1, TASK_DELETE = 2;
    private AssignTask assignTask;
    private ListView lvMain;
    private ArrayList<VehicleModel> cList;
    private FloatingActionButton fab;
    private int isScrolling = MotionEvent.ACTION_DOWN;
    private VehicleAdapter adapter;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (RWA.getVEHICLE(getActivity()).equals("")) {
            VehicleDetail vehicleDetail = new VehicleDetail();
            vehicleDetail.setPatentRef(VehicleList.this, VehicleDetail.MODE_INSERT);
            addFragment(vehicleDetail, android.R.id.content, "vehicleDetail");
        }
        View v = inflater.inflate(R.layout.frag_vehicle_list, container, false);
        setActionBarOption(v, "Vehicle");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        lvMain = (ListView) v.findViewById(R.id.expandableListView1);
        lvMain.setSmoothScrollbarEnabled(true);
        fab = (FloatingActionButton) v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Click action
                addVehicle();

            }
        });
        if (cList == null)
            cList = new ArrayList<>();
        if (adapter == null) {
            adapter = new VehicleAdapter(getActivity(), R.layout.slot_checkout, cList);
            lvMain.setAdapter(adapter);
        }
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getActivity(),"Long press to edit/delete",Toast.LENGTH_LONG).show();
            }
        });


        lvMain.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int lastPosition = -1;

            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING: // SCROLL_STATE_FLING
                        //hide button here
                        showOrHideButton(View.GONE);
//                        fab.setVisibility(View.VISIBLE);
                        break;

                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL: // SCROLL_STATE_TOUCH_SCROLL
                        //hide button here
                        showOrHideButton(View.GONE);
//                        fab.setVisibility(View.VISIBLE);
                        break;

                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE: // SCROLL_STATE_IDLE
                        //show button here
                        showOrHideButton(View.VISIBLE);
//                        fab.setVisibility(View.GONE);
                        break;

                    default:
                        //show button here
//                        btn.setVisibility(View.VISIBLE);
                        break;
                }

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        setProgressBar(v);
        executeTask(TASK_LOADLIST, null, 0);

        registerForContextMenu(lvMain);
    }

    private void addVehicle() {
        VehicleDetail vehicleDetail = new VehicleDetail();
        vehicleDetail.setPatentRef(VehicleList.this, VehicleDetail.MODE_INSERT);
        addFragment(vehicleDetail, android.R.id.content, "vehicleDetail");
    }

    private void executeTask(int task, String vehicleId, int removeItemPos) {
        switch (task) {
            case TASK_LOADLIST:
                assignTask = new AssignTask(getActivity(), task, vehicleId, removeItemPos);
                assignTask.execute();
                break;
            case TASK_DELETE:
                assignTask = new AssignTask(getActivity(), task, vehicleId, removeItemPos);
                assignTask.execute(AppSettings.vehicleDelete);
                break;
        }
    }

    private void showOrHideButton(int visibility) {
        if (visibility == View.VISIBLE && fab.getVisibility() != View.VISIBLE) {
            fab.setVisibility(View.VISIBLE);
            fab.animate().alpha(1).setStartDelay(0).setDuration(100)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }
                    });
        } else if (visibility == View.GONE && fab.getVisibility() != View.GONE) {
            fab.animate().alpha(0).setStartDelay(1000).setDuration(100)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            fab.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }
                    });
        }
    }

    public void setUpdateItemInList(int removeItemPos, ArrayList<VehicleModel> list) {
        if (list != null) {
            cList.remove(removeItemPos);
            list.addAll(cList);
            cList.clear();
            cList.addAll(list);
            adapter.notifyDataSetChanged();
        }
    }

    public void setUpdateList(ArrayList<VehicleModel> list) {
        if (list != null) {
            list.addAll(cList);
            cList.clear();
            cList.addAll(list);
            adapter.notifyDataSetChanged();
        }
    }

    public void refreshList() {
        executeTask(TASK_LOADLIST, null, 0);
    }


    private class AssignTask extends AsyncTask<String, Void, VehicleModel> {
        private int task;
        Activity activity;
        private String vehicleId;
        private int removeItemPos;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity, int task, String vehicleId, int removeItemPos) {
            this.activity = activity;
            this.task = task;
            this.vehicleId = vehicleId;
            this.removeItemPos = removeItemPos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
        }

        @Override
        protected VehicleModel doInBackground(String... urls) {
            JsonParser jsonParser;
            String mobile = null, retponse;
            if (task == TASK_LOADLIST) {
                jsonParser = new JsonParser();
                return jsonParser.parseVehicleData(RWA.getVEHICLE(activity));
            } else if (task == TASK_DELETE) {
                List<BasicNameValuePair> params = new ArrayList<>();
                if (activity != null) {
                    mobile = AppUser.getMOBILE(activity);
                    params.add(new BasicNameValuePair("userId", AppUser.getRegistrationId(activity)));
                    params.add(new BasicNameValuePair("mobile", mobile));
                }
                params.add(new BasicNameValuePair("vehicleId", vehicleId));

                HttpReq req = new HttpReq();
                retponse = req.makeConnection(urls[0], HttpReq.POST, params);
                jsonParser = new JsonParser();
                if(activity!=null)
                return jsonParser.parseVehicleDeleteJson(activity,retponse);
            }
            return null;
        }

        @Override
        protected void onPostExecute(VehicleModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressBar();
            if (result != null) {
                if (result.getOutput().equals("success")) {
                    if (task == TASK_LOADLIST) {
                        cList.clear();
                        cList.addAll(result.getList());
                        adapter.notifyDataSetChanged();
                    } else if (task == TASK_DELETE) {
                        refreshList();
//                        cList.remove(removeItemPos);
//                        adapter.notifyDataSetChanged();
                    }
                } else {
                    addVehicle();
                }
            } else {
                if (task == TASK_DELETE) {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            hideBaseServerErrorAlertBox();
                            executeTask(TASK_DELETE, vehicleId, removeItemPos);
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater m = getActivity().getMenuInflater();
        m.inflate(R.menu.vehicle_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info;
        int position;
        switch (item.getItemId()) {
            case R.id.edit_item:
                info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                position = (int) info.id;
//                cList.remove(position);
                showEditOption(position);
//                this.adapter.notifyDataSetChanged();
                return true;
            case R.id.delete_item:
                info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                position = (int) info.id;
                showDeleteOption(position);
                return true;
        }
        return super.onContextItemSelected(item);
    }

    private void showDeleteOption(int position) {
        executeTask(TASK_DELETE, cList.get(position).getVehicleId(), position);
    }

    private void showEditOption(int position) {
        VehicleDetail vehicleDetail = new VehicleDetail();
        vehicleDetail.setPatentRef(VehicleList.this, VehicleDetail.MODE_UPDATE);
        if (cList != null)
            vehicleDetail.setVehicleDetailForUpdate(position, cList.get(position).getVehicleId(), cList.get(position).getVehicleNo(),
                    cList.get(position).getVehicleType(), cList.get(position).getVehicleModel());
        addFragment(vehicleDetail, android.R.id.content, "vehicleDetail");
    }
}
