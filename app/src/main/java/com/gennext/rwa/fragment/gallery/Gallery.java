package com.gennext.rwa.fragment.gallery;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.fragment.PopupAlert;
import com.gennext.rwa.model.GalleryAdapter;
import com.gennext.rwa.model.GalleryModel;
import com.gennext.rwa.util.AppAnimation;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.RWA;

import java.util.ArrayList;

/**
 * Created by Abhijit on 29-Sep-16.
 */

public class Gallery extends CompactFragment {
    GridView gridView;
    ArrayList<GalleryModel> list;

    LinearLayout llProgress;

    public static Fragment newInstance() {
        Gallery fragment=new Gallery();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_gallery, container, false);
        setActionBarOption(v, "Gallery");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        gridView = (GridView) v.findViewById(R.id.gv_profile_selection);
        llProgress=(LinearLayout)v.findViewById(R.id.ll_progress);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                if (list != null) {
                    GalleryView galleryView =new GalleryView();
                    galleryView.setEventName(list.get(position).getEventName(),list.get(position).getEventListDetail());
                    FragmentTransaction transaction=getFragmentManager().beginTransaction();
                    transaction.add(android.R.id.content,galleryView,"galleryView");
                    transaction.addToBackStack("transaction");
                    transaction.commit();
                }
            }
        });
        setList();
    }

    private void setList() {
        JsonParser jsonParser = new JsonParser();
        GalleryModel result = jsonParser.getGalleryInfo(RWA.getGalarryInfo(getContext()));
        if (result != null) {
            if (result.getOutput().equals("success")) {
                list = result.getEventList();
                GalleryAdapter adapter = new GalleryAdapter(getActivity(), R.layout.custom_slot, list);
                gridView.setAdapter(adapter);
            }else{
                showPopupAlert("Gallery", getResources().getString(R.string.gallery_msg), PopupAlert.FRAGMENT);
            }
        }
    }

}