package com.gennext.rwa.fragment.dailyNeeds;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.model.CheckOutAdapter;
import com.gennext.rwa.model.CheckoutModel;
import com.gennext.rwa.util.DBManager;
import com.gennext.rwa.util.L;

import java.util.ArrayList;

/**
 * Created by Abhijit on 11-Oct-16.
 */



public class Checkout extends CompactFragment {
    private TextView tvPrice,payable;
    private LinearLayout llcheckout,progressBar;
    private ListView lv;
    private ArrayList<String> errorList;
    private ArrayList<CheckoutModel> checkoutlist;
    private DBManager db;
    private float calPrice=0.0f,calQuantity=0.0f,deductPrice=0.0f,shipingAmount=0.0f;

    LoadServiceList loadServiceList;
    CalPriceTask calPriceTask;
    DeductPriceTask deductPriceTask;

    LinearLayout llActionBack;
    TextView tvTitle;
    private Button btnCheckout;


    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if(loadServiceList!=null){
            loadServiceList.onAttach(context);
        }
        if(calPriceTask!=null){
            calPriceTask.onAttach(context);
        }
        if(deductPriceTask!=null){
            deductPriceTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(loadServiceList!=null){
            loadServiceList.onDetach();
        }
        if(calPriceTask!=null){
            calPriceTask.onDetach();
        }
        if(deductPriceTask!=null){
            deductPriceTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_checkout, container, false);
        setActionBarOpt(v);
        InitUI(v);
        return v;
    }

    private void setActionBarOpt(View v) {
        llActionBack = (LinearLayout) v.findViewById(R.id.ll_actionbar_back);
        tvTitle = (TextView) v.findViewById(R.id.actionbar_title);
        setActionBarOption();
        setActionBarTitle("Checkout");

    }
    private void setActionBarTitle(String title) {
        tvTitle.setText(title);
    }

    public void setActionBarOption() {
        llActionBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                getFragmentManager().popBackStack();
            }
        });
    }


    private void InitUI(View v) {
        progressBar = (LinearLayout) v.findViewById(R.id.ll_progress);
        lv = (ListView) v.findViewById(R.id.listView1);
        tvPrice = (TextView) v.findViewById(R.id.tv_cal_price);
        payable = (TextView) v.findViewById(R.id.tv_checkout_payable);
        btnCheckout = (Button) v.findViewById(R.id.btn_check_out_order);
        llcheckout = (LinearLayout) v.findViewById(R.id.llcheckout);
        llcheckout.setVisibility(View.INVISIBLE);
        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(checkoutlist!=null){


                }
            }
        });

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConfirmOrderClick();
            }
        });

        db = new DBManager(getActivity());

        loadServiceList=new LoadServiceList(getActivity());
        loadServiceList.execute();
//        new LoadServiceList().execute();
        calPriceMethod();
    }

    public void calPriceMethod(){
        calPriceTask=new CalPriceTask(getActivity());
        calPriceTask.execute();

    }

    public void deductPriceMethod(String amount){
//        new DeductPriceTask().execute(amount);
        deductPriceTask=new DeductPriceTask(getActivity(),tvPrice.getText().toString());
        deductPriceTask.execute();
    }

    public void onConfirmOrderClick() {
        CheckoutConfirm checkoutConfirm=new CheckoutConfirm();
        checkoutConfirm.setCheckoutDetail(payable.getText().toString());
        FragmentTransaction transaction=getFragmentManager().beginTransaction();
        transaction.add(android.R.id.content,checkoutConfirm,"checkoutConfirm");
        transaction.addToBackStack("checkoutConfirm");
        transaction.commit();
//        new fakeConfirmOrder().execute();
    }



    private class LoadServiceList extends AsyncTask<Void, Void, ArrayList<CheckoutModel>> {

        private Activity activity;

        public void onAttach(Activity activity) {
            this.activity=activity;
        }
        public void onDetach() {
            this.activity=null;
        }

        private LoadServiceList(Activity activity){
            this.activity=activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<CheckoutModel> doInBackground(Void... urls) {

            String ErrorMsg = getSt(R.string.record_not_available);
            if(activity!=null){
                checkoutlist=new ArrayList<>();
                checkoutlist=db.ViewData();
                if (checkoutlist!=null) {
                    // L.m(result);
                    return checkoutlist;
                } else {
                    L.m(ErrorMsg);
                }
            }

            return null;

        }

        @Override
        protected void onPostExecute(ArrayList<CheckoutModel> result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if(activity!=null){
                progressBar.setVisibility(View.GONE);
                if (result != null) {
                    CheckOutAdapter ExpAdapter = new CheckOutAdapter(activity,R.layout.slot_checkout, result);
                    lv.setAdapter(ExpAdapter);
                }else{
                    errorList=new ArrayList<String>();
                    errorList.add("Empty Cart");
//                ArrayAdapter<String> adapter=new ArrayAdapter<String>( activity, R.layout.slot_checkout, R.id.tv_error_text, errorList);
//                lv.setAdapter(adapter);
                    //showToast("No record Available");
                }
            }
        }
    }

    private class CalPriceTask extends AsyncTask<Void, Void, Boolean> {
        private Activity activity;

        public void onAttach(Activity activity) {
            this.activity=activity;
        }
        public void onDetach() {
            this.activity=null;
        }

        private CalPriceTask(Activity activity){
            this.activity=activity;
        }

        @Override
        protected Boolean doInBackground(Void... urls) {

            ArrayList<CheckoutModel>list=new ArrayList<>();
            if(activity!=null){
                DBManager db = new DBManager(getActivity());
                list=db.ViewData();
                if (list!=null) {
                    for(CheckoutModel ob : list){
                        calPrice=calPrice+(Float.parseFloat(ob.getProductPrice())*Float.parseFloat(ob.getProductQuantity()));
                    }

                    return true;
                } else {

                }
            }


            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if(activity!=null){
                if (result) {
                    tvPrice.setText(String.valueOf(calPrice));
                    payable.setText(String.valueOf(calPrice+shipingAmount));
                    if(calPrice!=0.0f){
                        llcheckout.setVisibility(View.VISIBLE);
                    }
                }else{
                    tvPrice.setText(String.valueOf(calPrice));
                    payable.setText(String.valueOf(calPrice+shipingAmount));
                    if(calPrice!=0.0){
                        llcheckout.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    private class DeductPriceTask extends AsyncTask<String, Void, Boolean> {
        private Activity activity;
        private String price;


        public void onAttach(Activity activity) {
            this.activity=activity;
        }
        public void onDetach() {
            this.activity=null;
        }

        private DeductPriceTask(Activity activity,String price){
            this.activity=activity;
            this.price=price;
        }
        @Override
        protected Boolean doInBackground(String... urls) {

            float actual=Float.parseFloat(price);
            float deductAmt=Float.parseFloat(urls[0]);
            deductPrice=actual-deductAmt;

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (result) {
                tvPrice.setText(String.valueOf(deductPrice));
                payable.setText(String.valueOf(calPrice-shipingAmount));

                if(calPrice!=0.0){
                    llcheckout.setVisibility(View.VISIBLE);
                }
            }

        }
    }

    public class fakeConfirmOrder extends AsyncTask<Void, Void, Void>{

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setCancelable(false);
            pDialog.setMessage("Processing, Keep patience!");
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            pDialog.dismiss();
            Toast.makeText(getActivity(),"Order Confirmed successfully",Toast.LENGTH_SHORT).show();
//            showToast();
//            AppTokens.sumTotalPrice=0;
//            AppTokens.countTotalItem=0;
            db.DropTable();
            getActivity().finish();

        }

    }


    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        db.CloseDB();
    }
}
