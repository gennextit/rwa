package com.gennext.rwa.fragment.directory;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.util.AnimationClass;


public class DirectoryDetail extends CompactFragment {
    private Button btnOK;
    private FragmentManager manager;
//    private AssignTask assignTask;
    private TextView tvHouseNo,tvName,tvOccupation,tvMobile,tvVehicle;
    private RelativeLayout RLView;
    private AnimationClass anim;
    private String houseNo,personName,occupation,phoneNo,vehicleNo;

//    @Override
//    public void onAttach(Activity activity) {
//        // TODO Auto-generated method stub
//        super.onAttach(activity);
//        if (assignTask != null) {
//            assignTask.onAttach(activity);
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        // TODO Auto-generated method stub
//        super.onDetach();
//        if (assignTask != null) {
//            assignTask.onDetach();
//        }
//    }

    public void setDetail(String houseNo, String personName, String occupation, String phoneNo, String vehicleNo) {
        this.houseNo=houseNo;
        this.personName=personName;
        this.occupation=occupation;
        this.phoneNo=phoneNo;
        this.vehicleNo=vehicleNo;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_directory_detail, container, false);
        manager = getFragmentManager();
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
//        tvTitle = (TextView) v.findViewById(R.id.tv_popup_title);
        btnOK = (Button) v.findViewById(R.id.btn_popup);
        RLView = (RelativeLayout) v.findViewById(R.id.llappFeedback);
        ImageView ivImage = (ImageView) v.findViewById(R.id.iv_profile);
        LinearLayout llClose = (LinearLayout) v.findViewById(R.id.ll_whitespace);
        tvHouseNo = (TextView) v.findViewById(R.id.tv_directory_house_no);
        tvName = (TextView) v.findViewById(R.id.tv_directory_name);
        tvOccupation = (TextView) v.findViewById(R.id.tv_directory_occupation);
        tvMobile = (TextView) v.findViewById(R.id.tv_directory_mobile);
        tvVehicle = (TextView) v.findViewById(R.id.tv_directory_vehicle);

        tvHouseNo.setText(houseNo!=null? houseNo:"");
        tvName.setText(personName!=null?personName:"");
        if(occupation!=null && !occupation.equals("")) {
            tvOccupation.setText(occupation);
        }else {
            tvOccupation.setVisibility(View.GONE);
        }
        tvMobile.setText(phoneNo!=null?"Mobile "+phoneNo:"");

        if(vehicleNo!=null && !vehicleNo.equals("")) {
            tvVehicle.setText("Vehicle No\n"+vehicleNo.replace(",","\n"));
        }else {
            tvVehicle.setVisibility(View.GONE);
        }

        anim=new AnimationClass();
        anim.setPopupAnimation(RLView);



        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.popBackStack();
            }
        });

//        btnOK.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                sendFeedback(String.valueOf(rbRating.getRating()), etFeedback.getText().toString(),
//                        AppUser.getRegistrationId(getActivity()),etEmail.getText().toString());
//            }
//        });
    }



//    private void sendFeedback(String rating, String feedback, String regId, String emailId) {
//        assignTask = new AssignTask(getActivity(),rating,feedback,regId,emailId);
//        assignTask.execute(AppSettings.sendFeedback);
//    }
//
//    private class AssignTask extends AsyncTask<String, Void, ResponseModel> {
//        Activity activity;
//        private String rating,feedback,regId,emailId;
//
//        public void onAttach(Activity activity) {
//            // TODO Auto-generated method stub
//            this.activity = activity;
//        }
//
//        public void onDetach() {
//            // TODO Auto-generated method stub
//            this.activity = null;
//        }
//
//        public AssignTask(Activity activity, String rating, String feedback, String regId, String emailId) {
//            this.rating=rating;
//            this.feedback=feedback;
//            this.regId=regId;
//            this.activity = activity;
//            this.emailId=emailId;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            showProgressDialog(activity,"Processing please wait...");
//        }
//
//        @Override
//        protected ResponseModel doInBackground(String... urls) {
//            List<BasicNameValuePair> params = new ArrayList<>();
//            if(activity!=null) {
//                params.add(new BasicNameValuePair("userId", AppUser.getRegistrationId(activity)));
//                params.add(new BasicNameValuePair("mobile", AppUser.getMOBILE(activity)));
//            }
//            params.add(new BasicNameValuePair("emailId", emailId));
//            params.add(new BasicNameValuePair("rating", rating));
//            params.add(new BasicNameValuePair("feedback", feedback));
//            HttpReq req=new HttpReq();
//            String response = req.makeConnection(urls[0], HttpReq.POST,params);
//            JsonParser jsonParser = new JsonParser();
//            return jsonParser.parseFeedInfo(response);
//        }
//
//        @Override
//        protected void onPostExecute(ResponseModel result) {
//            // TODO Auto-generated method stub
//            super.onPostExecute(result);
//            if(activity!=null) {
//                hideProgressDialog();
//                if (result != null) {
//                    if (result.getOutput().equals("success")) {
//                        showPopupAlert("Feedback", result.getOutputMsg(), PopupAlert.FRAGMENT);
//                    } else if (result.getOutput().equals("failure")) {
//                        showPopupAlert("Feedback", result.getOutputMsg(), PopupAlert.FRAGMENT);
//                    } else {
//                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.FRAGMENT);
//                    }
//                } else {
//                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
//                    retry.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            hideBaseServerErrorAlertBox();
//                            sendFeedback(String.valueOf(rating), feedback, regId, emailId);
//                        }
//                    });
//                }
//            }
//        }
//    }
}
