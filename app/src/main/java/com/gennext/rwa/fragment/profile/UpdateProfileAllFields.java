package com.gennext.rwa.fragment.profile;

/**
 * Created by Abhijit on 05-Nov-16.
 */

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.util.AppAnimation;
import com.gennext.rwa.util.AppSettings;
import com.gennext.rwa.util.AppTokens;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.GCMTokens;
import com.gennext.rwa.util.HttpReq;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.Utility;
import com.gennext.rwa.util.internet.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;


public class UpdateProfileAllFields extends CompactFragment {
    private static final int PERSONAL = 0, SPAUSE = 1;
    private static final String SPOUSE_TITLE = "Spouse Detail";
    FragmentManager manager;
    private PersonalDetail personalDetail;
    private SpauseDetail spauseDetail;
//    private VehicleDetail vehicleDetail;
    private String name, mobile, email, houseNo, floor, colony, address;
    private String spName, spMobile, spEmail, spOccupation;
    AssignTask assignTask;
    private TextView tvTitle;


    public static Fragment newInstance() {
        UpdateProfileAllFields fragment=new UpdateProfileAllFields();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_update_profile_all_fields, container, false);
        setActionBar(v,"Personal Detail");
        manager = getFragmentManager();

        personalDetail = PersonalDetail.newInstance(this);
        spauseDetail = SpauseDetail.newInstance(this);
//        vehicleDetail = new VehicleDetail();
//        vehicleDetail.setParentRefrence(this);

        InitUI();
        return v;
    }

    public void setActionBar(View v, String Title) {
        LinearLayout actionBack;
        actionBack = (LinearLayout) v.findViewById(R.id.ll_actionbar_back);
        tvTitle = (TextView) v.findViewById(R.id.actionbar_title);

        tvTitle.setText(Title);
        actionBack.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View arg0) {
                hideKeybord(getActivity());
                if(tvTitle.getText().equals(SPOUSE_TITLE)){
                    displayPersonalScreen();
                }else{
                    getActivity().onBackPressed();
                }
            }
        });

    }


    private void InitUI() {
        mobile = AppUser.getMOBILE(getActivity());
//        setTab(PERSONAL);
        displayPersonalScreen();
    }

    // Replace the switch method
    protected void displayPersonalScreen() {
        tvTitle.setText("Personal Detail");
        FragmentTransaction ft = manager.beginTransaction();
        if (personalDetail.isAdded()) { // if the fragment is already in container
            ft.show(personalDetail);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.container, personalDetail, "personalDetail");
        }
        // Hide fragment B
        if (spauseDetail.isAdded()) {
            ft.hide(spauseDetail);
        }
        // Hide fragment C
//        if (vehicleDetail.isAdded()) {
//            ft.hide(vehicleDetail);
//        }
        // Commit changes
        ft.commit();
    }

    protected void displaySpouseScreen() {
        tvTitle.setText(SPOUSE_TITLE);
        FragmentTransaction ft = manager.beginTransaction();
        if (spauseDetail.isAdded()) { // if the fragment is already in container
            ft.show(spauseDetail);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.container, spauseDetail, "spauseDetail");
        }
        // Hide fragment B
        if (personalDetail.isAdded()) {
            ft.hide(personalDetail);
        }
        // Hide fragment C
//        if (vehicleDetail.isAdded()) {
//            ft.hide(vehicleDetail);
//        }
        // Commit changes
        ft.commit();
    }


//    }

    public void setProfileDetail(String name, String email, String houseNo, String floor, String colony
            , String address) {
        this.name = name;
        this.email = email;
        this.houseNo = houseNo;
        this.floor = floor;
        this.colony = colony;
        this.address = address;

        displaySpouseScreen();
    }

    public void setSpouseData(String spName, String spMobile, String spEmail, String spOccupation) {
        this.spName = spName;
        this.spMobile = spMobile;
        this.spEmail = spEmail;
        this.spOccupation = spOccupation;

        executeTask();
    }

//    }

    private void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.UpdateProfile);
    }

    private class AssignTask extends AsyncTask<String, Void, String> {
        Activity activity;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity, "Profile updating...");
        }

        @Override
        protected String doInBackground(String... urls) {

//            MultipartEntity entity = new MultipartEntity();
//            entity.addPart("playerId", new StringBody(LoadPref(Buddy.PlayerId)));

            List<BasicNameValuePair> params = new ArrayList<>();
            if(activity!=null){
                String gcmId= GCMTokens.getGCM(activity);
                params.add(new BasicNameValuePair("gcmId",gcmId));
                params.add(new BasicNameValuePair("userId", AppUser.getRegistrationId(activity)));
            }
            params.add(new BasicNameValuePair("name", name != null ? name : ""));
            params.add(new BasicNameValuePair("mobile", mobile != null ? mobile : ""));
            params.add(new BasicNameValuePair("emailAddress", email != null ? email : ""));
            params.add(new BasicNameValuePair("houseNumber", houseNo != null ? houseNo : ""));
            params.add(new BasicNameValuePair("floor", floor != null ? floor : ""));
            params.add(new BasicNameValuePair("society", colony != null ? colony : ""));
            params.add(new BasicNameValuePair("address", address != null ? address : ""));

            params.add(new BasicNameValuePair("spouseName", spName != null ? spName : ""));
            params.add(new BasicNameValuePair("spousePhone", spMobile != null ? spMobile : ""));
            params.add(new BasicNameValuePair("spouseEmail", spEmail != null ? spEmail : ""));
            params.add(new BasicNameValuePair("spouseOccupation", spOccupation != null ? spOccupation : ""));

            HttpReq ob = new HttpReq();
            return ob.makeConnection(urls[0], HttpReq.POST, params, HttpReq.EXECUTE_TASK);
        }


        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressDialog();
            if (result != null) {
                if (result.equalsIgnoreCase("success")) {
                    AppUser.setRWAData(activity, AppUser.getRegistrationId(activity), name, mobile, email
                            , "Image", houseNo, floor, colony, address, "Free");
                    AppUser.setSpouseData(activity, spName, spMobile, spEmail, spOccupation);

                    Utility.SavePref(activity, AppTokens.SessionProfile, "success");
                    Toast.makeText(getActivity(), "Record updated successfully...", Toast.LENGTH_LONG).show();
                    restartMainActivity();
                } else {
                    Toast.makeText(getActivity(), result, Toast.LENGTH_LONG).show();
                }
            } else {
                Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                retry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideBaseServerErrorAlertBox();
                        executeTask();
                    }
                });
            }
        }
    }

    protected void restartMainActivity() {
        getActivity().finish();
    }
}

