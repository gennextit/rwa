package com.gennext.rwa.fragment.dailyNeeds;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.fragment.PopupAlert;
import com.gennext.rwa.model.CircularAdapter;
import com.gennext.rwa.model.OrderHistoryAdapter;
import com.gennext.rwa.model.OrderHistoryModel;
import com.gennext.rwa.util.AppSettings;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.HttpReq;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.RWA;

import com.gennext.rwa.util.internet.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhijit on 18-Oct-16.
 */

public class OrderHistory extends CompactFragment {
    AssignTask assignTask;
    private ListView lvMain;
    ArrayList<OrderHistoryModel>list;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_order_history, container, false);
        setActionBarOption(v,"Order History");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        lvMain=(ListView)v.findViewById(R.id.expandableListView1);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if(list!=null){
                    showPopupOrder(list.get(position).getOrderTrackId(),list.get(position).getOrderShipName()
                            ,list.get(position).getOrderPhone(),list.get(position).getOrderShipAddress(),list.get(position).getList());
                }
            }
        });

        setProgressBar(v);
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.orderedList);
    }

    private void showPopupOrder(String orderTrackId, String orderShipName, String orderPhone, String orderShipAddress, ArrayList<OrderHistoryModel> list) {
        OrderPopup orderPopup = new OrderPopup();
        orderPopup.setDetail(orderTrackId,orderShipName,orderPhone,orderShipAddress,list);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(android.R.id.content, orderPopup, "orderPopup");
        transaction.addToBackStack("orderPopup");
        transaction.commit();
    }

    private class AssignTask extends AsyncTask<String, Void, OrderHistoryModel> {
        Activity activity;
        private String response;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
        }

        @Override
        protected OrderHistoryModel doInBackground(String... urls) {
            List<BasicNameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("mobile", AppUser.getMOBILE(activity)));
            HttpReq ob = new HttpReq();
            response = ob.makeConnection(urls[0], HttpReq.GET, params);

            JsonParser jsonParser = new JsonParser();
            return jsonParser.getOrderHistoryInfo2(response);
        }


        @Override
        protected void onPostExecute(OrderHistoryModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressBar();
            if (result != null) {
                if(result.getOutput().equals("success")){
                    list=result.getList();
                    OrderHistoryAdapter adapter = new OrderHistoryAdapter(getActivity(),R.layout.slot_checkout,list);
                    lvMain.setAdapter(adapter);
                }else{
                    showPopupAlert("Alert",result.getOutputMsg(), PopupAlert.FRAGMENT);
                }
            }else{
                Button retry =showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                retry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hideBaseServerErrorAlertBox();
                        assignTask = new AssignTask(getActivity());
                        assignTask.execute(AppSettings.Login);
                    }
                });
            }
        }
    }
}