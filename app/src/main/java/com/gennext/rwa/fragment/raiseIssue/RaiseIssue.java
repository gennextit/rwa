package com.gennext.rwa.fragment.raiseIssue;

import android.animation.Animator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.gennext.rwa.ImageCroperActivity;
import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.fragment.PopupAlert;
import com.gennext.rwa.fragment.impContact.ImpContact;
import com.gennext.rwa.fragment.login.SignUpMobileVerify;
import com.gennext.rwa.model.AddressModel;
import com.gennext.rwa.model.ImageModel;
import com.gennext.rwa.model.RaiseTicketModel;
import com.gennext.rwa.model.SpinnerAdapter;
import com.gennext.rwa.model.SpinnerAdapter2;
import com.gennext.rwa.util.AppAnimation;
import com.gennext.rwa.util.AppSettings;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.HttpReq;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.L;
import com.gennext.rwa.util.RWA;
import com.gennext.rwa.util.WidgetAnimation;
import com.gennext.rwa.util.internet.FileBody;
import com.gennext.rwa.util.internet.MultipartEntity;
import com.gennext.rwa.util.internet.StringBody;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by Abhijit on 17-Sep-16.
 */
public class RaiseIssue extends CompactFragment {
    public static Uri mTempCropImageUri;
    Spinner spCat;

    private String sltCat;
    private EditText etReason;
    private Button btnSubmit;
    AssignTask assignTask;
    private ImageView ivAddImage;
    private LinearLayout llImagecontainer;
    private ArrayList<ImageModel> imgList;
    private static boolean instance;
    private ProgressDialog pDialog;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        instance = false;
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance() {
        RaiseIssue fragment=new RaiseIssue();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_raise_issue, container, false);
        setActionBarOption(v,"Raise Complaint");
        InitUI(v);
        if (instance) {
            L.m("savedInstanceState not null");
            if (imgList != null)
                restoreImage();
        } else {
            L.m( "savedInstanceState null");
        }
        return v;
    }

    private void InitUI(View v) {
        spCat = (Spinner) v.findViewById(R.id.sp_raise_issue_cat);
        etReason = (EditText) v.findViewById(R.id.et_raise_issue_reason);
        ivAddImage = (ImageView) v.findViewById(R.id.iv_main_event_create_addMore);
        llImagecontainer = (LinearLayout) v.findViewById(R.id.ll_main_event_create_imageContainer);

        ivAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                Intent ob=new Intent(getActivity(),ImageCroperActivity.class);
                startActivity(ob);
            }
        });

        btnSubmit = (Button) v.findViewById(R.id.btn_raise_issue);

        spCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View arg1, int position, long arg3) {
                // TODO Auto-generated method stub
                if(!adapter.getSelectedItem().toString().equals("Select Category")){
                    sltCat = adapter.getSelectedItem().toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeybord(getActivity());
                setAnimation();
            }
        });

        populateSpinnerTask();
    }

    private void setAnimation() {
        WidgetAnimation.zoomInForButton(btnSubmit, new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                executeTask();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }


    private void executeTask() {
        if(sltCat!=null){
            assignTask=new AssignTask(getActivity(),sltCat,etReason.getText().toString());
            assignTask.execute(AppSettings.ticketRaise);
        }else{
            Toast.makeText(getActivity(),"Please select category.",Toast.LENGTH_LONG).show();
        }
    }

    public void setCropedImageAndUri(Bitmap bmp,Uri uri){
        addImage(bmp,uri);
    }

    public void addImage(Bitmap bm, Uri cropImgUri) {
        if (imgList == null) {
            imgList = new ArrayList<>();
        }
        LinearLayout innerLinearLayout = new LinearLayout(getActivity());

        ImageView imageView = new ImageView(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                getResources().getDimensionPixelSize(R.dimen.login_signup_icon_width),
                LinearLayout.LayoutParams.MATCH_PARENT);
        params.rightMargin = 10;
        imageView.setLayoutParams(params);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageURI(cropImgUri);

        innerLinearLayout.addView(imageView);
        llImagecontainer.addView(innerLinearLayout);
        ImageModel ob = new ImageModel();
        ob.setImagePath(cropImgUri);
        ob.setImageBitmap(null);
        imgList.add(ob);
        instance = true;
        mTempCropImageUri=null;

    }

    private void restoreImage() {
        for (ImageModel ob : imgList) {
            LinearLayout innerLinearLayout = new LinearLayout(getActivity());
            ImageView imageView = new ImageView(getActivity());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    getResources().getDimensionPixelSize(R.dimen.login_signup_icon_width),
                    LinearLayout.LayoutParams.MATCH_PARENT);
            params.rightMargin = 10;
            imageView.setLayoutParams(params);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setImageBitmap(ob.getImageBitmap());
            innerLinearLayout.addView(imageView);
            llImagecontainer.addView(innerLinearLayout);
        }
    }

    private class AssignTask extends AsyncTask<String, Void, String> {
        Activity activity;
        private String category, reason;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity,String category, String reason) {
            this.activity = activity;
            this.category = category;
            this.reason = reason;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setCancelable(false);
            pDialog.setMessage("Sending request, Please wait...");
            pDialog.show();
//            showProgressDialog(activity,"Sending request, Please wait...");
        }

        @Override
        protected String doInBackground(String... urls) {
            MultipartEntity entity = new MultipartEntity();
            entity.addPart("category", new StringBody(category));
            if(activity!=null) {
                entity.addPart("userId", new StringBody(AppUser.getRegistrationId(activity)));
                entity.addPart("mobile", new StringBody(AppUser.getMOBILE(activity)));
            }
            entity.addPart("reason", new StringBody(reason));
            if (imgList != null) {
                for (ImageModel model : imgList) {
                    String imgPath=model.getImagePath().getPath();
                    File sourceFile = new File(imgPath);
                    entity.addPart("image[]", new FileBody(sourceFile));
                }
            } else {
                L.m("list is empty");
            }
//            List<BasicNameValuePair> params = new ArrayList<>();
//            params.add(new BasicNameValuePair("category", category));
//            if (activity != null)
//                params.add(new BasicNameValuePair("userId", AppUser.getRegistrationId(activity)));
//            params.add(new BasicNameValuePair("reason", reason));
            HttpReq ob = new HttpReq();
            return ob.makeConnection(urls[0], HttpReq.POST, entity, HttpReq.EXECUTE_TASK);
        }


        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            pDialog.dismiss();
            if (result != null) {
                if (result.equalsIgnoreCase("success")) {
                    showPopupAlert("OK","Ticket raised successfully", PopupAlert.FRAGMENT);
//                    Toast.makeText(getActivity(), "Raise ticket successfully...", Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(getActivity(), result, Toast.LENGTH_LONG).show();
                }
            } else {
                Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                retry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideBaseServerErrorAlertBox();
                        executeTask();
                    }
                });
            }
        }
    }

    private void populateSpinnerTask() {
        JsonParser jsonParser = new JsonParser();
        RaiseTicketModel result = jsonParser.getCategoryDropDownInfo(RWA.getIssueCategories(getActivity()));
        if (result != null) {
            if (result.getOutput().equals("success")) {
                SpinnerAdapter2 adapter = new SpinnerAdapter2(getActivity(), R.layout.slot_spinner);
                adapter.addAll(result.getList());
                adapter.add("Select Category");
                spCat.setAdapter(adapter);
                spCat.setSelection(adapter.getCount());
            }else{
                showPopupAlert("Alert", getResources().getString(R.string.raise_issue_error_msg), PopupAlert.FRAGMENT);
            }
        }
    }



}
