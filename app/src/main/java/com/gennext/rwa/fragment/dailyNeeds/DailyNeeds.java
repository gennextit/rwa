package com.gennext.rwa.fragment.dailyNeeds;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.model.CatAdapter;
import com.gennext.rwa.model.DailyNeedsModel;
import com.gennext.rwa.model.DailyNeedsModel;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.RWA;

import java.util.ArrayList;

/**
 * Created by Abhijit on 17-Sep-16.
 */
public class DailyNeeds extends CompactFragment {
    public static final int OPEN=1,CLOSE=2;
    GridView gridView;
    private ArrayList<DailyNeedsModel> catList;
    
    AssignTask assignTask;
    LinearLayout llActionBack,llActionCart;
    TextView tvTitle,tvCounter;
    private int countTotalItem;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }

    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }
    public void setListAndCounter(ArrayList<DailyNeedsModel> dailyNeeds,int countTotalItem) {
        this.countTotalItem=countTotalItem;
        this.catList=dailyNeeds;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_daily_needs, container, false);
        setActionBarOpt(v);
        InitUI(v);
        return v;
    }

    private void setActionBarOpt(View v) {
        llActionBack = (LinearLayout) v.findViewById(R.id.ll_actionbar_back);
        llActionCart = (LinearLayout) v.findViewById(R.id.ll_actionbar_cart);
        tvCounter = (TextView) v.findViewById(R.id.tv_subcat_count);
        tvTitle = (TextView) v.findViewById(R.id.actionbar_title);
        setActionBarOption();
        setActionBarTitle("Shopping");

        llActionCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCheckOutPage();
            }
        });
    }

    private void openCheckOutPage() {
        Checkout checkout = new Checkout();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(android.R.id.content, checkout, "checkout");
        transaction.addToBackStack("checkout");
        transaction.commit();
    }

    private void setActionBarTitle(String title) {
        tvTitle.setText(title);
        if(countTotalItem!=0){
            tvCounter.setText(String.valueOf(countTotalItem));
            llActionCart.setVisibility(View.VISIBLE);
        }else{
            llActionCart.setVisibility(View.INVISIBLE);
        }
    }

    public void setActionBarOption() {
        llActionBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                  getActivity().finish();
            }
        });
    }

    private void InitUI(View v) {
        setProgressBar(v);
        gridView = (GridView) v.findViewById(R.id.gridView1);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                if(catList!=null){
                    String catId=catList.get(position).getCategoryID();
                    String catName=catList.get(position).getCategoryName();
                    ArrayList<DailyNeedsModel> subCatList=catList.get(position).getSubCategoryList();
                    if(subCatList!=null&&subCatList.size()>0)
                    ((SubCategory)getActivity()).openSubCategoty(catId,catName,subCatList,position,DailyNeeds.this);
                }
            }
        });
        if(catList!=null){
            hideProgressBar();
            CatAdapter catAdapter = new CatAdapter(getActivity(),R.layout.custom_home_menu, catList);
            gridView.setAdapter(catAdapter);
        }else{
            assignTask =new AssignTask(getActivity());
            assignTask.execute();
        }
    }



    private class AssignTask extends AsyncTask<Void, Void, DailyNeedsModel> {
        Activity activity;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;

        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;

        }

        public AssignTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
        }

        @Override
        protected DailyNeedsModel doInBackground(Void... urls) {
            JsonParser jsonParser = new JsonParser();
            if(activity!=null) {
                return jsonParser.getDailyNeedsInfo(RWA.getDailyNeeds(activity));
            }else{
                return null;
            }
        }


        @Override
        protected void onPostExecute(DailyNeedsModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressBar();
            if (result != null) {
                if(result.getOutput().equals("success")){
                    catList=result.getCatList();
                    ((SubCategory)getActivity()).setDailyNeedsList(catList);
                    CatAdapter catAdapter = new CatAdapter(getActivity(),R.layout.custom_home_menu, catList);
                    gridView.setAdapter(catAdapter);
                }else{

                }
            }else{

            }

        }
    }
}
