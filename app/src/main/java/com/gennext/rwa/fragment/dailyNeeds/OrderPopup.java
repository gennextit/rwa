package com.gennext.rwa.fragment.dailyNeeds;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.model.OrderHistoryModel;
import com.gennext.rwa.model.OrderPopupAdapter;

import java.util.ArrayList;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class OrderPopup extends CompactFragment {
    private TextView tvTrackId, tvName,tvMobile,tvAddress;
    private Button btnOK;
    private String orderTrackId,orderShipName,orderPhone,orderShipAddress;
    private FragmentManager manager;
    private ArrayList<OrderHistoryModel> orderList;
    private ListView lvMain;

    public void setDetail(String orderTrackId, String orderShipName, String orderPhone, String orderShipAddress, ArrayList<OrderHistoryModel> orderList) {
        this.orderTrackId=orderTrackId;
        this.orderShipName=orderShipName;
        this.orderPhone=orderPhone;
        this.orderShipAddress=orderShipAddress;
        this.orderList=orderList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_popup_alert_order, container, false);
        manager = getFragmentManager();
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        tvTrackId = (TextView) v.findViewById(R.id.tv_popup_trackId);
        lvMain = (ListView) v.findViewById(R.id.lv_main);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.frag_popup_alert_order_header, null, false);
        tvName = (TextView) listHeaderView.findViewById(R.id.tv_popup_name);
        tvMobile = (TextView) listHeaderView.findViewById(R.id.tv_popup_mobile);
        tvAddress = (TextView) listHeaderView.findViewById(R.id.tv_popup_address);

        if(orderTrackId!=null)
            tvTrackId.setText("Order Track Id : "+orderTrackId);
        if(orderShipName!=null)
            tvName.setText("Name : "+orderShipName);
        if(orderPhone!=null)
            tvMobile.setText("Mobile : "+orderPhone);
        if(orderShipAddress!=null)
            tvAddress.setText("Address : "+orderShipAddress);

        lvMain.addHeaderView(listHeaderView);

        if(orderList==null){
            orderList=new ArrayList<>();
        }
        OrderPopupAdapter adapter = new OrderPopupAdapter(getActivity(),R.layout.slot_checkout, orderList);
        lvMain.setAdapter(adapter);

        btnOK = (Button) v.findViewById(R.id.btn_popup);
        ImageView ivImage = (ImageView) v.findViewById(R.id.iv_profile);
        ImageView ivClose = (ImageView) v.findViewById(R.id.iv_close);
        LinearLayout llClose = (LinearLayout) v.findViewById(R.id.ll_whitespace);

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.popBackStack();
            }
        });
        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.popBackStack();
            }
        });


        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.popBackStack();
            }
        });
    }



}