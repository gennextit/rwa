package com.gennext.rwa.fragment.profile;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.rwa.MainActivity;
import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.model.AddressModel;
import com.gennext.rwa.model.SpinnerAdapter;
import com.gennext.rwa.util.AppAnimation;
import com.gennext.rwa.util.AppSettings;
import com.gennext.rwa.util.AppTokens;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.FieldValidation;
import com.gennext.rwa.util.HttpReq;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.RWA;
import com.gennext.rwa.util.Utility;

import com.gennext.rwa.util.internet.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhijit on 13-Oct-16.
 */

public class SpauseDetail extends CompactFragment implements View.OnClickListener{
    //    AssignTask assignTask;
    EditText etSpName, etSpMobile,etSpEmail,etSpOccupation;
    Button btnNext, btnSkip, btnPrev;
    RelativeLayout llBtnContainer;

    ProgressBar progressBar;
    private UpdateProfileAllFields parentRefrence;


    public static SpauseDetail newInstance(UpdateProfileAllFields parentRefrence) {
        SpauseDetail fragment=new SpauseDetail();
        fragment.parentRefrence = parentRefrence;
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_spouse_detail, container, false);
        InitUI(v);
        return v;
    }


    private void InitUI(View v) {
//        spFloor = (Spinner) v.findViewById(R.id.sp_update_profile_floor);
        llBtnContainer = (RelativeLayout) v.findViewById(R.id.ll_button_container);
        etSpName = (EditText) v.findViewById(R.id.et_update_spouse_name);
        etSpMobile = (EditText) v.findViewById(R.id.et_update_spouse_phone);
        etSpEmail = (EditText) v.findViewById(R.id.et_update_spouse_email);
        etSpOccupation = (EditText) v.findViewById(R.id.et_update_spouse_occupation);

        btnNext = (Button) v.findViewById(R.id.btn_update_spouse_next);
        btnSkip = (Button) v.findViewById(R.id.btn_update_spouse_skip);
        btnPrev = (Button) v.findViewById(R.id.btn_update_spouse_prev);
        btnNext.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnSkip.setOnClickListener(this);

        progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);


        String[] spouseArr = AppUser.getSpouseData(getActivity());
        etSpName.setText(spouseArr[0]);
        etSpMobile.setText(spouseArr[1]);
        etSpEmail.setText(spouseArr[2]);
        etSpOccupation.setText(spouseArr[3]);

    }


    private void executeTask() {
//        assignTask = new AssignTask(getActivity(), etVehicleNo.getText().toString()
//                , sltVehicleType,etVehicleModel.getText().toString());
//        assignTask.execute(AppSettings.UpdateProfile);
        setSpouseData(etSpName.getText().toString(), etSpMobile.getText().toString()
                        , etSpEmail.getText().toString(),etSpOccupation.getText().toString());
    }

    private void setSpouseData(String spName, String spMobile, String spEmail, String spOccupation) {
        if(parentRefrence!=null){
            parentRefrence.setSpouseData(spName, spMobile, spEmail,spOccupation);
        }
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_update_spouse_next:
                hideKeybord(getActivity());
                executeTask();
                break;
            case R.id.btn_update_spouse_prev:
                hideKeybord(getActivity());
                if(parentRefrence!=null)
                    parentRefrence.displayPersonalScreen();
                break;
            case R.id.btn_update_spouse_skip:
                hideKeybord(getActivity());
//                executeTask();
                if(parentRefrence!=null){
                    parentRefrence.restartMainActivity();
                }
                break;
        }
    }


//    private class AssignTask extends AsyncTask<String, Void, String> {
//        Activity activity;
//        private String vehicleNo, vehicleType, vehicleModel;
//
//        public void onAttach(Activity activity) {
//            // TODO Auto-generated method stub
//            this.activity = activity;
//        }
//
//        public void onDetach() {
//            // TODO Auto-generated method stub
//            this.activity = null;
//        }
//
//        public AssignTask(Activity activity, String vehicleNo, String vehicleType, String vehicleModel) {
//            this.activity = activity;
//            this.vehicleNo = vehicleNo;
//            this.vehicleType = vehicleType;
//            this.vehicleModel = vehicleModel;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressBar.setVisibility(View.VISIBLE);
//            llBtnContainer.setVisibility(View.INVISIBLE);
//        }
//
//        @Override
//        protected String doInBackground(String... urls) {
//
////            MultipartEntity entity = new MultipartEntity();
////            entity.addPart("playerId", new StringBody(LoadPref(Buddy.PlayerId)));
//
//            List<BasicNameValuePair> params = new ArrayList<>();
//            params.add(new BasicNameValuePair("vehicleNo", vehicleNo));
//            params.add(new BasicNameValuePair("vehicleType", vehicleType));
//            params.add(new BasicNameValuePair("vehicleModel", vehicleModel));
//            HttpReq ob = new HttpReq();
//            return ob.makeConnection(urls[0], HttpReq.POST, params, HttpReq.EXECUTE_TASK);
//        }
//
//
//        @Override
//        protected void onPostExecute(String result) {
//            // TODO Auto-generated method stub
//            super.onPostExecute(result);
//            progressBar.setVisibility(View.INVISIBLE);
//            llBtnContainer.setVisibility(View.VISIBLE);
//            if (result != null) {
//                if (result.equalsIgnoreCase("success")) {
//                    Utility.SavePref(activity, AppTokens.SessionProfile, "success");
//                    Toast.makeText(getActivity(), "Update record successfully...", Toast.LENGTH_LONG).show();
//                    Intent intent = new Intent(getActivity(), MainActivity.class);
//                    startActivity(intent);
//                    getActivity().finish();
//                } else {
//                    Toast.makeText(getActivity(), result, Toast.LENGTH_LONG).show();
//                }
//            } else {
//                Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
//                retry.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        hideBaseServerErrorAlertBox();
//                        executeTask();
//                    }
//                });
//            }
//        }
//    }

}
