package com.gennext.rwa.fragment.login;

import android.Manifest;
import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gennext.rwa.LoadData;
import com.gennext.rwa.LoginActivity;
import com.gennext.rwa.MainActivity;
import com.gennext.rwa.R;
import com.gennext.rwa.TermAndConditionActivity;
import com.gennext.rwa.UpdateProfileActivity;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.model.JsonModel;
import com.gennext.rwa.util.AppSettings;
import com.gennext.rwa.util.AppTokens;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.HttpReq;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.Utility;
import com.gennext.rwa.util.WidgetAnimation;
import com.gennext.rwa.util.internet.BasicNameValuePair;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.List;

public class SignUpPassword extends CompactFragment {

    private EditText etMobile,etPassword;
    private Button btnSignIn;
    private ProgressBar progressBar;
    HttpTask httpTask;
    FragmentManager mannager;
    private LinearLayout llFormDialog;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (httpTask != null) {
            httpTask.onAttach(activity);
        }

    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (httpTask != null) {
            httpTask.onDetach();
        }
    }

    public static SignUpPassword newInstance() {
        SignUpPassword fragment=new SignUpPassword();
        return fragment;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.fragment_password_singup, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        llFormDialog = (LinearLayout) view.findViewById(R.id.ll_form_dialog);
        etMobile = (EditText) view.findViewById(R.id.et_mobile_signup);
        etPassword = (EditText) view.findViewById(R.id.et_mobile_password);
        btnSignIn = (Button) view.findViewById(R.id.btn_mobile_signin);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        LinearLayout lltandc = (LinearLayout) view.findViewById(R.id.lltandc);
        lltandc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),TermAndConditionActivity.class));
            }
        });

        mannager = getFragmentManager();

        btnSignIn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                hideKeybord(getActivity());
                if(isOnline()) {
                    httpTask = new HttpTask(getActivity(), btnSignIn, progressBar, etMobile.getText().toString(), etPassword.getText().toString());
                    httpTask.execute(AppSettings.LoginPass);
                }else{
                    showInternetAlertBox(getActivity());
                }
            }
        });
        WidgetAnimation.zoomOutForButton(llFormDialog, new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if(getContext()!=null) {
//                    setSMSPermission();
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    private void setSMSPermission() {
        new TedPermission(getActivity())
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }
                })
                .setDeniedMessage(getString(R.string.if_you_reject_permission_you_can_not_use_this_service_please_turn_on_permissions))
                .setPermissions(Manifest.permission.RECEIVE_SMS)
                .check();
    }

    private class HttpTask extends AsyncTask<String, Void, JsonModel> {
        Button btn;
        ProgressBar pBar;
        Activity activity;
        String name, password, mobile;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;

        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;

        }

        public HttpTask(Activity activity, Button btn, ProgressBar pBar, String mobile, String password) {
            this.activity = activity;
            this.btn = btn;
            this.pBar = pBar;
            this.mobile = mobile;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pBar.setVisibility(View.VISIBLE);
            btn.setVisibility(View.GONE);
        }

        @Override
        protected JsonModel doInBackground(String... urls) {
            String response = "error";

            List<BasicNameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("username", mobile));
            params.add(new BasicNameValuePair("password", password));

            HttpReq ob = new HttpReq();
            response = ob.makeConnection(urls[0], HttpReq.POST, params);
            JsonParser jsonParser=new JsonParser();
            JsonModel jsonModel=jsonParser.parseLoginPassword(response);
            return jsonModel;
        }

        @Override
        protected void onPostExecute(JsonModel result) {
            if (activity != null) {
                pBar.setVisibility(View.GONE);
                btn.setVisibility(View.VISIBLE);
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        AppUser.setMobile(activity,mobile);
                        AppUser.setRegistrationId(activity,result.getOutputMsg());

                        Utility.SavePref(getActivity(), AppTokens.SessionSignup, "success");
                        Utility.SavePref(getActivity(), AppTokens.SessionProfile, "success");
                        activity.startService(new Intent(activity, LoadData.class));

                        Intent intent=new Intent(activity,MainActivity.class);
                        startActivity(intent);
                        activity.finish();
                    } else {
                        Toast.makeText(getActivity(), result.getOutputMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (dialog != null) dialog.dismiss();
                            httpTask = new SignUpPassword.HttpTask(getActivity(), btnSignIn, progressBar, etMobile.getText().toString(), etPassword.getText().toString());
                            httpTask.execute(AppSettings.Registration);
                        }
                    });
                }
            }
        }
    }

}
