package com.gennext.rwa.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gennext.rwa.R;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.RWA;

/**
 * Created by Abhijit on 14-Nov-16.
 */
public class About extends CompactFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_about, container, false);
        setActionBarOption(v,"About");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        TextView tvAbout=(TextView)v.findViewById(R.id.tv_about_app);
        tvAbout.setText(RWA.getABOUTAPP(getActivity()));
    }
}
