package com.gennext.rwa.fragment.raiseIssue;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class TicketHistoryPopup extends CompactFragment {
    private TextView tvTicketNo, tvReason;
    private Button btnOK;
    private String ticketNo,reason;
    private FragmentManager manager;

    public void setDetail(String ticketNo, String reason) {
        this.ticketNo=ticketNo;
        this.reason=reason;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_popup_alert_ticket, container, false);
        manager = getFragmentManager();
//        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(getActivity()));
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        tvTicketNo = (TextView) v.findViewById(R.id.tv_popup_ticketNo);
        tvReason = (TextView) v.findViewById(R.id.tv_popup_reason);
        btnOK = (Button) v.findViewById(R.id.btn_popup);
        ImageView ivImage = (ImageView) v.findViewById(R.id.iv_profile);
        ImageView ivClose = (ImageView) v.findViewById(R.id.iv_close);
        LinearLayout llClose = (LinearLayout) v.findViewById(R.id.ll_whitespace);

        if(ticketNo!=null)
        tvTicketNo.setText("Ticket No : "+ticketNo);
        if(reason!=null)
            tvReason.setText(reason);

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.popBackStack();
            }
        });
        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.popBackStack();
            }
        });

//        ImageLoader imageLoader = ImageLoader.getInstance();
//        DisplayImageOptions options = new DisplayImageOptions.Builder()
//                .showImageOnLoading(R.drawable.profile)
//                .showImageForEmptyUri(R.mipmap.error_connection)
//                .showImageOnFail(R.mipmap.error_connection)
//                .cacheInMemory(true)
//                .cacheOnDisk(true)
//                .considerExifParams(true)
//                .bitmapConfig(Bitmap.Config.RGB_565)
//                .build();
//
//        imageLoader.displayImage(imageUrl, ivImage, options);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (finishType == ACTIVITY) {
//                    getActivity().finish();
//                } else  if (finishType == POPUP_DIALOG) {
                manager.popBackStack();
//                serviceProvider.sendFeedBack(categoryId, serviceProviderId, String.valueOf(ratingBar.getRating()), etFeedback.getText().toString());
//                }else {
//                    manager.popBackStack();
//                    manager.popBackStack();
//                }
            }
        });
    }



}