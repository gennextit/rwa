package com.gennext.rwa.fragment;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.gennext.rwa.R;
import com.gennext.rwa.model.ResponseModel;
import com.gennext.rwa.util.AnimationClass;
import com.gennext.rwa.util.AppSettings;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.HttpReq;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.internet.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class AppFeedback extends CompactFragment {
    private Button btnOK;
    private FragmentManager manager;
    AssignTask assignTask;
    private EditText etFeedback,etEmail;
    private RatingBar rbRating;
    private RelativeLayout RLView;
    private AnimationClass anim;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_app_feedback, container, false);
        manager = getFragmentManager();
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
//        tvTitle = (TextView) v.findViewById(R.id.tv_popup_title);
        btnOK = (Button) v.findViewById(R.id.btn_popup);
        RLView = (RelativeLayout) v.findViewById(R.id.llappFeedback);
        ImageView ivImage = (ImageView) v.findViewById(R.id.iv_profile);
        LinearLayout llClose = (LinearLayout) v.findViewById(R.id.ll_whitespace);
        etFeedback = (EditText) v.findViewById(R.id.et_alert_feedback);
        etEmail = (EditText) v.findViewById(R.id.et_alert_email);
        rbRating = (RatingBar) v.findViewById(R.id.ratingBar);

        anim=new AnimationClass();
        anim.setPopupAnimation(RLView);
//        tvTitle.setText(title);

        String emailId=AppUser.getEmailAddress(getActivity());
        if(emailId.equals("")){
            etEmail.setVisibility(View.VISIBLE);
        }else {
            etEmail.setText(emailId);
            etEmail.setVisibility(View.GONE);
        }

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                manager.popBackStack();
            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                sendFeedback(String.valueOf(rbRating.getRating()), etFeedback.getText().toString(),
                        AppUser.getRegistrationId(getActivity()),etEmail.getText().toString());
            }
        });
    }

    private void sendFeedback(String rating, String feedback, String regId, String emailId) {
        assignTask = new AssignTask(getActivity(),rating,feedback,regId,emailId);
        assignTask.execute(AppSettings.sendFeedback);
    }

    private class AssignTask extends AsyncTask<String, Void, ResponseModel> {
        Activity activity;
        private String rating,feedback,regId,emailId;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity, String rating, String feedback, String regId, String emailId) {
            this.rating=rating;
            this.feedback=feedback;
            this.regId=regId;
            this.activity = activity;
            this.emailId=emailId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity,"Processing please wait...");
        }

        @Override
        protected ResponseModel doInBackground(String... urls) {
            List<BasicNameValuePair> params = new ArrayList<>();
            if(activity!=null) {
                params.add(new BasicNameValuePair("userId", AppUser.getRegistrationId(activity)));
                params.add(new BasicNameValuePair("mobile", AppUser.getMOBILE(activity)));
            }
            params.add(new BasicNameValuePair("emailId", emailId));
            params.add(new BasicNameValuePair("rating", rating));
            params.add(new BasicNameValuePair("feedback", feedback));
            HttpReq req=new HttpReq();
            String response = req.makeConnection(urls[0], HttpReq.POST,params);
            JsonParser jsonParser = new JsonParser();
            return jsonParser.parseFeedInfo(response);
        }

        @Override
        protected void onPostExecute(ResponseModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if(activity!=null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        showPopupAlert("Feedback", result.getOutputMsg(), PopupAlert.FRAGMENT);
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Feedback", result.getOutputMsg(), PopupAlert.FRAGMENT);
                    } else {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.FRAGMENT);
                    }
                } else {
                    Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            sendFeedback(String.valueOf(rating), feedback, regId, emailId);
                        }
                    });
                }
            }
        }
    }
}