package com.gennext.rwa.fragment.dailyNeeds;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.rwa.BaseActivity;
import com.gennext.rwa.R;
import com.gennext.rwa.model.DailyNeedsModel;
import com.gennext.rwa.model.DataTransferInterface;
import com.gennext.rwa.model.tabLib.PagerSlidingTabStrip;
import com.gennext.rwa.model.tabLib.SuperAwesomeCardFragment;

import java.util.ArrayList;

import static com.gennext.rwa.fragment.dailyNeeds.DailyNeeds.CLOSE;
import static com.gennext.rwa.fragment.dailyNeeds.DailyNeeds.OPEN;

public class SubCategory extends BaseActivity implements DataTransferInterface {
    private PagerSlidingTabStrip tabs;
    private ViewPager pager;
    private MyPagerAdapter adapter;
    public ArrayList<DailyNeedsModel> subCatList;
    private LinearLayout Checkout;
    public int countTotalItem = 0;
    public float sumTotalPrice = 0.0f;
    private TextView tvCounter, tvSumPrice;
    private ImageView ivRupee;
    private String catId, catName, subCatJson;
    FragmentManager manager;
    LinearLayout llActionBack;
    TextView tvTitle;
    public ArrayList<DailyNeedsModel> dailyNeedsList;
    int dailyNeedsStatus = CLOSE;
    private LinearLayout llProgress;
    public int subCatListPos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcat);

        manager = getSupportFragmentManager();
        catId = getIntent().getStringExtra("catId");
        catName = getIntent().getStringExtra("catName");
        subCatJson = getIntent().getStringExtra("subCatList");
        initUi();
    }

    private void initUi() {
        setActionBarOpt();
        llProgress = (LinearLayout) findViewById(R.id.ll_progress);
        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        pager = (ViewPager) findViewById(R.id.pager);
        Checkout = (LinearLayout) findViewById(R.id.llcheckout);
        tvCounter = (TextView) findViewById(R.id.tv_subcat_count);
        tvSumPrice = (TextView) findViewById(R.id.tv_subcat_price);
        ivRupee = (ImageView) findViewById(R.id.iv_subcat_rupeeLogo);
        Checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                openCheckOutPage();
//                if(!LoadPref(AppTokens.Mobile).equals("")){
//                    Intent ob=new Intent(SubCategoryActivity.this,CheckoutActivity.class);
//                    startActivity(ob);
//
//                }else{
//                    Intent ob=new Intent(SubCategoryActivity.this,LogIn.class);
//                    ob.putExtra("from", "subcat");
//                    startActivity(ob);
//                }
            }
        });
        hideCheckOutOption();
        showDailyNeedsList();
    }

    private void openCheckOutPage() {
        Checkout checkout = new Checkout();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(android.R.id.content, checkout, "checkout");
        transaction.addToBackStack("checkout");
        transaction.commit();
    }

    public void hideCheckOutOption() {
        if(countTotalItem==0) {
            Checkout.setVisibility(View.GONE);
        }
    }

    public void showCheckOutOption() {
        tvCounter.setText(String.valueOf(countTotalItem));
        tvSumPrice.setText(String.valueOf(sumTotalPrice));

        Checkout.setVisibility(View.VISIBLE);
    }

    private void setActionBarOpt() {
        llActionBack = (LinearLayout) findViewById(R.id.ll_actionbar_back);
        tvTitle = (TextView) findViewById(R.id.actionbar_title);
        setActionBarOption();

    }

    private void setActionBarTitle(String title) {
        tvTitle.setText(title);
    }

    public void setActionBarOption() {
        llActionBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DailyNeeds dailyNeeds = (DailyNeeds) manager.findFragmentByTag("dailyNeeds");
                if (dailyNeeds != null) {
                    dailyNeedsStatus = OPEN;
                    dailyNeeds.setListAndCounter(dailyNeedsList,countTotalItem);
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.attach(dailyNeeds);
                    transaction.commit();
                }
            }
        });
    }


    private void showDailyNeedsList() {
        dailyNeedsStatus = OPEN;
        DailyNeeds dailyNeeds = new DailyNeeds();
        dailyNeeds.setListAndCounter(dailyNeedsList,countTotalItem);
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(android.R.id.content, dailyNeeds, "dailyNeeds");
        transaction.addToBackStack("dailyNeeds");
        transaction.commit();
    }


    public void openSubCategoty(String catId, String catName, ArrayList<DailyNeedsModel> subCatList, int subCatListPos, DailyNeeds dailyNeeds) {
        setActionBarTitle(catName);
        this.subCatList=subCatList;
        this.subCatListPos=subCatListPos;
        if (dailyNeeds != null) {
            dailyNeedsStatus = CLOSE;
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.detach(dailyNeeds);
            transaction.commit();
        }
        setListWithTabs(subCatList);
    }

    private void setListWithTabs(ArrayList<DailyNeedsModel> subCatList) {

        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        adapter = new MyPagerAdapter(getSupportFragmentManager(), subCatList);
        pager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        pager.setPageMargin(pageMargin);
        tabs.setTextColor(getResources().getColor(R.color.white));
        tabs.setIndicatorColor(getResources().getColor(R.color.colorAccent));
        tabs.setAllCaps(false);
        tabs.setShouldExpand(true);
        tabs.setViewPager(pager);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }

    public void setDailyNeedsList(ArrayList<DailyNeedsModel> list) {
        this.dailyNeedsList = list;
    }


//    private class LoadSubCat extends AsyncTask<String, Void, DailyNeedsModel> {
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            llProgress.setVisibility(View.VISIBLE);
//        }
//
//        @Override
//        protected DailyNeedsModel doInBackground(String... params) {
//
//            JsonParser jsonParser = new JsonParser(SubCategory.this);
//            return jsonParser.getSubCatList(params[2]);
//        }
//
//        @Override
//        protected void onPostExecute(DailyNeedsModel result) {
//            // TODO Auto-generated method stub
//            super.onPostExecute(result);
//            llProgress.setVisibility(View.GONE);
//            final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
//                    .getDisplayMetrics());
//            switch (result.getOutput()) {
//                case "success":
//                    uCatList = result.getList();
//                    adapter = new MyPagerAdapter(getSupportFragmentManager(), uCatList);
//                    pager.setAdapter(adapter);
//                    pager.setPageMargin(pageMargin);
//                    tabs.setTextColor(getResources().getColor(R.color.white));
//                    tabs.setIndicatorColor(getResources().getColor(R.color.colorAccent));
//                    tabs.setAllCaps(false);
////                    tabs.setUnderlineColor(getResources().getColor(R.color.colorAccent));
//                    tabs.setShouldExpand(true);
//                    tabs.setViewPager(pager);
//                    break;
//                case "failure":
//                    uCatList = result.getList();
//                    adapter = new MyPagerAdapter(getSupportFragmentManager(), uCatList);
//                    pager.setAdapter(adapter);
//                    pager.setPageMargin(pageMargin);
//                    tabs.setViewPager(pager);
//
//                    break;
//                case "error":
//                    break;
//                default:
//
//            }
//
//
//        }
//    }


    public class MyPagerAdapter extends FragmentPagerAdapter {
        ArrayList<DailyNeedsModel> subCatList;

        public MyPagerAdapter(FragmentManager fm, ArrayList<DailyNeedsModel> subCatList) {
            super(fm);
            this.subCatList = subCatList;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return subCatList.get(position).getSubCategoryName();
        }

        @Override
        public int getCount() {
            return subCatList.size();
            //return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {
            return SuperAwesomeCardFragment.newInstance(position);
        }


    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    public void setValues(int count, float sum) {
        // TODO Auto-generated method stub
//        if(AppTokens.countTotalItem==0){
//            tvCounter.setVisibility(View.INVISIBLE);
//        }else if(AppTokens.countTotalItem>=0){
//            tvCounter.setVisibility(View.VISIBLE);
//            tvCounter.setText(String.valueOf(AppTokens.countTotalItem));
//        }
//        if(AppTokens.sumTotalPrice==0){
//            tvSumPrice.setVisibility(View.INVISIBLE);
//            ivRupee.setVisibility(View.INVISIBLE);
//        }else if(AppTokens.sumTotalPrice>=0){
//            tvSumPrice.setVisibility(View.VISIBLE);
//            ivRupee.setVisibility(View.VISIBLE);
//            tvSumPrice.setText(String.valueOf(AppTokens.sumTotalPrice));
//        }
    }

    @Override
    public void onBackPressed() {
        int count=manager.getBackStackEntryCount();
        if ( count > 0) {
            if(count>=2){
                manager.popBackStack();
            }else{
                if (dailyNeedsStatus == CLOSE) {
                    DailyNeeds dailyNeeds = (DailyNeeds) manager.findFragmentByTag("dailyNeeds");
                    if (dailyNeeds != null) {
                        dailyNeedsStatus = OPEN;
                        dailyNeeds.setListAndCounter(dailyNeedsList,countTotalItem);
                        FragmentTransaction transaction = manager.beginTransaction();
                        transaction.attach(dailyNeeds);
                        transaction.commit();
                    }
                } else {
                    finish();
                }
            }
        } else {
            super.onBackPressed();
            finish();
        }

    }
}
