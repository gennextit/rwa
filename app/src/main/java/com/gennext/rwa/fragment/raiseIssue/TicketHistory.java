package com.gennext.rwa.fragment.raiseIssue;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.fragment.PopupAlert;
import com.gennext.rwa.model.TicketHistoryAdapter;
import com.gennext.rwa.model.TicketHistoryModel;
import com.gennext.rwa.util.AppSettings;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.HttpReq;
import com.gennext.rwa.util.JsonParser;

import com.gennext.rwa.util.internet.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhijit on 18-Oct-16.
 */

public class TicketHistory extends CompactFragment {
    AssignTask assignTask;
    private ListView lvMain;
    ArrayList<TicketHistoryModel>list;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_ticket_history, container, false);
        setActionBarOption(v,"Ticket History");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        lvMain=(ListView)v.findViewById(R.id.expandableListView1);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if(list!=null){
                    showPopupHistory(list.get(position).getTicketNumber(),list.get(position).getReason());
                }
            }
        });
        setProgressBar(v);
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.ticketRaisedList);
    }

    private void showPopupHistory(String ticketNo,String reason) {
        TicketHistoryPopup ticketHistoryPopup = new TicketHistoryPopup();
        ticketHistoryPopup.setDetail(ticketNo,reason);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(android.R.id.content, ticketHistoryPopup, "ticketHistoryPopup");
        transaction.addToBackStack("ticketHistoryPopup");
        transaction.commit();
    }

    private class AssignTask extends AsyncTask<String, Void, TicketHistoryModel> {
        Activity activity;
        private String response;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
        }

        @Override
        protected TicketHistoryModel doInBackground(String... urls) {
            List<BasicNameValuePair> params = new ArrayList<>();
            if(activity!=null){
                params.add(new BasicNameValuePair("userId", AppUser.getRegistrationId(activity)));
                params.add(new BasicNameValuePair("mobile", AppUser.getMOBILE(activity)));
            }
            HttpReq ob = new HttpReq();
            response = ob.makeConnection(urls[0], HttpReq.GET, params);

            JsonParser jsonParser = new JsonParser();
            return jsonParser.getTicketHistoryInfo2(response);
        }


        @Override
        protected void onPostExecute(TicketHistoryModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressBar();
            if (result != null) {
                if(result.getOutput().equals("success")){
                    list=result.getList();
                    TicketHistoryAdapter adapter = new TicketHistoryAdapter(getActivity(),R.layout.slot_checkout,list);
                    lvMain.setAdapter(adapter);
                }else{
                    showPopupAlert("Alert",result.getOutputMsg(), PopupAlert.FRAGMENT);
                }
            }else{
                Button retry =showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                retry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hideBaseServerErrorAlertBox();
                        assignTask = new AssignTask(getActivity());
                        assignTask.execute(AppSettings.Login);
                    }
                });
            }
        }
    }
}