package com.gennext.rwa.fragment.gallery;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.util.CameraUtility;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.L;
import com.gennext.rwa.util.Utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Abhijit on 15-Oct-16.
 */

public class MasterImageView extends CompactFragment implements View.OnClickListener{
//    ImageLoader imageLoader;
    private String imgUrl;
    private ImageView ivDownload,ivClose,ivShare;
    private ProgressDialog progressDialog;
    private InputStream inputStream;
    private int totalSize;
    private LinearLayout progressBar;
    private ImageView ivMasterView;


    public void setImage(String imgUrl) {
        this.imgUrl = imgUrl;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_master_image_view, container, false);
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        ivMasterView=(ImageView)v.findViewById(R.id.iv_image);
        ivDownload=(ImageView)v.findViewById(R.id.iv_download);
        ivShare=(ImageView)v.findViewById(R.id.iv_share);
        ivClose=(ImageView)v.findViewById(R.id.iv_close);
        progressBar=(LinearLayout)v.findViewById(R.id.ll_progress);

        ivDownload.setOnClickListener(this);
        ivShare.setOnClickListener(this);
        ivClose.setOnClickListener(this);

        Glide.with(getActivity())
                .load(imgUrl)
                .placeholder(R.drawable.default_image)
                .error(R.drawable.default_image)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(ivMasterView);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_download:
                AssignTask assignTask = new AssignTask(getActivity(),AssignTask.GET_SIZE);
                assignTask.execute(imgUrl,"Image");
                break;
            case R.id.iv_share:
                shareImageOption(getActivity());
                break;
            case R.id.iv_close:
                getFragmentManager().popBackStack();
                break;
        }

    }

    private void shareImageOption(Activity activity) {
        Bitmap bitmap=CameraUtility.screenShot(activity,ivMasterView);
        Uri mCropImageUri;
        if(bitmap!=null){
           mCropImageUri = CameraUtility.getOutputMediaFileUri();
            CameraUtility.saveImageExternal(activity,bitmap,mCropImageUri);

            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setData(mCropImageUri);
            shareIntent.setType("image/png");
            shareIntent.putExtra(Intent.EXTRA_STREAM, mCropImageUri);
            startActivity(Intent.createChooser(shareIntent, "Share Image"));
//            activity.startActivityForResult(Intent.createChooser(shareIntent, "Share Via"), Navigator.REQUEST_SHARE_ACTION);

        }

//        try {
//            Intent i = new Intent(Intent.ACTION_SEND);
//            i.setType("text/plain");
//            i.putExtra(Intent.EXTRA_SUBJECT, "RWA event image");
//            i.putExtra(Intent.EXTRA_TEXT, imgUrl);
//            startActivity(Intent.createChooser(i, "choose one"));
//        } catch (Exception e) { // e.toString();
//        }
    }


    public class AssignTask extends AsyncTask<String, String, String> {

        private Activity activity;
        private static final int MEGABYTE = 1024 * 1024;
        private String fileName="Image";
        public static final int GET_SIZE = 1, DOWNLOAD = 2;
        private int task;
        String fileSize;
        private String url;
        public AssignTask(Activity activity, int task) {
            onAttach(activity);
            this.task = task;
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        public void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            switch (task) {
                case GET_SIZE:
                    showPDialog2(activity, "Processing, please wait... ");
                    break;
                case DOWNLOAD:
                    showPDialog(activity, "Downloading file, please wait... ");
                    break;
            }

        }

        @Override
        protected String doInBackground(String... urls) {
            String fileUrl = urls[0];
            this.url=urls[0];

            switch (task) {
                case GET_SIZE:
                    int count;
                    fileName = urls[1];
                    try {

                        URL url = new URL(fileUrl);
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        //urlConnection.setRequestMethod("GET");
                        //urlConnection.setDoOutput(true);
                        urlConnection.connect();

                        inputStream = urlConnection.getInputStream();
                        totalSize = urlConnection.getContentLength();
                        fileSize = Utility.bytesIntoHumanReadable(totalSize);
                        if(fileSize!=null){
                            return "success";
                        }

                    } catch (MalformedURLException e) {
                        JsonParser.ERRORMESSAGE = e.toString();
                        return null;
                    } catch (IOException e) {
                        JsonParser.ERRORMESSAGE = e.toString();
                        return null;
                    }
                    break;
                case DOWNLOAD:
                    try {
                        File directory = Utility.getExternalDirectory(Utility.getNameWithTimeStamp(fileName) + ".png");
                        FileOutputStream fileOutputStream = new FileOutputStream(directory);
                        String fileSize = Utility.bytesIntoHumanReadable(totalSize);
                        byte[] buffer = new byte[MEGABYTE];
                        long total = 0;

                        int bufferLength = 0;
                        while ((bufferLength = inputStream.read(buffer)) > 0) {
                            total += bufferLength;
                            // publishing the progress....
                            // After this onProgressUpdate will be called
                            publishProgress("" + (int) ((total * 100) / totalSize));


                            fileOutputStream.write(buffer, 0, bufferLength);

                        }
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }

            return "success";
        }

        @Override
        protected void onProgressUpdate(String... progress) {
            super.onProgressUpdate(progress);
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String result) {

            if (activity != null) {
                dismissPDialog();

                if (result != null) {
                    if (result.equals("success")) {
                        switch(task){
                            case GET_SIZE:
                                showBaseAlertBox(fileName, "Are you sure to download this image \nFile size : "+fileSize, "NA", fileName,GET_SIZE,url);
                                break;
                            case DOWNLOAD:
                                showBaseAlertBox("Download Complete", "Image stored in Download/rwa folder", "NA", fileName,DOWNLOAD, url);
                                break;
                        }
                    } else {
                        Toast.makeText(activity, result, Toast.LENGTH_LONG).show();
                        L.m(result);
                    }
                } else {
                    Toast.makeText(activity, JsonParser.ERRORMESSAGE, Toast.LENGTH_LONG).show();
                    L.m(JsonParser.ERRORMESSAGE);
                }
            }
        }
    }

    public void showPDialog(Context context, String msg) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMax(100);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMessage(msg);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void showPDialog2(Context context, String msg) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void dismissPDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    public Button showBaseAlertBox(String title, String Description, final String errorMessage, final String fileName, int task, final String url) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_dialog, null);
        dialogBuilder.setView(v);
        ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
        LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
        LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
        ivAbout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (errorMessage != null) {
                    tvDescription.setText(errorMessage);
                }
            }
        });

        tvTitle.setText(title);
        tvDescription.setText(Description);
//        if (noOfButtons == 1) {
//            button2.setVisibility(View.GONE);
//            llBtn2.setVisibility(View.GONE);
//        }
        switch(task){
            case AssignTask.GET_SIZE:
                button1.setText("Download");
                button1.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        dialog.dismiss();
                        AssignTask assignTask = new AssignTask(getActivity(),AssignTask.DOWNLOAD);
                        assignTask.execute(url, fileName);
                    }
                });
                button2.setText("Cancel");
                button2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Close dialog
                        dialog.dismiss();
                    }
                });
                break;
            case AssignTask.DOWNLOAD:
                button1.setText("OK");
                button1.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        dialog.dismiss();
//                        viewPDF(fileName, "", APP);
                    }
                });
                button2.setText("Cancel");
                button2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Close dialog
                        dialog.dismiss();
                    }
                });
                break;
        }


        dialog = dialogBuilder.create();
        dialog.show();
        return button1;
    }

}
