package com.gennext.rwa.fragment.circular;

import android.animation.Animator;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.fragment.PopupAlert;
import com.gennext.rwa.fragment.gallery.Gallery;
import com.gennext.rwa.fragment.impContact.ImpContact;
import com.gennext.rwa.model.CircularAdapter;
import com.gennext.rwa.model.CircularModel;
import com.gennext.rwa.model.ImpContactAdapter;
import com.gennext.rwa.util.AppAnimation;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.RWA;

import java.util.ArrayList;

/**
 * Created by Abhijit on 15-Oct-16.
 */

public class Circular extends CompactFragment {
    private ListView lvMain;
    private ArrayList<CircularModel> cList;
    private FloatingActionButton fab;
    private int isScrolling = MotionEvent.ACTION_DOWN;

    public static Fragment newInstance() {
        Circular fragment=new Circular();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_circular, container, false);
        setActionBarOption(v, "Circular");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        lvMain = (ListView) v.findViewById(R.id.expandableListView1);
        lvMain.setSmoothScrollbarEnabled(true);
        fab = (FloatingActionButton) v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Click action
                if(isScrolling==MotionEvent.ACTION_UP){
                    lvMain.smoothScrollToPosition(0);
                }else if(isScrolling==MotionEvent.ACTION_DOWN) {
                    lvMain.smoothScrollToPosition(cList.size());
                }
            }
        });

        lvMain.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int lastPosition = -1;

            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING: // SCROLL_STATE_FLING
                        //hide button here
                        showOrHideButton(View.VISIBLE);
//                        fab.setVisibility(View.VISIBLE);
                        break;

                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL: // SCROLL_STATE_TOUCH_SCROLL
                        //hide button here
                        showOrHideButton(View.VISIBLE);
//                        fab.setVisibility(View.VISIBLE);
                        break;

                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE: // SCROLL_STATE_IDLE
                        //show button here
                        showOrHideButton(View.GONE);
//                        fab.setVisibility(View.GONE);
                        break;

                    default:
                        //show button here
//                        btn.setVisibility(View.VISIBLE);
                        break;
                }

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                Log.e("@@",String.valueOf(firstVisibleItem+","+visibleItemCount+","+totalItemCount));
                if (lastPosition == firstVisibleItem) {
//                    Log.e("@@", "return");
                    return;
                }
                if (firstVisibleItem > lastPosition) {
                    fab.setImageResource(R.mipmap.ic_group_down);
                    isScrolling = MotionEvent.ACTION_DOWN;
//                    Log.e("@@", "Going Down");
                } else {
                    fab.setImageResource(R.mipmap.ic_group_up);
                    isScrolling = MotionEvent.ACTION_UP;
//                    Log.e("@@", "Going Up");
                }
                lastPosition = firstVisibleItem;
            }
        });

        setProgressBar(v);
        executeTask();
    }

    private void executeTask() {
        JsonParser jsonParser = new JsonParser();
        CircularModel result = jsonParser.getCircularInfo(getActivity(), RWA.getCircularInfo(getActivity()));
        if (result.getOutput().equals("success")) {
            cList = result.getList();
            CircularAdapter adapter = new CircularAdapter(getActivity(), R.layout.slot_checkout, cList);
            lvMain.setAdapter(adapter);
        } else {
            showPopupAlert("Circular", result.getOutputMsg(), PopupAlert.FRAGMENT);
        }
    }

    private void showOrHideButton(int visibility) {
        if (visibility == View.VISIBLE && fab.getVisibility()!=View.VISIBLE) {
            fab.setVisibility(View.VISIBLE);
            fab.animate().alpha(1).setStartDelay(0).setDuration(100)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }
                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }
                    });
        } else if (visibility == View.GONE && fab.getVisibility()!=View.GONE) {
            fab.animate().alpha(0).setStartDelay(1000).setDuration(100)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            fab.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }
                    });
        }
    }




}
