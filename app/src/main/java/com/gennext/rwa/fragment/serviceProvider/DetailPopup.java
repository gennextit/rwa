package com.gennext.rwa.fragment.serviceProvider;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.model.OrderPopupAdapter;
import com.gennext.rwa.model.SPReviewAdapter;
import com.gennext.rwa.model.ServiceProviderModel;

import java.util.ArrayList;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class DetailPopup extends CompactFragment {
    private TextView  tvName, tvDesignation, tvDescription;
    private Button btnOK;
    private String name, designation, description;
    private FragmentManager manager;
    private ArrayList<ServiceProviderModel> reviewList;
    private ListView lvMain;
    private String imageUrl, categoryId, serviceProviderId;
    ServiceProvider serviceProvider;
    private ImageView ivImage;
    private ServiceProviderDetail serviceProviderDetail;
    private LinearLayout llReview;

    public void setFeedDetail(String imageUrl, String categoryId, String serviceProviderId,
                              ServiceProvider serviceProvider, String name, String designation,
                              String description, ArrayList<ServiceProviderModel> reviewList, ServiceProviderDetail serviceProviderDetail) {
        this.imageUrl = imageUrl;
        this.categoryId = categoryId;
        this.serviceProviderId = serviceProviderId;
        this.serviceProvider = serviceProvider;
        this.name = name;
        this.designation = designation;
        this.description = description;
        this.reviewList=reviewList;
        this.serviceProviderDetail=serviceProviderDetail;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_popup_alert_sp, container, false);
        manager = getFragmentManager();
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        tvName = (TextView) v.findViewById(R.id.tv_popup_name);
        ivImage = (ImageView) v.findViewById(R.id.iv_profile);
        lvMain = (ListView) v.findViewById(R.id.lv_main);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.frag_popup_alert_sp_header, null, false);
        tvDesignation = (TextView) listHeaderView.findViewById(R.id.tv_popup_1);
        tvDescription = (TextView) listHeaderView.findViewById(R.id.tv_popup_2);
        llReview = (LinearLayout) listHeaderView.findViewById(R.id.llreview);

        Glide.with(getContext())
                .load(imageUrl)
                .placeholder(R.drawable.profile)
                .error(R.drawable.profile)
                .into(ivImage);

        if (name != null)
            tvName.setText(name);
        if (!TextUtils.isEmpty(designation)) {
            tvDesignation.setVisibility(View.VISIBLE);
            tvDesignation.setText(designation);
        }else{
            tvDesignation.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(description)){
            tvDescription.setVisibility(View.VISIBLE);
            tvDescription.setText(description);
        }else{
            tvDescription.setVisibility(View.GONE);
        }

        lvMain.addHeaderView(listHeaderView);



        if (reviewList == null) {
            reviewList = new ArrayList<>();
        }
        if(reviewList.size()==0){
            llReview.setVisibility(View.GONE);
        }

        SPReviewAdapter adapter = new SPReviewAdapter(getActivity(), R.layout.slot_checkout, reviewList);
        lvMain.setAdapter(adapter);

        btnOK = (Button) v.findViewById(R.id.btn_popup);
        ImageView ivClose = (ImageView) v.findViewById(R.id.iv_close);
        LinearLayout llClose = (LinearLayout) v.findViewById(R.id.ll_whitespace);

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.popBackStack();
            }
        });
        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.popBackStack();
            }
        });


        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serviceProviderDetail.setFeedbackAlert();
            }
        });
    }


}