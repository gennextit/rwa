package com.gennext.rwa.fragment.directory;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.fragment.PopupAlert;
import com.gennext.rwa.fragment.gallery.Gallery;
import com.gennext.rwa.model.DirectoryModel;
import com.gennext.rwa.model.FilterDirectoryAdapter;
import com.gennext.rwa.util.AnimationClass;
import com.gennext.rwa.util.AppAnimation;
import com.gennext.rwa.util.AppSettings;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.HttpReq;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.L;
import com.gennext.rwa.util.ListViewFastSearch;
import com.gennext.rwa.util.RWA;

import com.gennext.rwa.util.internet.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhijit on 12-Nov-16.
 */

public class Directory extends CompactFragment {
    public static final int ALL_FIELDS=-1, PERSON_NAME=0,HOUSE_NO=1,VEHICLE_NO=2,PHONE_NO=3,OCCUPATION=4,SPOUSE_NAME=5;

    AssignTask assignTask;
    private ListView lvMain;
    private EditText etSearch;
    private FilterDirectoryAdapter adapter;
    private ArrayList<DirectoryModel>list;
    private LinearLayout llFilter,llSidebar;
    private AnimationClass anim;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }
    public static Fragment newInstance() {
        Directory fragment=new Directory();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_directory, container, false);
        setActionBarOption(v,"Directory");
        anim=new AnimationClass();
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        lvMain=(ListView)v.findViewById(R.id.expandableListView1);
        etSearch=(EditText)v.findViewById(R.id.et_dir_search);
        llFilter=(LinearLayout)v.findViewById(R.id.ll_filter);
        llSidebar=(LinearLayout)v.findViewById(R.id.llsidebar);
        llSidebar.setVisibility(View.GONE);
        llFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFilterAlertBox(getActivity());
            }
        });

        list=new ArrayList<>();
        adapter= new FilterDirectoryAdapter(getActivity(),list,this);
//        lvMain.setAdapter(adapter);

        setSearchCriteria(ALL_FIELDS);
        setProgressBar(v);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        executeTask();
    }

    private void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.getDirectory);
    }

    private void setSearchCriteria(int criteria) {
        adapter.setFilterCriteria(criteria);
    }



    private class AssignTask extends AsyncTask<String, Void, DirectoryModel> {
        Activity activity;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
        }

        @Override
        protected DirectoryModel doInBackground(String... urls) {
            DirectoryModel model;
            String response;
            HttpReq ob=new HttpReq();
            List<BasicNameValuePair> params = new ArrayList<>();
            if (activity != null) {
                String userMobile=AppUser.getMOBILE(activity);
                String userId=AppUser.getRegistrationId(activity);
                L.m("Mobile :"+userMobile!=""?userMobile:"mobile not saved");
                L.m("userId :"+userId!=""?userId:"userId not saved");
                params.add(new BasicNameValuePair("mobile", userMobile));
                params.add(new BasicNameValuePair("userId", userId));
            }
            response = ob.makeConnection(urls[0], HttpReq.GET,params);
            JsonParser jsonParser = new JsonParser();
            model=jsonParser.getDirectoryInfo(response);
            L.m(response);
            if(model!=null){
                if(activity!=null && model.getOutput().equals("success")){
                    RWA.setDIRECTORY(activity,response);
                }
                return model;
            }else{
                if(activity!=null)
                return jsonParser.getDirectoryInfo(RWA.getDIRECTORY(activity));
            }
            return null;
        }


        @Override
        protected void onPostExecute(DirectoryModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressBar();
            if (result != null) {
                if(result.getOutput().equals("success")){
                    list.addAll(result.getList());
                    adapter.notifyDataSetChanged();
                    lvMain.setAdapter(adapter);

                }else{
                    showPopupAlert("Alert",result.getOutputMsg(), PopupAlert.FRAGMENT);
                }
            }else{
                Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                retry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideBaseServerErrorAlertBox();
                        executeTask();
                    }
                });
            }
        }
    }


    public void showFilterAlertBox(Activity activity) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_filter_dialog, null);
        dialogBuilder.setView(v);
        ListView lvDialog = (ListView) v.findViewById(R.id.lv_filter);
        lvDialog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                dialog.dismiss();
                setSearchCriteria(pos);
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

    }

}