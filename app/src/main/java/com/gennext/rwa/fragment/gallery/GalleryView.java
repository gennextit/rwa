package com.gennext.rwa.fragment.gallery;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import com.etsy.android.grid.StaggeredGridView;
import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.model.GalleryModel;
import com.gennext.rwa.model.GalleryViewAdapter;

import java.util.ArrayList;


/**
 * Created by Abhijit on 29-Sep-16.
 */

public class GalleryView extends CompactFragment {
    StaggeredGridView gridView;
    ArrayList<GalleryModel> eventListDetail;
    AssignTask assignTask;
    String eventName;
    LinearLayout llProgress;

    public void setEventName(String eventName, ArrayList<GalleryModel> eventListDetail) {
        this.eventName=eventName;
        this.eventListDetail=eventListDetail;
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_gallery_view, container, false);
        setActionBarOption(v,eventName );
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
//        gridView = (GridView) v.findViewById(R.id.gv_profile_selection);
        llProgress =setProgressBarWithoutButton(v);
        gridView = (StaggeredGridView)v.findViewById(R.id.grid_view);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                if (eventListDetail != null) {
                    MasterImageView masterImageView = new MasterImageView();
                    masterImageView.setImage(eventListDetail.get(position).getImageUrl());
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.add(android.R.id.content, masterImageView, "masterImageView");
                    transaction.addToBackStack("masterImageView");
                    transaction.commit();
                }
            }
        });
        assignTask = new AssignTask(getActivity());
        assignTask.execute();
    }

    private void setEventList() {
        if(eventListDetail!=null){
            GalleryViewAdapter galleryViewAdapter=new GalleryViewAdapter(getActivity(),R.layout.slot_checkout,eventListDetail);
            gridView.setAdapter(galleryViewAdapter);
        }
    }


    private class AssignTask extends AsyncTask<String, Void, String> {

        Activity activity;

        public AssignTask(Activity activity) {
            onAttach(activity);
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

//        @Override
//        protected void onPreExecute() {
//            // TODO Auto-generated method stub
//            super.onPreExecute();
//            showProgressBar();
//        }

        @Override
        protected String doInBackground(String... params) {
            return "success";
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (activity != null) {
//                hideProgressBar();
                if (result != null) {
                    setEventList();
                }
            }
        }
    }

}