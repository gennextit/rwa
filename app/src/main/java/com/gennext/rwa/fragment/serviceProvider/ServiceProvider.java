package com.gennext.rwa.fragment.serviceProvider;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.fragment.PopupAlert;
import com.gennext.rwa.model.ServiceProviderAdapter;
import com.gennext.rwa.model.ServiceProviderModel;
import com.gennext.rwa.util.AppAnimation;
import com.gennext.rwa.util.AppSettings;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.HttpReq;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.RWA;
import com.gennext.rwa.util.internet.BasicNameValuePair;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhijit on 17-Sep-16.
 */
public class ServiceProvider extends CompactFragment {
    private static final int LOAD_LIST = 1, SEND_FEEDBACK = 2;
    private ExpandableListView lv;
    ArrayList<ServiceProviderModel> rwaList;
    AssignTask assignTask;
    private ServiceProviderAdapter adapter;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }

    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }
    public static Fragment newInstance() {
        ServiceProvider fragment=new ServiceProvider();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_service_provider, container, false);
        setActionBarOption(v, "Services");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        setProgressBar(v);
        lv = (ExpandableListView) v.findViewById(R.id.expandableListView1);
        lv.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                Intent ob;
                if (rwaList != null) {
                    String categoryId = rwaList.get(groupPosition).getCategoryId();
                    String serviceProviderId = rwaList.get(groupPosition).getChildList().get(childPosition).getServiceProviderId();
                    String imageUrl = rwaList.get(groupPosition).getChildList().get(childPosition).getImage();
                    String name = rwaList.get(groupPosition).getChildList().get(childPosition).getName();
                    String designation = rwaList.get(groupPosition).getChildList().get(childPosition).getDesignation();
                    String description = rwaList.get(groupPosition).getChildList().get(childPosition).getDescription();
                    ArrayList<ServiceProviderModel> reviewList = rwaList.get(groupPosition).getChildList().get(childPosition).getChildReviewList();

                    showFeedbackOption(imageUrl, categoryId, serviceProviderId,name,designation,description,reviewList);
                }
                return false;
            }
        });
        setList();
    }

    private void setList() {
        JsonParser jsonParser = new JsonParser();
        ServiceProviderModel result = jsonParser.getServiceProviderInfo(RWA.getServiceProvider(getActivity()));
        if(result.getOutput().equals("success")) {
            rwaList = result.getList();
            adapter = new ServiceProviderAdapter(getActivity(), rwaList, lv,ServiceProvider.this);
            lv.setAdapter(adapter);
        }else{
            showPopupAlert("Alert", getResources().getString(R.string.service_provider), PopupAlert.FRAGMENT);
        }
    }

    public void showCallAlertBox(final Activity context, final String name, final String mobile) {
        new TedPermission(getActivity())
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        adapter.showCallAlertBox(context, name, mobile);
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }
                })
                .setDeniedMessage(getString(R.string.if_you_reject_permission_you_can_not_use_this_service_please_turn_on_permissions))
                .setPermissions(Manifest.permission.CALL_PHONE)
                .check();
    }

    public void showBulkCallAlertBox(final Activity context, final String name, final String[] mobArray) {
        new TedPermission(getActivity())
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        adapter.showBulkCallAlertBox(context, name, mobArray);
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }
                })
                .setDeniedMessage(getString(R.string.if_you_reject_permission_you_can_not_use_this_service_please_turn_on_permissions))
                .setPermissions(Manifest.permission.CALL_PHONE)
                .check();
    }


    private class AssignTask extends AsyncTask<String, Void, ServiceProviderModel> {
        private final int task;
        Activity activity;
        private String rating, spCategoryId, contactId, feedback;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;

        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;

        }

        public AssignTask(Activity activity, int task) {
            this.task = task;
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (task == LOAD_LIST) {
                showProgressBar();
            } else {
                showProgressDialog(activity, "Processing... please wait.");
            }
        }

        @Override
        protected ServiceProviderModel doInBackground(String... urls) {
            JsonParser jsonParser;
            String response;
            if (task == LOAD_LIST) {
                jsonParser = new JsonParser();
                return jsonParser.getServiceProviderInfo(RWA.getServiceProvider(getActivity()));
            } else {
                this.spCategoryId = urls[1];
                this.contactId = urls[2];
                this.rating = urls[3];
                this.feedback = urls[4];

                List<BasicNameValuePair> params = new ArrayList<>();
                if (activity != null) {
                    params.add(new BasicNameValuePair("userId", AppUser.getRegistrationId(activity)));
                    params.add(new BasicNameValuePair("mobile", AppUser.getMOBILE(activity)));
                }
                params.add(new BasicNameValuePair("categoryId", urls[1]));
                params.add(new BasicNameValuePair("serviceProviderId", urls[2]));
                params.add(new BasicNameValuePair("rating", urls[3]));
                params.add(new BasicNameValuePair("feedback", urls[4]));

                HttpReq httpReq = new HttpReq();
                response = httpReq.makeConnection(urls[0], HttpReq.POST, params);
                String[] output = httpReq.makeSimpleJsonTask(response);
                ServiceProviderModel model = new ServiceProviderModel();
                model.setOutput(output[0]);
                model.setOutputMsg(output[1]);

                return model;
            }
        }

        @Override
        protected void onPostExecute(ServiceProviderModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (result != null) {
                if (task == LOAD_LIST) {
                    hideProgressBar();
                    if(result.getOutput().equals("success")) {
                        rwaList = result.getList();
                        ServiceProviderAdapter ExpAdapter = new ServiceProviderAdapter(getActivity(), rwaList, lv,ServiceProvider.this);
                        lv.setAdapter(ExpAdapter);
                    }else{
                        showPopupAlert("Alert", getResources().getString(R.string.service_provider), PopupAlert.FRAGMENT);
                    }
                } else {
                    hideProgressDialog();
                    if (result.getOutput().equals("success")) {
                        showPopupAlert("Feedback", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Feedback", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    } else {
                        Button retry = showBaseServerErrorAlertBox(result.getOutputMsg());
                        retry.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                hideBaseServerErrorAlertBox();
                                assignTask = new AssignTask(getActivity(), SEND_FEEDBACK);
                                assignTask.execute(AppSettings.feedbackForSP, contactId, spCategoryId, rating, feedback);
                            }
                        });
                    }
                }
            } else {
                if (task == LOAD_LIST) {
                    hideProgressBar();
                    String res = JsonParser.ERRORMESSAGE;
                    showPopupAlert("Alert", getResources().getString(R.string.service_provider), PopupAlert.FRAGMENT);
                } else {
                    hideProgressDialog();
                }
            }
        }

    }

    public void sendFeedBack(String categoryId, String serviceProviderId, String rating, String feedback) {
        assignTask = new AssignTask(getActivity(), SEND_FEEDBACK);
        assignTask.execute(AppSettings.feedbackForSP, categoryId, serviceProviderId, rating, feedback);

    }


    public void showFeedbackOption(String imageUrl, final String categoryId, final String serviceProviderId,
                                   String name, String designation, String description,
                                   ArrayList<ServiceProviderModel> reviewList) {

//        Feedback feedback = new Feedback();
//        feedback.setFeedDetail(imageUrl, categoryId, serviceProviderId, ServiceProvider.this);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
//        transaction.add(android.R.id.content, feedback, "feedback");
//        transaction.addToBackStack("feedback");
//        transaction.commit();
        ServiceProviderDetail serviceProviderDetail=new ServiceProviderDetail();
        serviceProviderDetail.setFeedDetail(imageUrl, categoryId, serviceProviderId, ServiceProvider.this,
                name,designation,description,reviewList);
        addFragment(serviceProviderDetail,android.R.id.content,"serviceProviderDetail");

//        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getActivity().getLayoutInflater();
//
//        View v = inflater.inflate(R.layout.alert_feedback, null);
//        dialogBuilder.setView(v);
//        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
//        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
//        //TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
//        final EditText etFeedback = (EditText) v.findViewById(R.id.et_alert_feedback);
//        final RatingBar ratingBar = (RatingBar) v.findViewById(R.id.ratingBar);
//
//
//        button1.setText("Send");
//        button1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Close dialog
//                dialog.dismiss();
//                assignTask = new AssignTask(getActivity(),SEND_FEEDBACK);
//                assignTask.execute(AppSettings.feedbackForSP,categoryId,serviceProviderId,String.valueOf(ratingBar.getRating()),etFeedback.getText().toString());
//            }
//        });
//        button2.setText("Cancel");
//        button2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Close dialog
//                dialog.dismiss();
//            }
//        });
//
//        dialog = dialogBuilder.create();
//        dialog.show();
    }
}
