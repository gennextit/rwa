package com.gennext.rwa.fragment.homeScreen;

/**
 * Created by Abhijit on 29-Jul-16.
 */

import android.animation.Animator;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gennext.rwa.MainActivity;
import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.model.SliderAdapter;
import com.gennext.rwa.model.SliderModel;
import com.gennext.rwa.util.AutoScrollViewPager;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.RWA;
import com.gennext.rwa.util.WidgetAnimation;

import java.util.ArrayList;


public class OptionMenu1 extends CompactFragment implements View.OnClickListener{

    LinearLayout opt1,opt2,opt3,opt4,opt5,opt6;
    FragmentManager manager;
    private AutoScrollViewPager viewPager;
    private ArrayList<SliderModel> sList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_home_screen, container, false);
        initUI(v);
        manager=getFragmentManager();
        return v;
    }

    private void initUI(View v) {
        opt1=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_1);
        opt2=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_2);
        opt3=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_3);
        opt4=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_4);
        opt5=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_5);
        opt6=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_6);

        opt1.setOnClickListener(this);
        opt2.setOnClickListener(this);
        opt3.setOnClickListener(this);
        opt4.setOnClickListener(this);
        opt5.setOnClickListener(this);
        opt6.setOnClickListener(this);

        viewPager = (AutoScrollViewPager) v.findViewById(R.id.myviewpager);


        sliderTask();

    }

    private void sliderTask() {
        JsonParser jsonParser = new JsonParser();
        SliderModel result = jsonParser.getBannerInfo(RWA.getBanner(getContext()));
        if (result != null) {
            if(result.getOutput().equals("success")){
                sList=result.getList();
                SliderAdapter myPagerAdapter = new SliderAdapter(getActivity(), sList,getFragmentManager());
                viewPager.setAdapter(myPagerAdapter);
                viewPager.setInterval(5000);
                viewPager.startAutoScroll();
            }
        }
    }

    @Override
    public void onClick(View view) {
        setAnimation(view);

    }

    private void setAnimation(final View view) {
        WidgetAnimation.zoomInOutForRectButton(view, new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                switch(view.getId()){
                    case R.id.ll_home_menu_option_1:
                        ((MainActivity)getActivity()).setServiceProvider();
                        break;
                    case R.id.ll_home_menu_option_2:
                        ((MainActivity)getActivity()).setRaiseIssue();
                        break;
                    case R.id.ll_home_menu_option_3:
//                ((MainActivity)getActivity()).setDailyNeeds();
                        ((MainActivity)getActivity()).setDirectory();
                        break;
                    case R.id.ll_home_menu_option_4:
                        ((MainActivity)getActivity()).setImpContact();
                        break;
                    case R.id.ll_home_menu_option_5:
                        ((MainActivity)getActivity()).setGallery();
                        break;
                    case R.id.ll_home_menu_option_6:
                        ((MainActivity)getActivity()).setCircular();
                        break;
                }

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

}

