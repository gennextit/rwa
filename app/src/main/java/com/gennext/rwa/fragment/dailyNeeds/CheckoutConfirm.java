package com.gennext.rwa.fragment.dailyNeeds;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.fragment.PopupAlert;
import com.gennext.rwa.model.CheckoutJson;
import com.gennext.rwa.model.CheckoutModel;
import com.gennext.rwa.util.AppSettings;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.DBManager;
import com.gennext.rwa.util.HttpReq;
import com.gennext.rwa.util.L;

import com.gennext.rwa.util.internet.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhijit on 13-Oct-16.
 */

public class CheckoutConfirm extends CompactFragment {
    private EditText etName,etCoupon;
    private TextView tvAddress,tvMobile,tvPayable,tvTotal,tvShipping;
    private ProgressDialog pDialog;
    private DBManager db;

    LinearLayout llActionBack;
    TextView tvTitle;
    private String payableAmount;
    private float shipingAmount=0.0f;
    private LinearLayout llAddress;
    LoadCheckoutList loadCheckoutList;
    private String name,mobile,address,alternateMobile;

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if(loadCheckoutList!=null){
            loadCheckoutList.onAttach(context);
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(loadCheckoutList!=null){
            loadCheckoutList.onDetach();
        }

    }

    public void setCheckoutDetail(String payableAmount) {
        this.payableAmount = payableAmount;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_checkout_confirm, container, false);
        setActionBarOpt(v);
        InitUI(v);
        return v;
    }

    private void setActionBarOpt(View v) {
        llActionBack = (LinearLayout) v.findViewById(R.id.ll_actionbar_back);
        tvTitle = (TextView) v.findViewById(R.id.actionbar_title);
        setActionBarOption();
        setActionBarTitle("Delivery Details");

    }
    private void setActionBarTitle(String title) {
        tvTitle.setText(title);
    }

    public void setActionBarOption() {
        llActionBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                getFragmentManager().popBackStack();
            }
        });
    }


    private void InitUI(View v) {
        etName = (EditText) v.findViewById(R.id.et_delivery_name);
        etCoupon = (EditText) v.findViewById(R.id.et_delivery_couponCode);
        tvAddress = (TextView) v.findViewById(R.id.tv_delivery_address);
        tvMobile = (TextView) v.findViewById(R.id.tv_delivery_mobile);
        tvPayable = (TextView) v.findViewById(R.id.tv_delivery_payableAmt);
        tvTotal = (TextView) v.findViewById(R.id.tv_delivery_totalAmt);
        tvShipping = (TextView) v.findViewById(R.id.tv_delivery_shipingAmt);
        llAddress = (LinearLayout) v.findViewById(R.id.llAddress);

        Button btnConfirm=(Button)v.findViewById(R.id.btn_checkout_confirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConfirmOrderClick();
            }
        });
        llAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddressOption();
            }
        });

        address= AppUser.getAddress(getActivity());
        mobile=AppUser.getMOBILE(getActivity());
        name=AppUser.getNAME(getActivity());
        float TotalPrice=Float.parseFloat(payableAmount);

        if(!address.equals("")){
            tvAddress.setText(address);
        }
        if(!mobile.equals("")){
            tvMobile.setText(mobile);
        }
        if(!name.equals("")){
            etName.setText(name);
        }
        tvPayable.setText(String.valueOf(TotalPrice+shipingAmount));
        tvTotal.setText(String.valueOf(TotalPrice));
        tvShipping.setText(String.valueOf(shipingAmount));
        
        db = new DBManager(getActivity());
  
    }

     
    
    public void onConfirmOrderClick() {
        loadCheckoutList=new LoadCheckoutList(getActivity(),tvAddress.getText().toString(),tvMobile.getText().toString(),
                tvPayable.getText().toString(),etName.getText().toString());
        loadCheckoutList.execute(AppSettings.order);
    }




    private class LoadCheckoutList extends AsyncTask<String, Void, String[]> {

        private final String address,mobile,payableAmt;
        private Activity activity;
        private String personName;

        public void onAttach(Activity activity) {
            this.activity=activity;
        }
        public void onDetach() {
            this.activity=null;
        }

        private LoadCheckoutList(Activity activity, String address, String mobile, String payableAmt,String personName){
            this.activity=activity;
            this.address=address;
            this.mobile=mobile;
            this.payableAmt=payableAmt;
            this.personName=personName;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setCancelable(false);
            pDialog.setMessage("Processing, Keep patience!");
            pDialog.show();
        }

        @Override
        protected String[] doInBackground(String... urls) {
            String checkOutJson,response = null;
            if(activity!=null){
                ArrayList<CheckoutModel> checkoutlist;
                checkoutlist=db.ViewData();
                if (checkoutlist!=null) {
                    // L.m(result);
                    checkOutJson=CheckoutJson.toJSonArray(checkoutlist);
                } else {
                    checkOutJson="";
                }

                List<BasicNameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair("userId", AppUser.getRegistrationId(activity)));
                params.add(new BasicNameValuePair("shippingPerson", personName));
                params.add(new BasicNameValuePair("shippingAddress", address));
                if(alternateMobile!=null && !alternateMobile.equals("")){
                    params.add(new BasicNameValuePair("mobileNo", alternateMobile));
                }else{
                    params.add(new BasicNameValuePair("mobileNo", mobile));
                }
                params.add(new BasicNameValuePair("payableAmt", payableAmt));
                params.add(new BasicNameValuePair("orderJson", checkOutJson));
                HttpReq ob = new HttpReq();
                response = ob.makeConnection(urls[0], HttpReq.POST, params);

                return ob.makeSimpleJsonTask(response);
            }else{
                return null;
            }
        }

        @Override
        protected void onPostExecute(String[] result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if(activity!=null){
                pDialog.dismiss();
                if (result != null) {
                    if(result[0].equals("success")){
                        db.DropTable();
                        showPopupAlert("Checkout Confirmed","Your Order Confirmed successfully", PopupAlert.ACTIVITY);
                    }else if(result[0].equals("failure")){
                        Toast.makeText(getActivity(),result[1],Toast.LENGTH_LONG).show();
                    }else{
                        Button retry=showBaseServerErrorAlertBox(result[1]);
                        retry.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                hideBaseServerErrorAlertBox();
                                onConfirmOrderClick();
                            }
                        });
                    }

                }
            }
        }
    }

     
    public class fakeConfirmOrder extends AsyncTask<Void, Void, Void>{

        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setCancelable(false);
            pDialog.setMessage("Processing, Keep patience!");
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            pDialog.dismiss();
            db.DropTable();
            showPopupAlert("Checkout Confirmed","Your Order Confirmed successfully", PopupAlert.ACTIVITY);
//            Toast.makeText(getActivity(),"Order Confirmed successfully",Toast.LENGTH_SHORT).show();
//            showToast();
//            AppTokens.sumTotalPrice=0;
//            AppTokens.countTotalItem=0;

//            getActivity().finish();

        }

    }


    public void showAddressOption() {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_address_change, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        //TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        final EditText etAddress = (EditText) v.findViewById(R.id.et_alert_address);
        etAddress.setText(address);
        final EditText etMobile = (EditText) v.findViewById(R.id.et_alert_mobile);


        button1.setText("OK");
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                String address=etAddress.getText().toString();
                if(!address.equals(""))
                tvAddress.setText(address);
                alternateMobile=etMobile.getText().toString();
                tvMobile.setText(mobile+"\n"+alternateMobile);
            }
        });
        button2.setText("Cancel");
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();
    }
}
