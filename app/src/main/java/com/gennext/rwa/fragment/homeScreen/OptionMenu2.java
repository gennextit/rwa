package com.gennext.rwa.fragment.homeScreen;

/**
 * Created by Abhijit on 29-Jul-16.
 */

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;


public class OptionMenu2 extends CompactFragment implements View.OnClickListener{

    LinearLayout opt1,opt2,opt3,opt4,opt5,opt6,opt7,opt8,opt9,opt10,opt11,opt12;
    FragmentManager manager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.option_menu_2, container, false);
//        initUI(v);
        manager=getFragmentManager();
        return v;
    }

//    private void initUI(View v) {
//        opt1=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_1);
//        opt2=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_2);
//        opt3=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_3);
//        opt4=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_4);
//        opt5=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_5);
//        opt6=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_6);
//        opt7=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_7);
//        opt8=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_8);
//        opt9=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_9);
//        opt10=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_10);
//        opt11=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_11);
//        opt12=(LinearLayout)v.findViewById(R.id.ll_home_menu_option_12);
//
//        opt1.setOnClickListener(this);
//        opt2.setOnClickListener(this);
//        opt3.setOnClickListener(this);
//        opt4.setOnClickListener(this);
//        opt5.setOnClickListener(this);
//        opt6.setOnClickListener(this);
//        opt7.setOnClickListener(this);
//        opt8.setOnClickListener(this);
//        opt9.setOnClickListener(this);
//        opt10.setOnClickListener(this);
//        opt11.setOnClickListener(this);
//        opt12.setOnClickListener(this);
//
//    }
//
    @Override
    public void onClick(View view) {
//        FragmentTransaction transaction;
//        switch(view.getId()){
//            case R.id.ll_home_menu_option_1:
//
//                Inquiry inquiry=new Inquiry();
//                transaction=manager.beginTransaction();
//                transaction.add(android.R.id.content,inquiry,"inquiry");
//                transaction.addToBackStack("inquiry");
//                transaction.commit();
//
//                break;
//            case R.id.ll_home_menu_option_2:
//                String phoneNo=School.getSchoolImportantcontactnumbers(getActivity());
//                Intent ob=new Intent(Intent.ACTION_CALL);
//                ob.setData(Uri.parse("tel:"+phoneNo));
//                startActivity(ob);
//
//                break;
//            case R.id.ll_home_menu_option_3:
//                AboutUs aboutUs=new AboutUs();
//                transaction=manager.beginTransaction();
//                transaction.add(android.R.id.content,aboutUs,"aboutUs");
//                transaction.addToBackStack("aboutUs");
//                transaction.commit();
//                break;
//            case R.id.ll_home_menu_option_4:
//                CurriculumMenu curriculumMenu=new CurriculumMenu();
//                transaction=manager.beginTransaction();
//                transaction.add(android.R.id.content,curriculumMenu,"curriculumMenu");
//                transaction.addToBackStack("curriculumMenu");
//                transaction.commit();
//                break;
//            case R.id.ll_home_menu_option_5:
//                Performance performance=new Performance();
//                transaction=manager.beginTransaction();
//                transaction.add(android.R.id.content,performance,"performance");
//                transaction.addToBackStack("performance");
//                transaction.commit();
//                break;
//            case R.id.ll_home_menu_option_6:
//                EventSports eventSports=new EventSports();
//                transaction=manager.beginTransaction();
//                transaction.add(android.R.id.content,eventSports,"eventSports");
//                transaction.addToBackStack("eventSports");
//                transaction.commit();
//                break;
//            case R.id.ll_home_menu_option_7:
//                Fees fees=new Fees();
//                transaction=manager.beginTransaction();
//                transaction.add(android.R.id.content,fees,"fees");
//                transaction.addToBackStack("fees");
//                transaction.commit();
//                break;
//            case R.id.ll_home_menu_option_8:
//                Transport transport=new Transport();
//                transaction=manager.beginTransaction();
//                transaction.add(android.R.id.content,transport,"transport");
//                transaction.addToBackStack("transport");
//                transaction.commit();
//                break;
//            case R.id.ll_home_menu_option_9:
////                MyProfile myprofile=new MyProfile();
////                transaction=manager.beginTransaction();
////                transaction.add(android.R.id.content,myprofile,"myprofile");
////                transaction.addToBackStack("myprofile");
////                transaction.commit();
//                break;
//            case R.id.ll_home_menu_option_10:
//                BookShop bookShop=new BookShop();
//                transaction=manager.beginTransaction();
//                transaction.add(android.R.id.content,bookShop,"bookShop");
//                transaction.addToBackStack("bookShop");
//                transaction.commit();
//                break;
//            case R.id.ll_home_menu_option_11:
////                MyProfile myprofile=new MyProfile();
////                transaction=manager.beginTransaction();
////                transaction.add(android.R.id.content,myprofile,"myprofile");
////                transaction.addToBackStack("myprofile");
////                transaction.commit();
//                break;
//            case R.id.ll_home_menu_option_12:
////                MyProfile myprofile=new MyProfile();
////                transaction=manager.beginTransaction();
////                transaction.add(android.R.id.content,myprofile,"myprofile");
////                transaction.addToBackStack("myprofile");
////                transaction.commit();
//                break;
//
//        }
//
    }
}

