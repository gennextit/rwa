package com.gennext.rwa.fragment.banner;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;

/**
 * Created by Abhijit on 23-Nov-16.
 */

public class ViewBannerDetail extends CompactFragment {

    private CollapsingToolbarLayout collapsingToolbarLayout = null;

    private String title="RWA";
    private ImageView ivBanner;
    private String imgUrl,adDetail;
    private WebView webView;
//    private TextView webBanner;
    private StringBuilder finalBlog;

    public void setData(String imgUrl, String adDetail) {
        this.imgUrl=imgUrl;
        this.adDetail=adDetail;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_banner_detail_view, container, false);
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        Toolbar toolbar = (Toolbar)v.findViewById(R.id.toolbar);

        ivBanner = (ImageView) v.findViewById(R.id.iv_banner_detail);
        webView = (WebView) v.findViewById(R.id.web_banner);
//        webBanner = (TextView) v.findViewById(R.id.web_banner);
        collapsingToolbarLayout = (CollapsingToolbarLayout) v.findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(title);



        if(imgUrl!=null) {
            Glide.with(getActivity())
                    .load(imgUrl)
                    .placeholder(R.drawable.profile)
                    .error(R.mipmap.error_connection)
                    .into(ivBanner);
        }

//        webBanner.getSettings().setJavaScriptEnabled(true);
//        webBanner.setWebChromeClient(new WebChromeClient());
        if(!TextUtils.isEmpty(adDetail)){
            startWebView(adDetail);
        }

//        if(adDetail!=null){
//            // Javascript inabled on webview
//            finalBlog=new StringBuilder();
////            finalBlog.append("<strong>"+title+"</strong>");
////            finalBlog.append("\n\n");
//            finalBlog.append(adDetail);
//
////            webBanner.loadData(finalBlog.toString(), "text/html; charset=utf-8","utf-8");
//        }else{
//            finalBlog=new StringBuilder();
////            finalBlog.append("<strong>"+title+"</strong>");
//            finalBlog.append("<strong>"+"Advertisement"+"</strong>");
//            finalBlog.append("\n\n");
//            String res=getResources().getString(R.string.about);
//            finalBlog.append(res);
//            finalBlog.append("\n\n");
//            finalBlog.append(res);
//
//
////            webBanner.setText(finalBlog.toString());
//        }
    }

    private void startWebView(String url) {
        webView.setWebViewClient(new WebViewClient() {

            // If you will not use this method url links are opeen in new brower
            // not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadUrl(url);

                return true;
            }

            // Show loader on url load
            public void onLoadResource(WebView view, String url) {

            }

            public void onPageFinished(WebView view, String url) {

            }

        });



        // Javascript inabled on webview
        webView.getSettings().setJavaScriptEnabled(true);

        // Other webview options
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        //webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        //webView.setScrollbarFadingEnabled(false);
        webView.loadUrl(url);

    }


}
