package com.gennext.rwa.fragment.vehicle;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.fragment.dialog.RemoveVehicleDialog;
import com.gennext.rwa.model.CheckoutJson;
import com.gennext.rwa.model.JsonModel;
import com.gennext.rwa.model.VehicleModel;
import com.gennext.rwa.util.AppSettings;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.FieldValidation;
import com.gennext.rwa.util.HttpReq;
import com.gennext.rwa.util.JsonParser;

import com.gennext.rwa.util.internet.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhijit on 13-Oct-16.
 */

public class VehicleDetail extends CompactFragment implements View.OnClickListener, View.OnFocusChangeListener, RemoveVehicleDialog.RemoveVehicleDialogListener, AdapterView.OnItemSelectedListener {
    public static final int MODE_INSERT = 1, MODE_UPDATE = 2;
    private Spinner spVehicleType, spVehicleType2, spVehicleType3, spVehicleType4;
    private AssignTask assignTask;
    private EditText etVehicleNo, etVehicleModel;
    private EditText etVehicleNo2, etVehicleModel2;
    private EditText etVehicleNo3, etVehicleModel3;
    private EditText etVehicleNo4, etVehicleModel4;

    private Button btnNext, btnSkip, btnAdd;
    private RelativeLayout llBtnContainer;
    private ImageView delete2, delete3, delete4;
    private View addAconatiner2, addAconatiner3, addAconatiner4;
    private ProgressBar progressBar;
    private String sltVehicleType = "", sltVehicleType2 = "", sltVehicleType3 = "", sltVehicleType4 = "";
    private String[] vehicleArr = new String[3];

    private LinearLayout contVehicle;

    private int addContainerFlag = 1;
    private VehicleList patentRef;

    private int modeRequest;

    private String vehicleId = "0", vehicleNo, vehicleType, vehicleModel;
    private int removeItemPos;
    private boolean[] check = {false, false, true, true, true, true, true, true};

    public void setPatentRef(VehicleList patentRef, int modeRequest) {
        this.patentRef = patentRef;
        this.modeRequest = modeRequest;
    }

    public void setVehicleDetailForUpdate(int removeItemPos, String vehicleId, String vehicleNo, String vehicleType, String vehicleModel) {
        this.removeItemPos = removeItemPos;
        this.vehicleId = vehicleId;
        this.vehicleNo = vehicleNo;
        this.vehicleType = vehicleType;
        this.vehicleModel = vehicleModel;

    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_vehicle_detail, container, false);
        setActionBarOption(v, "Vehicle Detail");
        InitUI(v);
        return v;
    }


    private void InitUI(View v) {
        llBtnContainer = (RelativeLayout) v.findViewById(R.id.ll_button_container);
        etVehicleNo = (EditText) v.findViewById(R.id.et_update_vehicle_no);
        spVehicleType = (Spinner) v.findViewById(R.id.sp_update_profile_vehicle_type);
        etVehicleModel = (EditText) v.findViewById(R.id.et_update_vehicle_model);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);

        contVehicle = (LinearLayout) v.findViewById(R.id.cont_vehicle);

        btnNext = (Button) v.findViewById(R.id.btn_update_vehicle_next);
        btnSkip = (Button) v.findViewById(R.id.btn_update_vehicle_skip);
        btnAdd = (Button) v.findViewById(R.id.btn_update_vehicle_add);
        btnNext.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        btnSkip.setOnClickListener(this);
        etVehicleNo.setOnFocusChangeListener(this);
        etVehicleModel.setOnFocusChangeListener(this);

        if (modeRequest == MODE_INSERT) {
            btnAdd.setVisibility(View.VISIBLE);
        } else if (modeRequest == MODE_UPDATE) {
            btnNext.setText("Update");
            btnAdd.setVisibility(View.GONE);
            if (vehicleNo != null) {
                etVehicleNo.setText(vehicleNo);
                etVehicleNo.requestFocus();
            }
            if (vehicleModel != null) {
                etVehicleModel.setText(vehicleModel);
            }
            sltVehicleType = vehicleType;
            if (!sltVehicleType.equals("")) {
                int pos = 0;
                String[] sports = getResources().getStringArray(R.array.vehicle_type);
                for (int i = 0; i < sports.length; i++) {
                    if (sports[i].equals(sltVehicleType)) {
                        pos = i;
                    }
                }
                spVehicleType.setSelection(pos);
            }
        }

//        vehicleArr = AppUser.getVehicleData(getActivity());
//        etVehicleNo.setText(vehicleArr[0]);
//        String userName = AppUser.getNAME(getActivity());
//        if (!userName.equals("")) {
//            btnSkip.setVisibility(View.VISIBLE);
//        } else {
//            btnSkip.setVisibility(View.GONE);
//        }
//
//        etVehicleModel.setText(vehicleArr[2]);

//        String vType = vehicleArr[1];


        spVehicleType.setOnItemSelectedListener(this);

    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.et_update_vehicle_no:
                if (!hasFocus) {
                    check[0] = FieldValidation.validate(getActivity(), etVehicleNo, FieldValidation.VEHICLE, true);
                }
                break;
            case R.id.et_update_vehicle_model:
                if (!hasFocus) {
                    check[1] = FieldValidation.validate(getActivity(), etVehicleModel, FieldValidation.STRING, true);
                }
                break;
            case R.id.et_update_vehicle_no2:
                if (!hasFocus) {
                    check[2] = FieldValidation.validate(getActivity(), etVehicleNo2, FieldValidation.VEHICLE, true);
                }
                break;
            case R.id.et_update_vehicle_model2:
                if (!hasFocus) {
                    check[3] = FieldValidation.validate(getActivity(), etVehicleModel2, FieldValidation.STRING, true);
                }
                break;
            case R.id.et_update_vehicle_no3:
                if (!hasFocus) {
                    check[4] = FieldValidation.validate(getActivity(), etVehicleNo3, FieldValidation.VEHICLE, true);
                }
                break;
            case R.id.et_update_vehicle_model3:
                if (!hasFocus) {
                    check[5] = FieldValidation.validate(getActivity(), etVehicleModel3, FieldValidation.STRING, true);
                }
                break;
            case R.id.et_update_vehicle_no4:
                if (!hasFocus) {
                    check[6] = FieldValidation.validate(getActivity(), etVehicleNo4, FieldValidation.VEHICLE, true);
                }
                break;
            case R.id.et_update_vehicle_model4:
                if (!hasFocus) {
                    check[7] = FieldValidation.validate(getActivity(), etVehicleModel4, FieldValidation.STRING, true);
                }
                break;

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapter, View view, int i, long l) {
        switch (adapter.getId()) {
            case R.id.sp_update_profile_vehicle_type:
                if (!adapter.getSelectedItem().toString().equals("Select type")) {
                    sltVehicleType = adapter.getSelectedItem().toString();
                }
                break;
            case R.id.sp_update_profile_vehicle_type2:
                if (!adapter.getSelectedItem().toString().equals("Select type")) {
                    sltVehicleType2 = adapter.getSelectedItem().toString();
                }
                break;
            case R.id.sp_update_profile_vehicle_type3:
                if (!adapter.getSelectedItem().toString().equals("Select type")) {
                    sltVehicleType3 = adapter.getSelectedItem().toString();
                }
                break;
            case R.id.sp_update_profile_vehicle_type4:
                if (!adapter.getSelectedItem().toString().equals("Select type")) {
                    sltVehicleType4 = adapter.getSelectedItem().toString();
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    private void executeTask(int modeRequest) {
//        if (modeRequest == MODE_INSERT) {
        if (validatorProcess()) {
            VehicleModel ob;
            View c1 = addAconatiner2;
            View c2 = addAconatiner3;
            View c3 = addAconatiner4;
            VehicleModel model = new VehicleModel();
            model.setOutput("success");
            model.setList(new ArrayList<VehicleModel>());
            String vNo = etVehicleNo.getText().toString();
            if (!vNo.equals("")) {
                ob = new VehicleModel();
                ob.setField(vehicleId, etVehicleNo.getText().toString(), sltVehicleType, etVehicleModel.getText().toString());
                model.getList().add(ob);
            } else {
                model.setOutput("failure");
            }
            if (c1 != null) {
                ob = new VehicleModel();
                ob.setField(vehicleId, etVehicleNo2.getText().toString(), sltVehicleType2, etVehicleModel2.getText().toString());
                model.getList().add(ob);
            }
            if (c2 != null) {
                ob = new VehicleModel();
                ob.setField(vehicleId, etVehicleNo3.getText().toString(), sltVehicleType3, etVehicleModel3.getText().toString());
                model.getList().add(ob);
            }
            if (c3 != null) {
                ob = new VehicleModel();
                ob.setField(vehicleId, etVehicleNo4.getText().toString(), sltVehicleType4, etVehicleModel4.getText().toString());
                model.getList().add(ob);
            }

            assignTask = new AssignTask(getActivity(), model);
            assignTask.execute(AppSettings.vehicleInsertUpdate);
//        } else if (modeRequest == MODE_UPDATE) {
//            VehicleModel model = new VehicleModel();
//            model.setOutput("success");
//            model.setList(new ArrayList<VehicleModel>());
//            String vNo = etVehicleNo.getText().toString();
//            if (!vNo.equals("")) {
//                VehicleModel ob = new VehicleModel();
//                ob.setField(etVehicleNo.getText().toString(), sltVehicleType, etVehicleModel.getText().toString());
//                model.getList().add(ob);
//            } else{
//                model.setOutput("failure");
//            }
//            assignTask = new AssignTask(getActivity(), model);
//            assignTask.execute(AppSettings.vehicleInsertUpdate);
//        }
        }
    }

    /**
     * Starts processing the item on a separate thread.
     */
    public boolean validatorProcess() {
        boolean res=true;
        if(!FieldValidation.validate(getActivity(), etVehicleNo, FieldValidation.VEHICLE, true)){
            res= check[0] =false;
        }else if(sltVehicleType.equals("")){
            Toast.makeText(getActivity(),"Please select vehicle type",Toast.LENGTH_LONG).show();
            res=false;
        }else if(!FieldValidation.validate(getActivity(), etVehicleModel, FieldValidation.STRING, true)){
            res= check[1] =false;
        }else if(addAconatiner2!=null && !FieldValidation.validate(getActivity(), etVehicleNo2, FieldValidation.VEHICLE, true)){
            res= check[2] =false;
        }else if(addAconatiner2!=null && sltVehicleType2.equals("")){
            Toast.makeText(getActivity(),"Please select vehicle type",Toast.LENGTH_LONG).show();
            res=false;
        }else if(etVehicleModel2!=null && !FieldValidation.validate(getActivity(), etVehicleModel2, FieldValidation.STRING, true)){
            res= check[3] =false;
        }else if(addAconatiner3!=null && !FieldValidation.validate(getActivity(), etVehicleNo3, FieldValidation.VEHICLE, true)){
            res= check[4] =false;
        }else if(addAconatiner3!=null && sltVehicleType3.equals("")){
            Toast.makeText(getActivity(),"Please select vehicle type",Toast.LENGTH_LONG).show();
            res=false;
        }else if(etVehicleModel3!=null && !FieldValidation.validate(getActivity(), etVehicleModel3, FieldValidation.STRING, true)){
            res= check[5] =false;
        }else if(addAconatiner4!=null && !FieldValidation.validate(getActivity(), etVehicleNo4, FieldValidation.VEHICLE, true)){
            res= check[6] =false;
        }else if(addAconatiner4!=null && sltVehicleType4.equals("")){
            Toast.makeText(getActivity(),"Please select vehicle type",Toast.LENGTH_LONG).show();
            res=false;
        }else if(etVehicleModel4!=null && !FieldValidation.validate(getActivity(), etVehicleModel4, FieldValidation.STRING, true)){
            res= check[7] =false;
        }

        return res;
    }

    @Override
    public void onClick(View view) {
        hideKeybord(getActivity());
        switch (view.getId()) {
            case R.id.btn_update_vehicle_next:
                executeTask(modeRequest);
                break;
            case R.id.btn_update_vehicle_add:
                addSecondFieldInContainer();
                break;
            case R.id.btn_update_vehicle_skip:
                getFragmentManager().popBackStack();
                break;
            case R.id.delete2:
                setRemoveDialog(2);
                break;
            case R.id.delete3:
                setRemoveDialog(3);
                break;
            case R.id.delete4:
                setRemoveDialog(4);
                break;

        }
    }

    private void addSecondFieldInContainer() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        if (addAconatiner2 == null) {
            check[2] = false;
            check[3] = false;
            addAconatiner2 = inflater.inflate(R.layout.container_vehicle2,
                    contVehicle, false);
            contVehicle.addView(addAconatiner2);
            delete2 = (ImageView) addAconatiner2.findViewById(R.id.delete2);
            etVehicleNo2 = (EditText) addAconatiner2.findViewById(R.id.et_update_vehicle_no2);
            spVehicleType2 = (Spinner) addAconatiner2.findViewById(R.id.sp_update_profile_vehicle_type2);
            etVehicleModel2 = (EditText) addAconatiner2.findViewById(R.id.et_update_vehicle_model2);
            delete2.setOnClickListener(this);
            spVehicleType2.setOnItemSelectedListener(this);
            etVehicleNo2.setOnFocusChangeListener(this);
            etVehicleModel2.setOnFocusChangeListener(this);

        } else if (addAconatiner3 == null) {
            check[4] = false;
            check[5] = false;
            addAconatiner3 = inflater.inflate(R.layout.container_vehicle3,
                    contVehicle, false);
            contVehicle.addView(addAconatiner3);
            delete3 = (ImageView) addAconatiner3.findViewById(R.id.delete3);
            etVehicleNo3 = (EditText) addAconatiner3.findViewById(R.id.et_update_vehicle_no3);
            spVehicleType3 = (Spinner) addAconatiner3.findViewById(R.id.sp_update_profile_vehicle_type3);
            etVehicleModel3 = (EditText) addAconatiner3.findViewById(R.id.et_update_vehicle_model3);
            delete3.setOnClickListener(this);
            spVehicleType3.setOnItemSelectedListener(this);
            etVehicleNo3.setOnFocusChangeListener(this);
            etVehicleModel3.setOnFocusChangeListener(this);
        } else if (addAconatiner4 == null) {
            check[6] = false;
            check[7] = false;
            addAconatiner4 = inflater.inflate(R.layout.container_vehicle4,
                    contVehicle, false);
            contVehicle.addView(addAconatiner4);
            delete4 = (ImageView) addAconatiner4.findViewById(R.id.delete4);
            etVehicleNo4 = (EditText) addAconatiner4.findViewById(R.id.et_update_vehicle_no4);
            spVehicleType4 = (Spinner) addAconatiner4.findViewById(R.id.sp_update_profile_vehicle_type4);
            etVehicleModel4 = (EditText) addAconatiner4.findViewById(R.id.et_update_vehicle_model4);
            delete4.setOnClickListener(this);
            spVehicleType4.setOnItemSelectedListener(this);
            etVehicleNo4.setOnFocusChangeListener(this);
            etVehicleModel4.setOnFocusChangeListener(this);
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        closeFragmentDialog("removevehicle");
    }


    public void setRemoveDialog(int field) {
        RemoveVehicleDialog.newInstance("Alert", "Are you sure to remove this vehicle detail", field, this).
                show(getFragmentManager(), "removevehicle");
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, int field) {
        switch (field) {
            case 2:
                check[2] = true;
                check[3] = true;
                contVehicle.removeView(addAconatiner2);
                addAconatiner2 = null;
                break;
            case 3:
                check[4] = true;
                check[5] = true;
                contVehicle.removeView(addAconatiner3);
                addAconatiner3 = null;
                break;
            case 4:
                check[6] = true;
                check[7] = true;
                contVehicle.removeView(addAconatiner4);
                addAconatiner4 = null;
                break;
        }
    }


    private class AssignTask extends AsyncTask<String, Void, VehicleModel> {
        private final VehicleModel model;
        Activity activity;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity, VehicleModel model) {
            this.activity = activity;
            this.model = model;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            llBtnContainer.setVisibility(View.INVISIBLE);
        }

        @Override
        protected VehicleModel doInBackground(String... urls) {
            ArrayList<VehicleModel> vehicleList;
            String mobile = "";
            String vehicleJson = null;
            if (model != null && model.getOutput().equals("success")) {
                vehicleList = model.getList();
                if (vehicleList != null) {
                    vehicleJson = CheckoutJson.toVehicleJSonArray(vehicleList);
                }
            }

            List<BasicNameValuePair> params = new ArrayList<>();
            if (activity != null) {
                mobile = AppUser.getMOBILE(activity);
                params.add(new BasicNameValuePair("mobile", mobile));
                params.add(new BasicNameValuePair("userId", AppUser.getRegistrationId(activity)));
            }

            params.add(new BasicNameValuePair("vehicleInfo", vehicleJson));
            HttpReq ob = new HttpReq();
            String res = ob.makeConnection(urls[0], HttpReq.POST, params);
            JsonParser jsonParser = new JsonParser();
            VehicleModel jsonModel = jsonParser.parseVehicleInsertUpdate(activity, res);
            return jsonModel;
        }


        @Override
        protected void onPostExecute(VehicleModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressBar.setVisibility(View.INVISIBLE);
            llBtnContainer.setVisibility(View.VISIBLE);
            if (result != null) {
                if (result.getOutput().equalsIgnoreCase("success")) {
                    if (model.getOutput().equals("success")) {
                        if (modeRequest == MODE_INSERT) {
                            Toast.makeText(getActivity(), "Vehicle added", Toast.LENGTH_LONG).show();
//                            patentRef.setUpdateList(model.getList());
                            patentRef.refreshList();
                            getFragmentManager().popBackStack();
                        } else if (modeRequest == MODE_UPDATE) {
                            Toast.makeText(getActivity(), "Vehicle record updated", Toast.LENGTH_LONG).show();
//                            patentRef.setUpdateItemInList(removeItemPos, model.getList());
                            patentRef.refreshList();
                            getFragmentManager().popBackStack();
                        }
                    } else {
                        patentRef.setUpdateList(null);
                        getFragmentManager().popBackStack();
                    }
                } else {
                    Toast.makeText(getActivity(), result.getOutputMsg(), Toast.LENGTH_LONG).show();
                }
            } else {
                Button retry = showBaseServerErrorAlertBox(JsonParser.ERRORMESSAGE);
                retry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideBaseServerErrorAlertBox();
                        executeTask(modeRequest);
                    }
                });
            }
        }
    }

}