package com.gennext.rwa.fragment.impContact;

import android.Manifest;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.fragment.PopupAlert;
import com.gennext.rwa.model.ImpContactAdapter;
import com.gennext.rwa.model.ImpContactModel;
import com.gennext.rwa.util.AppAnimation;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.RWA;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

/**
 * Created by Abhijit on 17-Sep-16.
 */
public class ImpContact extends CompactFragment {
    private ExpandableListView lv;
    ArrayList<ImpContactModel> rwaList;
    private ImpContactAdapter adapter;

    public static Fragment newInstance() {
        ImpContact fragment=new ImpContact();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_imp_contact, container, false);
        setActionBarOption(v, "Important Contact");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        setProgressBar(v);
        lv = (ExpandableListView) v.findViewById(R.id.expandableListView1);
        lv.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                if (rwaList != null) {
                    String name=rwaList.get(groupPosition).getChildList().get(childPosition).getName();
                    String image=rwaList.get(groupPosition).getChildList().get(childPosition).getImage();
                    String designation=rwaList.get(groupPosition).getChildList().get(childPosition).getDesignation();
                    String detail=rwaList.get(groupPosition).getChildList().get(childPosition).getDetail();
                    String availableFrom=rwaList.get(groupPosition).getChildList().get(childPosition).getAvailableFrom();

                    ImpContactDetail impContactDetail=new ImpContactDetail();
                    impContactDetail.setData(name,image,designation,detail,availableFrom);
                    addFragment(impContactDetail,android.R.id.content,"impContactDetail");
                }
                return false;
            }
        });
        setList();
    }

    private void setList() {
        JsonParser jsonParser = new JsonParser();
        rwaList = jsonParser.getImpContactInfo(RWA.getImportantContactNumber(getActivity()));

        if (rwaList != null) {
            adapter = new ImpContactAdapter(getActivity(), rwaList,lv,ImpContact.this);
            lv.setAdapter(adapter);
        }else {
            showPopupAlert("Alert", getResources().getString(R.string.imp_contact), PopupAlert.FRAGMENT);
        }
    }

    public void showCallAlertBox(final Activity context, final String name, final String mobile) {
        new TedPermission(getActivity())
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        adapter.showCallAlertBox(context, name, mobile);
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }
                })
                .setDeniedMessage(getString(R.string.if_you_reject_permission_you_can_not_use_this_service_please_turn_on_permissions))
                .setPermissions(Manifest.permission.CALL_PHONE)
                .check();
    }

    public void showBulkCallAlertBox(final Activity context, final String name, final String[] mobArray) {
        new TedPermission(getActivity())
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        adapter.showBulkCallAlertBox(context, name, mobArray);
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }
                })
                .setDeniedMessage(getString(R.string.if_you_reject_permission_you_can_not_use_this_service_please_turn_on_permissions))
                .setPermissions(Manifest.permission.CALL_PHONE)
                .check();
    }
}
