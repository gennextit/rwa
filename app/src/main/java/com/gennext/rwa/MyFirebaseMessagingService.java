/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gennext.rwa;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.gennext.rwa.util.GCMTokens;
import com.gennext.rwa.util.L;
import com.gennext.rwa.util.NotificationUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    private NotificationUtils notificationUtils;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            String notificationBody = remoteMessage.getNotification().getBody();
            Log.e(TAG, "Notification Body: " + notificationBody);
            handleNotification(notificationBody);
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            JSONObject jsonObject=null;
            String dataPayload = remoteMessage.getData().toString();
            Log.e(TAG, "Data Payload: " + dataPayload);
            try {
                jsonObject=new JSONObject(dataPayload);
                handleDataMessage(jsonObject);
            } catch (JSONException e) {
                L.m("Json Error"+ e.toString());
            }
        }
    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(GCMTokens.PUSH_NOTIFICATION);
            pushNotification.putExtra("state", GCMTokens.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            String timestamp = null;
//            showNotificationMessage(getApplicationContext(), "RWA", message, timestamp, pushNotification);

            sendNotification("RWA", message, pushNotification);
            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            // app is in background, show the notification in notification tray
            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
            resultIntent.putExtra("state", GCMTokens.PUSH_NOTIFICATION);
            resultIntent.putExtra("message", message);

//            sendNotification(message);
            // check for image attachment
            String timestamp = null;
//            showNotificationMessage(getApplicationContext(), "RWA", message, timestamp, resultIntent);

            sendNotification("RWA", message, resultIntent);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) {
//        {url=, category=N, sound=1, title=RWA Notification, vibrate=1, message=test}
        Log.e(TAG, "push json: " + json);

        try {
            JSONObject data = json.getJSONObject("data");

            String category = data.optString("category");
            String title = data.optString("title");
            String message = data.optString("message");
            String imageUrl = data.optString("image");
            int sound = data.optInt("sound");
            int vibrate = data.optInt("vibrate");
            String url = data.optString("url");

            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "category: " + category);
            Log.e(TAG, "url: " + url);
            Log.e(TAG, "imageUrl: " + imageUrl!=null?imageUrl:"null");



            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                Intent pushNotification = null;
                if(!TextUtils.isEmpty(category)){
                    switch (category){
                        case "N":
                            pushNotification = new Intent(GCMTokens.PUSH_NOTIFICATION);
                            pushNotification.putExtra("state", GCMTokens.PUSH_NOTIFICATION);
                            pushNotification.putExtra("message", message);
                            break;
                        case "C":
                            pushNotification = new Intent(GCMTokens.PUSH_NOTIFICATION);
                            pushNotification.putExtra("state", GCMTokens.CIRCULAR_NOTIFICATION);
                            pushNotification.putExtra("message", message);
                            break;
                        case "A":
                            pushNotification = new Intent(GCMTokens.PUSH_NOTIFICATION);
                            pushNotification.putExtra("state", GCMTokens.ADVERTISE_NOTIFICATION);
                            pushNotification.putExtra("message", message);
                            pushNotification.putExtra("url", url);
                            break;
                        default:
                            pushNotification = new Intent(GCMTokens.PUSH_NOTIFICATION);
                            pushNotification.putExtra("state", GCMTokens.PUSH_NOTIFICATION);
                            pushNotification.putExtra("message", message);
                    }
                }
                if(pushNotification!=null) {
                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                    sendNotification(title, message, pushNotification);
                }
                // play notification sound
//                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//                notificationUtils.playNotificationSound();
            } else {
                // app is in background, show the notification in notification tray
                Intent pushNotification = null;

                if(!TextUtils.isEmpty(category)){
                    switch (category){
                        case "N":
                            pushNotification = new Intent(getApplicationContext(), MainActivity.class);
                            pushNotification.putExtra("state", GCMTokens.PUSH_NOTIFICATION);
                            pushNotification.putExtra("message", message);
                            break;
                        case "C":
                            pushNotification = new Intent(getApplicationContext(), MainActivity.class);
                            pushNotification.putExtra("state", GCMTokens.CIRCULAR_NOTIFICATION);
                            pushNotification.putExtra("message", message);
                            break;
                        case "A":
                            pushNotification = new Intent(getApplicationContext(), MainActivity.class);
                            pushNotification.putExtra("state", GCMTokens.ADVERTISE_NOTIFICATION);
                            pushNotification.putExtra("message", message);
                            pushNotification.putExtra("url", url);
                            break;
                        default:
                            pushNotification = new Intent(getApplicationContext(), MainActivity.class);
                            pushNotification.putExtra("state", GCMTokens.PUSH_NOTIFICATION);
                            pushNotification.putExtra("message", message);
                    }
                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                }
                if(pushNotification!=null) {
                    sendNotification(title, message, pushNotification);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    // [START receive_message]
//    @Override
//    public void onMessageReceived(RemoteMessage remoteMessage) {
//
//        Log.d(TAG, "From: " + remoteMessage.getFrom());
//
//        // Check if message contains a data payload.
//        if (remoteMessage.getData().size() > 0) {
//            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
//        }
//
//        // Check if message contains a notification payload.
//        if (remoteMessage.getNotification() != null) {
//            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
//        }
//
//    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *  @param title
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String title, String messageBody, Intent intent) {
//        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        final PendingIntent pendingIntent = PendingIntent.getActivity(
                        getApplicationContext(),
                        0,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );

//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
