package com.gennext.rwa;

// "Therefore those skilled at the unorthodox
// are infinite as heaven and earth,
// inexhaustible as the great rivers.
// When they come to an end,
// they begin again,
// like the days and months;
// they die and are reborn,
// like the four seasons."
//
// - Sun Tsu,
// "The Art of War"

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.gennext.rwa.fragment.raiseIssue.RaiseIssue;
import com.gennext.rwa.util.AppTokens;
import com.gennext.rwa.util.CameraUtility;
import com.gennext.rwa.util.L;
import com.gennext.rwa.util.image.FileUtils;
import com.gennext.rwa.util.imagecache.ImageLoader;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ImageCroperActivity extends AppCompatActivity {

    /**
     * Persist URI image to crop URI if specific permissions are required
     */
    private Uri mCropImageUri;
    AlertDialog dialog = null;
   //private Uri mImageCaptureUri, cropImageUri;
    private static final int PICK_FROM_CAMERA = 1;
    private static final int CROP_FROM_CAMERA = 2;
    private static final int PICK_FROM_FILE = 3;
    private static final int REQUEST_CODE_IMAGE_PICKER = 6384; // onActivityResult
    ImageView ivProfileImage;
    ImageLoader imageLoader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_croper);
        ivProfileImage=(ImageView) findViewById(R.id.quick_start_cropped_image);
        imageLoader=new ImageLoader(this);
        setActionBarOption();
        OptionAlertBox(this);

        setDefaultImage();
    }

    private void setDefaultImage() {

//        Buddy.setBuddyProfileImage(this, imageLoader, ivProfileImage);
    }


    public void setActionBarOption() {
        LinearLayout ActionBack;
        ActionBack = (LinearLayout)findViewById(R.id.ll_actionbar_back);
        ActionBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                RaiseIssue.mTempCropImageUri=null;
                finish();
            }
        });

    }

    /**
     * Start pick image activity with chooser.
     */
    public void onSelectImageClick(View view) {
       // CropImage.startPickImageActivity(this);

        startActivityToSetImage(mCropImageUri);

    }

    public void onChooseImageClick(View view) {
        OptionAlertBox(this);

    }

    public void OptionAlertBox(Activity Act) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);

        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = Act.getLayoutInflater();

        View v = inflater.inflate(R.layout.custom_take_camera_dialog, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.button1);
        Button button2 = (Button) v.findViewById(R.id.button2);
        // if decline button is clicked, close the custom dialog
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                mCropImageUri = getOutputMediaFileUri();
                if(mCropImageUri!=null){
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImageUri);
                }

                try {
                    intent.putExtra("return-data", true);
					/*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);*/

                    startActivityForResult(intent, PICK_FROM_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }

            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                // pick from file
                // Intent intent = new Intent();
                //
                // intent.setType("image/*");
                // intent.setAction(Intent.ACTION_GET_CONTENT);
                //
                // startActivityForResult(Intent.createChooser(intent, "Complete
                // action using"), PICK_FROM_FILE);

                Intent target = FileUtils.createGetContentIntent();
                // Create the chooser Intent
                Intent intent = Intent.createChooser(target, "Select Image");
                try {
                    startActivityForResult(intent, REQUEST_CODE_IMAGE_PICKER);
                } catch (ActivityNotFoundException e) {
                    // The reason for the existence of aFileChooser
                }

            }
        });
        dialog = dialogBuilder.create();
        dialog.show();

    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean requirePermissions;

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mCropImageUri=result.getUri();
                try{
                    ivProfileImage.setImageURI(mCropImageUri);

                }catch (OutOfMemoryError e){
                    L.m(e.toString());
                    Toast.makeText(ImageCroperActivity.this,"Image is to large, please select small image from gallery.",Toast.LENGTH_LONG).show();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: please select a valid Image", Toast.LENGTH_LONG).show();
            }
        }

        switch (requestCode) {
            case PICK_FROM_CAMERA:
                requirePermissions = false;
//                startCropImageActivity(mCropImageUri);
                ivProfileImage.setImageURI(mCropImageUri);

                break;

            case REQUEST_CODE_IMAGE_PICKER:

                // If the file selection was successful
                if (resultCode ==RESULT_OK) {
                    if (data != null) {
                        // Get the URI of the selected file
                        final Uri uri = data.getData();
                        L.m("Uri = " + uri.toString());
                        try {
                            requirePermissions = false;
                            mCropImageUri=uri;
//                            ivProfileImage.setImageURI(uri);
                            startCropImageActivity(uri);

                        } catch (Exception e) {
                            L.m("FileSelectorTestActivity"+ "File select error"+ e.toString());
                        }
                    }
                }
                break;

            case CROP_FROM_CAMERA:
                if(data!=null){
                    Bundle extras = data.getExtras();
                    Bitmap cropedPhoto = extras.getParcelable("data");

                    mCropImageUri = Uri.fromFile(CameraUtility.getOutputCropedMediaFile());
                    CameraUtility.saveImageExternal(this, cropedPhoto, mCropImageUri);
                    //ivImg.setImageBitmap(cropedPhoto);
                    //L.m("cropImageUri**************" +cropImageUri);
                    ivProfileImage.setImageURI(mCropImageUri);

                }


                // File f = new File(mCropImageUri.getPath());
                //
                // if (f.exists())
                // f.delete();

                break;



        }

    }


    private void startActivityToSetImage( Uri cropImageUri) {

        setImageUriFromSource(mCropImageUri);
        finish();

    }

    private void setImageUriFromSource(Uri uri) {
        RaiseIssue.mTempCropImageUri=uri;

    }

    public Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    /*
     * returning image / video
     */
    private static File getOutputMediaFile() {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                AppTokens.PROFILE_IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(AppTokens.PROFILE_IMAGE_DIRECTORY_NAME,
                        "Oops! Failed create " + AppTokens.PROFILE_IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }


        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    /*@Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }*/

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setAspectRatio(1,1)
                .setFixAspectRatio(false)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        RaiseIssue.mTempCropImageUri=null;
    }
}
