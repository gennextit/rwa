package com.gennext.rwa;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import com.gennext.rwa.fragment.profile.UpdateProfile;
import com.gennext.rwa.fragment.profile.UpdateProfileAllFields;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class UpdateProfileActivity extends BaseActivity {

    FragmentManager manager;
    public static final String UPDATE_PROFILE="updateProfile";
    public static final int LOGIN_PROCESS=1,MAIN_ACTIVITY=2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager=getSupportFragmentManager();
        Intent intent=getIntent();
        startScreen(intent);
    }

    private void startScreen(Intent intent) {
        if(intent!=null){
            int data=intent.getExtras().getInt(UPDATE_PROFILE);
            if(data==LOGIN_PROCESS){
                UpdateProfile updateProfile=UpdateProfile.newInstance();
                FragmentTransaction transaction=manager.beginTransaction();
                transaction.replace(android.R.id.content,updateProfile,"updateProfile");
                transaction.commit();
            } if(data==MAIN_ACTIVITY){
                Fragment updateProfileAllFields=UpdateProfileAllFields.newInstance();
                FragmentTransaction transaction=manager.beginTransaction();
                transaction.replace(android.R.id.content,updateProfileAllFields,"updateProfileAllFields");
                transaction.commit();
//                Intent ob=new Intent(UpdateProfileActivity.this,UpdateProfileAllFields.class);
//                startActivity(ob);
            }
        }else{
            Toast.makeText(getApplicationContext(),"Please restart app",Toast.LENGTH_LONG).show();
        }

    }
}
