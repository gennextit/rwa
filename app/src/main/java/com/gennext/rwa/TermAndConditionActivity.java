package com.gennext.rwa;

import android.os.Bundle;

import com.gennext.rwa.fragment.AppWebView;
import com.gennext.rwa.util.AppSettings;

/**
 * Created by Admin on 3/24/2018.
 */

public class TermAndConditionActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppWebView appWebView=new AppWebView();
        appWebView.setUrl(getResources().getString(R.string.t_and_c), AppSettings.TandC, AppWebView.URL);
        addFragmentWithoutBackstack(appWebView, "appWebView");
    }
}
