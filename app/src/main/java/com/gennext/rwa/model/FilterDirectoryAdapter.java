package com.gennext.rwa.model;

/**
 * Created by Abhijit on 14-Nov-16.
 */


import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.directory.Directory;
import com.gennext.rwa.fragment.directory.DirectoryDetail;
import com.gennext.rwa.util.L;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

// The standard text view adapter only seems to search from the beginning of whole words
// so we've had to write this whole class to make it possible to search
// for parts of the arbitrary string we want
public class FilterDirectoryAdapter extends BaseAdapter implements Filterable  {

    private final Activity context;
    private final Directory directory;
    private List<DirectoryModel> originalData = null;
    private List<DirectoryModel> filteredData = null;
    private LayoutInflater mInflater;
    private ItemFilter mFilter = new ItemFilter();
    private int filterCriteria;
    private static String sections = "abcdefghilmnopqrstuvz";

    public FilterDirectoryAdapter(Activity context, List<DirectoryModel> data, Directory directory) {
        this.context=context;
        this.filteredData = data;
        this.originalData = data;
        mInflater = LayoutInflater.from(context);
        this.directory=directory;
    }

    public int getCount() {
        return filteredData.size();
    }

    public DirectoryModel getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unnecessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.slot_directory, null);

            // Creates a ViewHolder and store references to the two children views
            // we want to bind data to.
            holder = new ViewHolder();
//            holder.text = (TextView) convertView.findViewById(R.id.list_view);
            holder.llSlot = (LinearLayout) convertView.findViewById(R.id.llslot);
            holder.ivSide = (ImageView) convertView.findViewById(R.id.iv_side);
            holder.llVehicle = (LinearLayout) convertView.findViewById(R.id.llvehicle);
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_slot_dir_name);
            holder.tvHouse = (TextView) convertView.findViewById(R.id.tv_slot_dir_house);
            holder.tvVehicle = (TextView) convertView.findViewById(R.id.tv_slot_dir_vehicle);
//            holder.slot = (LinearLayout) convertView.findViewById(R.id.ll_slot);
            // Bind the data efficiently with the holder.

            convertView.setTag(holder);
        } else {
            // Get the ViewHolder back to get fast access to the TextView
            // and the ImageView.
            holder = (ViewHolder) convertView.getTag();
        }

        // If weren't re-ordering this you could rely on what you set last time
//        holder.text.setText(filteredData.get(position));
//        setColor(holder.slot);
        String name = filteredData.get(position).getPersonName();
        holder.tvName.setText(name != null ? name : "Not Aavailable");
        String houseNo = filteredData.get(position).getHouseNo();
        holder.tvHouse.setText(houseNo != null ? houseNo : "Not Aavailable");
        String vehicleNo = filteredData.get(position).getVehicleNo();
        if(vehicleNo!=null && !vehicleNo.equals("")) {
            holder.llVehicle.setVisibility(View.VISIBLE);
            holder.tvVehicle.setText(vehicleNo);
        }else {
            holder.llVehicle.setVisibility(View.GONE);
        }
//        Drawable background = holder.tvHouse.getBackground();
//        setBackgroundColorAndRetainShape(getRandColor(),background);
        setColor(holder.ivSide);

        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DirectoryDetail directoryDetail=new DirectoryDetail();
                directoryDetail.setDetail(filteredData.get(position).getHouseNo(),filteredData.get(position).getPersonName(),
                        filteredData.get(position).getOccupation(),filteredData.get(position).getPhoneNo(),filteredData.get(position).getVehicleNo());
                directory.addFragment(directoryDetail,android.R.id.content,"directoryDetail");
            }
        });

        return convertView;
    }



    public void setFilterCriteria(int filterCriteria) {
        this.filterCriteria = filterCriteria;
    }

    static class ViewHolder {
        //        TextView text;
        private TextView tvName, tvHouse, tvVehicle;
//        private LinearLayout slot;
        private LinearLayout llVehicle,llSlot;
        public ImageView ivSide;
    }



    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<DirectoryModel> list = originalData;

            int count = list.size();
            final ArrayList<DirectoryModel> nlist = new ArrayList<>(count);
            String filterableText,filterByName,filterByHouse,filterByVehicle;

            for (DirectoryModel model : list) {

                if(!filterString.equals("")) {
                    filterableText = model.getHouseNo();
                    filterableText+= model.getVehicleNo();
                    filterableText+= model.getPersonName();
                    if (filterableText != null && filterableText.toLowerCase().contains(filterString))  {
                        nlist.add(model);
                    }
//                    filterableText = model.getVehicleNo();
//                    if (filterableText != null && filterableText.toLowerCase().contains(filterString)) {
//                        nlist.add(model);
//                    }
//                    filterableText = model.getPersonName();
//                    if (filterableText != null && filterableText.toLowerCase().contains(filterString)) {
//                        nlist.add(model);
//                    }
                }else{
                    nlist.add(model);
                }
            }
            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<DirectoryModel>) results.values;
            notifyDataSetChanged();
        }

    }
    public void setColor(View tv) {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        tv.setBackgroundColor(color);

    }

//    @Override
//    public int getPositionForSection(int section) {
//        if (section == 35) {
//            return 0;
//        }
//        for (int i=0; i < this.getCount(); i++) {
//            DirectoryModel item = this.getItem(i);
//            if (item.getPersonName()!=null&&item.getPersonName().toUpperCase().charAt(0) == section)
//                return i;
//        }
////        for (int i = 0; i < stringArray.size(); i++) {
////            String l = stringArray.get(i);
////            char firstChar = l.toUpperCase().charAt(0);
////            if (firstChar == section) {
////                return i;
////            }
////        }
//        return -1;
//    }
//
//    @Override
//    public int getSectionForPosition(int arg0 ) {
//        return 0;
//    }
//
//    @Override
//    public Object[] getSections() {
//        String[] sectionsArr = new String[sections.length()];
//        for (int i=0; i < sections.length(); i++)
//            sectionsArr[i] = "" + sections.charAt(i);
//        return sectionsArr;
//
//    }

    private void setSection(LinearLayout header, String label) {
        TextView text = new TextView(context);
        header.setBackgroundColor(0xffaabbcc);
        text.setTextColor(Color.WHITE);
        text.setText(label.substring(0, 1).toUpperCase());
        text.setTextSize(20);
        text.setPadding(5, 0, 0, 0);
        text.setGravity(Gravity.CENTER_VERTICAL);
        header.addView(text);
    }
    private int getRandColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    public static void setBackgroundColorAndRetainShape(final int color, final Drawable background) {

        if (background instanceof ShapeDrawable) {
            ((ShapeDrawable) background.mutate()).getPaint().setColor(color);
        } else if (background instanceof GradientDrawable) {
            ((GradientDrawable) background).setColor(color);
        } else if (background instanceof ColorDrawable) {
            ((ColorDrawable) background.mutate()).setColor(color);
        }else{
           L.m("Not a valid background type");
        }

    }
//                switch (filterCriteria){
//                    case ALL_FIELDS:
//                        filterByName = model.getPersonName();
//                        filterByHouse = model.getHouseNo();
//                        filterByVehicle = model.getVehicleNo();
//                    case PERSON_NAME:
//                        filterByName = model.getPersonName();
//                        filterByHouse = "";
//                        filterByVehicle = "";
//                        break;
//                    case HOUSE_NO:
//                        filterByName = model.getHouseNo();
//                        filterByHouse = "";
//                        filterByVehicle = "";
//                        break;
//                    case VEHICLE_NO:
//                        filterByName = model.getVehicleNo();
//                        filterByHouse = "";
//                        filterByVehicle = "";
//                        break;
//                    case PHONE_NO:
//                        filterByName = model.getPhoneNo();
//                        filterByHouse = "";
//                        filterByVehicle = "";
//                        break;
//                    case OCCUPATION:
//                        filterByName = model.getOccupation();
//                        filterByHouse = "";
//                        filterByVehicle = "";
//                        break;
//                    case SPOUSE_NAME:
//                        filterByName = model.getSpouseName();
//                        filterByHouse = "";
//                        filterByVehicle = "";
//                        break;
//                    default:
//                        filterByName="";
//                        filterByHouse = "";
//                        filterByVehicle = "";
//                        break;
//                }
}