package com.gennext.rwa.model;

/**
 * Created by Abhijit on 29-Sep-16.
 */


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.gennext.rwa.R;

import java.util.ArrayList;

public class GalleryViewAdapter extends ArrayAdapter<GalleryModel> {
    private ArrayList<GalleryModel> list;

    private Activity context;


    public GalleryViewAdapter(Activity context, int textViewResourceId, ArrayList<GalleryModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
    }


    @Override
    public int getCount() {
        return super.getCount();
    }

    class ViewHolder {
        TextView tvName;
        ImageView ivImage;
        LinearLayout progressBar;

        public ViewHolder(View v) {
            tvName = (TextView) v.findViewById(R.id.tv_slot_profile_selector_name);
            ivImage = (ImageView) v.findViewById(R.id.iv_slot_profile_selector);
            progressBar = (LinearLayout) v.findViewById(R.id.ll_progress);
        }
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_gallery_view, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        if (list.get(position).getImageUrl() != null) {
//                imageLoader.DisplayImage(list.get(position).getImageUrl(), holder.ivImage, "blank",false);
            final ViewHolder finalHolder = holder;
//            imageLoader.displayImage(list.get(position).getImageUrl(), holder.ivImage, options, new SimpleImageLoadingListener() {
//                @Override
//                public void onLoadingStarted(String imageUri, View view) {
//                    finalHolder.progressBar.setVisibility(View.VISIBLE);
//                }
//
//                @Override
//                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                    finalHolder.progressBar.setVisibility(View.GONE);
//                }
//
//                @Override
//                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                    finalHolder.progressBar.setVisibility(View.GONE);
//                }
//            }, new ImageLoadingProgressListener() {
//                @Override
//                public void onProgressUpdate(String imageUri, View view, int current, int total) {
////                                holder.progressBar.setProgress(Math.round(100.0f * current / total));
//                }
//            });

            Glide.with(context)
                    .load(list.get(position).getImageUrl())
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            finalHolder.progressBar.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            finalHolder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.ivImage);
        }

        return v;
    }

}
