package com.gennext.rwa.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 15-Oct-16.
 */

public class SliderModel {

    private String imgUrl;
    private String bannerUrl;

    private String output;
    private String outputMsg;

    private ArrayList<SliderModel>list;

    public String getImgUrl() {
        return imgUrl;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<SliderModel> getList() {
        return list;
    }

    public void setList(ArrayList<SliderModel> list) {
        this.list = list;
    }
}
