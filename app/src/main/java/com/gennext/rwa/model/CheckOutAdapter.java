package com.gennext.rwa.model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gennext.rwa.R;
import com.gennext.rwa.util.DBManager;
import com.gennext.rwa.util.imagecache.ImageLoader;

import java.util.ArrayList;

/**
 * Created by Abhijit on 11-Oct-16.
 */
public class CheckOutAdapter extends ArrayAdapter<CheckoutModel> {
    private final ArrayList<CheckoutModel> list;

    ImageLoader imageLoader;
    DBManager db;
    private final Activity context;
    private String calPrice;

    public CheckOutAdapter(Activity context, int textViewResourceId, ArrayList<CheckoutModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
        db = new DBManager(context);
        imageLoader=new ImageLoader(context);
    }

    class ViewHolder {
        TextView tvProductName, tvUnit, tvPrice, tvCount;
        ImageView ivImage, ivPlus, ivMinus;

        public ViewHolder(View v) {
            tvProductName = (TextView) v.findViewById(R.id.textView1);
            tvUnit = (TextView) v.findViewById(R.id.textView2);
            tvPrice = (TextView) v.findViewById(R.id.textView3);
            tvCount = (TextView) v.findViewById(R.id.textView4);
            ivImage = (ImageView) v.findViewById(R.id.imageView1);
            ivPlus = (ImageView) v.findViewById(R.id.imageView3);
            ivMinus = (ImageView) v.findViewById(R.id.imageView2);
        }
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_checkout, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }


        holder.ivPlus.setVisibility(View.INVISIBLE);
        holder.ivMinus.setVisibility(View.INVISIBLE);
        String name=list.get(position).getProductName();
        if(name!=null){
            holder.tvProductName.setText(name);
        }
        holder.tvUnit.setText("1");
        String price=list.get(position).getProductPrice();
        if(price!=null){
            holder.tvPrice.setText(price);
        }
        String quantity=list.get(position).getProductQuantity();
        if(quantity!=null){
            holder.tvCount.setText("Quantity "+quantity);
        }
        String image=list.get(position).getProductImage();
        if(image!=null){
            imageLoader.DisplayImage(image,holder.ivImage,"blank",false);
        }

//        if(list.get(position).getSubcat_id().equalsIgnoreCase("1")){
//            image.setImageResource(list.get(position).getDrawableSource());
//        }else if(list.get(position).getSubcat_id().equalsIgnoreCase("2")){
//            image.setImageResource(list.get(position).getDrawableSource());
//        }else{
//            image.setImageResource(R.drawable.bg_load);
//        }
//			plus.setOnClickListener(new View.OnClickListener() {
//
//				@Override
//				public void onClick(View arg0) {
//					// TODO Auto-generated method stub
//					int getQuantiity = Integer.parseInt(list.get(position).getU_subcat_quantity());
//					getQuantiity++;
//					replaceItemAt(position, list.get(position).getStore_category_id(),
//							list.get(position).getSubcat_id(), list.get(position).getU_subcat_id(),
//							list.get(position).getU_subcat_name(), list.get(position).getU_subcat_price(),
//							getQuantiity,list.get(position).getDrawableSource());
//					db.InsertProduct(list.get(position).getStore_category_id(), list.get(position).getSubcat_id(), list.get(position).getU_subcat_id(), list.get(position).getU_subcat_name(),
//							list.get(position).getU_subcat_price(), getQuantiity,list.get(position).getDrawableSource());
//					calPriceMethod();
//				}
//			});
//			minus.setOnClickListener(new View.OnClickListener() {
//
//				@Override
//				public void onClick(View arg0) {
//					// TODO Auto-generated method stub
//					int getQuantiity = Integer.parseInt(list.get(position).getU_subcat_quantity());
//					if (getQuantiity >0) {
//						getQuantiity--;
//						replaceItemAt(position, list.get(position).getStore_category_id(),
//								list.get(position).getSubcat_id(), list.get(position).getU_subcat_id(),
//								list.get(position).getU_subcat_name(), list.get(position).getU_subcat_price(),
//								getQuantiity,list.get(position).getDrawableSource());
//						db.InsertProduct(list.get(position).getStore_category_id(), list.get(position).getSubcat_id(), list.get(position).getU_subcat_id(), list.get(position).getU_subcat_name(),
//								list.get(position).getU_subcat_price(), getQuantiity,list.get(position).getDrawableSource());
//
//						deductPriceMethod(list.get(position).getU_subcat_price());
//
//						if(getQuantiity==0){
//							db.DeleteProduct(list.get(position).getStore_category_id(), list.get(position).getSubcat_id(), list.get(position).getU_subcat_id());
//							removeItem(position);
//						}
//
//					}
//
//
//				}
//			});

        // image.setImageResource(R.drawable.frouts_vegitable);

        return v;
    }

    public void replaceItemAt(int position, String StoreId, String SubCatId, String id, String name, String price,
                              int quantity,String imageSource) {
        // Replace the item in the array list
        CheckoutModel ob = new CheckoutModel();
        ob.setCategoryId(StoreId);
        ob.setSubCategoryId(SubCatId);
        ob.setProductId(id);
        ob.setProductName(name);
        ob.setProductPrice(String.valueOf(price));
        ob.setProductQuantity(String.valueOf(quantity));
        ob.setProductImage(imageSource);
        this.list.set(position, ob);
        // Let the custom adapter know it needs to refresh the view
        this.notifyDataSetChanged();
    }
    public void removeItem(int position) {
        this.list.remove(position);
        this.notifyDataSetChanged();
    }

}