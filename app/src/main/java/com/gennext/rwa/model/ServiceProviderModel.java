package com.gennext.rwa.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 17-Sep-16.
 */
public class ServiceProviderModel {

    private String categoryId;
    private String category;
    private String serviceProviderId;
    private String name;
    private String image;
    private String description;
    private String designation;
    private String mobile;
    private String rating;
    private String feedback;
    private String date;

    private ArrayList<ServiceProviderModel>childList;
    private ArrayList<ServiceProviderModel>childReviewList;

    private String output;
    private String outputMsg;

    public String getFeedback() {
        return feedback;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public ArrayList<ServiceProviderModel> getChildReviewList() {
        return childReviewList;
    }

    public void setChildReviewList(ArrayList<ServiceProviderModel> childReviewList) {
        this.childReviewList = childReviewList;
    }

    private ArrayList<ServiceProviderModel>list;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(String serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

    public String getOutput() {
        return output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<ServiceProviderModel> getList() {
        return list;
    }

    public void setList(ArrayList<ServiceProviderModel> list) {
        this.list = list;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public ArrayList<ServiceProviderModel> getChildList() {
        return childList;
    }

    public void setChildList(ArrayList<ServiceProviderModel> childList) {
        this.childList = childList;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
