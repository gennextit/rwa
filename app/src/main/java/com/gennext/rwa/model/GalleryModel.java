package com.gennext.rwa.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 29-Sep-16.
 */

public class GalleryModel {

    private String eventName;
    private String eventDate;

    private String imageUrl;

    private ArrayList<GalleryModel>eventList;
    private ArrayList<GalleryModel>eventListDetail;

    private String output;
    private String outputMsg;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public ArrayList<GalleryModel> getEventList() {
        return eventList;
    }

    public void setEventList(ArrayList<GalleryModel> eventList) {
        this.eventList = eventList;
    }

    public ArrayList<GalleryModel> getEventListDetail() {
        return eventListDetail;
    }

    public void setEventListDetail(ArrayList<GalleryModel> eventListDetail) {
        this.eventListDetail = eventListDetail;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
