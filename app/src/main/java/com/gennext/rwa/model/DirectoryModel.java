package com.gennext.rwa.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 12-Nov-16.
 */

public class DirectoryModel {

    private String personName;
    private String houseNo;
    private String phoneNo;
    private String vehicleNo;
    private String spouseName;
    private String occupation;

    private String output;
    private String outputMsg;
    private ArrayList<DirectoryModel> list;

    public void setData(String houseNo,String personName,String phoneNo,String vehicleNo,
                         String spouseName,String occupation){
        this.houseNo=houseNo;
        this.personName=personName;
        this.phoneNo=phoneNo;
        this.vehicleNo=vehicleNo;
        this.spouseName=spouseName;
        this.occupation=occupation;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public ArrayList<DirectoryModel> getList() {
        return list;
    }

    public void setList(ArrayList<DirectoryModel> list) {
        this.list = list;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }
}
