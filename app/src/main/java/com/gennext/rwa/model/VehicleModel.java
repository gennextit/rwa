package com.gennext.rwa.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 18-Nov-16.
 */

public class VehicleModel {

    private String vehicleId;
    private String vehicleNo;
    private String vehicleType;
    private String vehicleModel;

    private String output;
    private String outputMsg;
    private ArrayList<VehicleModel>list;


    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }


    public void setField(String vId,String vNo, String vType, String vModel){
        this.vehicleId=vId;
        this.vehicleNo=vNo;
        this.vehicleType=vType;
        this.vehicleModel=vModel;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<VehicleModel> getList() {
        return list;
    }

    public void setList(ArrayList<VehicleModel> list) {
        this.list = list;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }
}
