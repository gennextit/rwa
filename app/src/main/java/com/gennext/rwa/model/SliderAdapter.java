package com.gennext.rwa.model;


import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.rwa.R;
import com.gennext.rwa.fragment.AppWebView;
import com.gennext.rwa.util.Utility;

import java.util.ArrayList;

public class SliderAdapter extends PagerAdapter {
    private final ArrayList<SliderModel> list;

    //int NumberOfPages = 5;
    Activity context;
//    public ImageLoader imageLoader;
    FragmentManager mannager;
    public final int ADVERTISMENT=1;
    public final int EVENT=2;

//	int[] res = { R.drawable.bg, R.drawable.rss_main_bg,
//			R.drawable.sbuddy_profile_bg, R.drawable.sport_bg_medium,
//			R.drawable.rss_main_bg };
//	int[] backgroundcolor = { 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF };
//	String[] text = { "Event one", "Event two", "Event three", "Event four", "Event five" };
//	String[] location = { "Location Noida","Location Delhi","Location Gurgaon","Location Haryana","Location New Delhi",};

    public SliderAdapter(Activity context, ArrayList<SliderModel> list, FragmentManager fragmentManager) {
        this.list = list;
        this.context = context;
        this.mannager=fragmentManager;


    }

//	public RSSFeedSliderAdapter(Context context) {
//		this.context = context;
//	}


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        TextView textView = new TextView(context);
        textView.setTextColor(context.getResources().getColor(R.color.white));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        Utility.setTypsFace(context, textView);
        textView.setText(list.get(position).getBannerUrl());
        //textView.setText(Utility.setBoldFont(context, R.color.text, Typeface.NORMAL, ));
        textView.setPadding(5, 0, 0, 0);


        ImageView imageView = new ImageView(context);
        LayoutParams imageParams = new LayoutParams(LayoutParams.MATCH_PARENT, context.getResources().getDimensionPixelSize(R.dimen.slider_height));
        imageView.setLayoutParams(imageParams);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        if(list.get(position).getImgUrl()!=null){
            Glide.with(context)
                    .load(list.get(position).getImgUrl())
                    .into(imageView);
        }

        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        layout.setBackgroundColor(0xFFFFFF);
        layout.setGravity(Gravity.CENTER);
        layout.setLayoutParams(layoutParams);
        layout.addView(imageView);
//        layout.addView(textView);

        final int pos = position;
        layout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String banerDetail=list.get(pos).getBannerUrl();
                if(banerDetail!=null && !banerDetail.equals("")) {
//                    ViewBannerDetail viewBannerDetail = new ViewBannerDetail();
//                    viewBannerDetail.setData(list.get(pos).getImgUrl(), banerDetail);
//                    addFragment(viewBannerDetail, android.R.id.content, "viewBannerDetail");
                    AppWebView appWebView=new AppWebView();
                    appWebView.setUrl("RWA",banerDetail, AppWebView.URL);
                    addFragment(appWebView,android.R.id.content, "appWebView");
                }
            }
        });

        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    protected void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction =mannager.beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack("tag");
        transaction.commit();
    }
}
