package com.gennext.rwa.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 13-Oct-16.
 */

public class RaiseTicketModel {

    private String output;

    private ArrayList<String> list;

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public ArrayList<String> getList() {
        return list;
    }

    public void setList(ArrayList<String> list) {
        this.list = list;
    }
}
