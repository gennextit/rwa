package com.gennext.rwa.model;

import com.gennext.rwa.fragment.dailyNeeds.Checkout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Abhijit on 17-Oct-16.
 */


public class CheckoutJson {

    public static String toJSonArray(ArrayList<CheckoutModel> list) {
        try {
            // In this case we need a json array to hold the java list
            JSONArray jsonArr = new JSONArray();
            //JSONObject jsonOBJ = new JSONObject();
            CheckoutModel sport = new CheckoutModel();
            if (list != null) {
                for (CheckoutModel model : list) {
                    JSONObject pnObj = new JSONObject();
                    pnObj.put("categoryId", model.getCategoryId());
                    pnObj.put("subCategoryId", model.getSubCategoryId());
                    pnObj.put("productId", model.getProductId());
                    pnObj.put("productName", model.getProductName());
                    pnObj.put("productPrice", model.getProductPrice());
                    pnObj.put("productQuantity", model.getProductQuantity());
                    pnObj.put("imageUrl", model.getProductImage());
                    jsonArr.put(pnObj);
                    //jsonOBJ.put(pnObj);
                }
            } else {
                return null;
            }
            return jsonArr.toString();


        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return null;

    }

    public static String toVehicleJSonArray(ArrayList<VehicleModel> list) {
        try {
            // In this case we need a json array to hold the java list
            JSONArray jsonArr = new JSONArray();
            if (list != null) {
                for (VehicleModel model : list) {
                    JSONObject pnObj = new JSONObject();
                    pnObj.put("vehicleId", model.getVehicleId());
                    pnObj.put("vehicleNo", model.getVehicleNo());
                    pnObj.put("vehicleType", model.getVehicleType());
                    pnObj.put("vehicleModel", model.getVehicleModel());
                    jsonArr.put(pnObj);
                }
            } else {
                return null;
            }
            return jsonArr.toString();


        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}