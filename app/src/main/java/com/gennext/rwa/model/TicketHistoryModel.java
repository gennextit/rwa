package com.gennext.rwa.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 18-Oct-16.
 */

public class TicketHistoryModel {

    private String category;
    private String ticketNumber;
    private String reason;
    private String imageUrl;
    private String currentStatus;
    private String allocatedPerson;
    private String remarks;
    private String ticketRaisedDate;
    private String ticketRaisedTime;
    private String assignDate;
    private String assignTime;
    private String problemSolvedDate;
    private String problemSolvedTime;
    private String ticketClosedDate;
    private String ticketClosedTime;

    private String output;
    private String outputMsg;
    private ArrayList<TicketHistoryModel> list;


    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<TicketHistoryModel> getList() {
        return list;
    }

    public void setList(ArrayList<TicketHistoryModel> list) {
        this.list = list;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getAllocatedPerson() {
        return allocatedPerson;
    }

    public void setAllocatedPerson(String allocatedPerson) {
        this.allocatedPerson = allocatedPerson;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTicketRaisedDate() {
        return ticketRaisedDate;
    }

    public void setTicketRaisedDate(String ticketRaisedDate) {
        this.ticketRaisedDate = ticketRaisedDate;
    }

    public String getTicketRaisedTime() {
        return ticketRaisedTime;
    }

    public void setTicketRaisedTime(String ticketRaisedTime) {
        this.ticketRaisedTime = ticketRaisedTime;
    }

    public String getAssignDate() {
        return assignDate;
    }

    public void setAssignDate(String assignDate) {
        this.assignDate = assignDate;
    }

    public String getAssignTime() {
        return assignTime;
    }

    public void setAssignTime(String assignTime) {
        this.assignTime = assignTime;
    }

    public String getProblemSolvedDate() {
        return problemSolvedDate;
    }

    public void setProblemSolvedDate(String problemSolvedDate) {
        this.problemSolvedDate = problemSolvedDate;
    }

    public String getProblemSolvedTime() {
        return problemSolvedTime;
    }

    public void setProblemSolvedTime(String problemSolvedTime) {
        this.problemSolvedTime = problemSolvedTime;
    }

    public String getTicketClosedDate() {
        return ticketClosedDate;
    }

    public void setTicketClosedDate(String ticketClosedDate) {
        this.ticketClosedDate = ticketClosedDate;
    }

    public String getTicketClosedTime() {
        return ticketClosedTime;
    }

    public void setTicketClosedTime(String ticketClosedTime) {
        this.ticketClosedTime = ticketClosedTime;
    }
}
