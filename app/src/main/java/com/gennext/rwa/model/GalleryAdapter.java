package com.gennext.rwa.model;

/**
 * Created by Abhijit on 29-Sep-16.
 */


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.gennext.rwa.R;

import java.util.ArrayList;

public class GalleryAdapter extends ArrayAdapter<GalleryModel> {
    private ArrayList<GalleryModel> list;
    private ArrayList<GalleryModel> eventDetailList;

    private Activity context;
//    public ImageLoader imageLoader;

    public GalleryAdapter(Activity context, int textViewResourceId, ArrayList<GalleryModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
//        imageLoader = new ImageLoader(context.getApplicationContext());
//        imageLoader = ImageLoader.getInstance();
//        options = new DisplayImageOptions.Builder()
//                .showImageOnLoading(R.drawable.image8)
//                .showImageForEmptyUri(R.mipmap.error_connection)
//                .showImageOnFail(R.mipmap.error_connection)
//                .cacheInMemory(true)
//                .cacheOnDisk(true)
//                .considerExifParams(true)
//                .bitmapConfig(Bitmap.Config.RGB_565)
//                .build();
    }


//    @Override
//    public int getCount() {
//        if(list.size()>=8){
//            return 8;
//        }else{
//            return list.size();
//        }
//    }


    @Override
    public int getCount() {
        return super.getCount();
    }

    class ViewHolder {
        TextView tvName;
        ImageView ivImage0, ivImage1;
//        LinearLayout slot;
        LinearLayout progressBar;

        public ViewHolder(View v) {
            tvName = (TextView) v.findViewById(R.id.tv_slot_profile_selector_name);
            ivImage0 = (ImageView) v.findViewById(R.id.iv_slot_profile_selector0);
            ivImage1 = (ImageView) v.findViewById(R.id.iv_slot_profile_selector1);
//            slot = (LinearLayout) v.findViewById(R.id.ll_slot_profile_selector);
            progressBar = (LinearLayout) v.findViewById(R.id.ll_progress);
        }
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_gallery, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

//        holder.slot.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });

//        if (list.get(position).getSelect()) {
//            holder.ivImage.setBackgroundResource(R.drawable.rounded_select_image);
//            holder.tvName.setTypeface(Typeface.DEFAULT_BOLD);
//            holder.tvName.setTextColor(context.getResources().getColor(R.color.colorAccent));
//        } else {
//            holder.ivImage.setBackgroundResource(R.drawable.rounded_deselect_image);
//            holder.tvName.setTypeface(Typeface.DEFAULT);
//            holder.tvName.setTextColor(context.getResources().getColor(R.color.text_hint));
//        }

        if (list.get(position).getEventName() != null) {
            holder.tvName.setText(list.get(position).getEventName());
        }

        eventDetailList = list.get(position).getEventListDetail();
        if (eventDetailList.size() >= 1) {
            if (eventDetailList.get(0).getImageUrl() != null) {
//                imageLoader.DisplayImage(eventDetailList.get(0).getImageUrl(), holder.ivImage1, "galery0");
                final ViewHolder finalHolder = holder;
//                imageLoader.displayImage(eventDetailList.get(0 ).getImageUrl(), holder.ivImage1, options, new SimpleImageLoadingListener() {
//                    @Override
//                    public void onLoadingStarted(String imageUri, View view) {
//                        finalHolder.progressBar.setVisibility(View.VISIBLE);
//                    }
//
//                    @Override
//                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                        finalHolder.progressBar.setVisibility(View.GONE);
//                    }
//
//                    @Override
//                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                        finalHolder.progressBar.setVisibility(View.GONE);
//                    }
//                }, new ImageLoadingProgressListener() {
//                    @Override
//                    public void onProgressUpdate(String imageUri, View view, int current, int total) {
////                                holder.progressBar.setProgress(Math.round(100.0f * current / total));
//                    }
//                });

                Glide.with(context)
                        .load(eventDetailList.get(0).getImageUrl())
                        .placeholder(R.drawable.default_image)
                        .error(R.drawable.default_image)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                finalHolder.progressBar.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                finalHolder.progressBar.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.ivImage1);
            }
        }
        if (eventDetailList.size() >= 2) {
            if (eventDetailList.get(1).getImageUrl() != null) {
//                imageLoader.DisplayImage(eventDetailList.get(1).getImageUrl(), holder.ivImage0, "galery1");
                final ViewHolder finalHolder1 = holder;
//                imageLoader.displayImage(eventDetailList.get(1).getImageUrl(), holder.ivImage0, options, new SimpleImageLoadingListener() {
//                    @Override
//                    public void onLoadingStarted(String imageUri, View view) {
//                        finalHolder1.progressBar.setVisibility(View.VISIBLE);
//                    }
//
//                    @Override
//                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                        finalHolder1.progressBar.setVisibility(View.GONE);
//                    }
//
//                    @Override
//                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                        finalHolder1.progressBar.setVisibility(View.GONE);
//                    }
//                }, new ImageLoadingProgressListener() {
//                    @Override
//                    public void onProgressUpdate(String imageUri, View view, int current, int total) {
////                                holder.progressBar.setProgress(Math.round(100.0f * current / total));
//                    }
//                });

                Glide.with(context)
                        .load(eventDetailList.get(1).getImageUrl())
                        .placeholder(R.drawable.default_image)
                        .error(R.drawable.default_image)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                finalHolder1.progressBar.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                finalHolder1.progressBar.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.ivImage0);
            }
        }

        return v;
    }

}
