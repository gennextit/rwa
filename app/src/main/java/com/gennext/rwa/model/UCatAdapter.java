package com.gennext.rwa.model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.dailyNeeds.SubCategory;
import com.gennext.rwa.util.DBManager;
import com.gennext.rwa.util.L;
import com.gennext.rwa.util.imagecache.ImageLoader;

import java.util.ArrayList;

public class UCatAdapter extends ArrayAdapter<DailyNeedsModel> {
    private final ArrayList<DailyNeedsModel> list;
    private final int tabPos, subCatListPos;
    int itemQuantiry = 0;

    ImageLoader imageLoader;
    DBManager db;
    private final Activity context;
    DataTransferInterface dtInterface;

    public UCatAdapter(Activity context, int textViewResourceId, ArrayList<DailyNeedsModel> list, int subCatListPos, int tabPos) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.tabPos = tabPos;
        this.subCatListPos = subCatListPos;
        this.list = list;
        imageLoader = new ImageLoader(context);
        this.dtInterface = (DataTransferInterface) context;
        db = new DBManager(context);

    }

    class ViewHolder {
        TextView productName, unit, price, count;
        ImageView image, plus, minus;

        public ViewHolder(View v) {
            productName = (TextView) v.findViewById(R.id.textView1);
            unit = (TextView) v.findViewById(R.id.textView2);
            price = (TextView) v.findViewById(R.id.textView3);
            count = (TextView) v.findViewById(R.id.textView4);
            image = (ImageView) v.findViewById(R.id.imageView1);
            plus = (ImageView) v.findViewById(R.id.imageView3);
            minus = (ImageView) v.findViewById(R.id.imageView2);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_sub_category, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }


        if (list.get(position).getProductName() != null) {
            holder.productName.setText(list.get(position).getProductName());
        }
        holder.unit.setText("1");
        if (list.get(position).getProductPrice() != null) {
            holder.price.setText(list.get(position).getProductPrice());
        }
        if (list.get(position).getQuantity() != null) {
            holder.count.setText(list.get(position).getQuantity());
        }
        if (list.get(position).getProductImage() != null) {
            imageLoader.DisplayImage(list.get(position).getProductImage(), holder.image, "blank", false);
        }

        holder.plus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                float sumTotalPrice = 0.0f;
                try {
                    itemQuantiry = Integer.parseInt(list.get(position).getQuantity());
                    itemQuantiry++;
                    float prodPrice=Float.parseFloat(list.get(position).getProductPrice());
                    sumTotalPrice=prodPrice;
                } catch (NumberFormatException e) {
                    L.m(e.toString());
                }

//				AppTokens.countTotalItem++;

//				AppTokens.sumTotalPrice+=Float.parseFloat(list.get(position).getProductPrice());
                if (itemQuantiry != 0) {

                    dtInterface.setValues(itemQuantiry, sumTotalPrice);
                    replaceItemAt(position, list.get(position).getCategoryId(), list.get(position).getSubCategoryId(),
                            list.get(position).getProductID(), list.get(position).getProductName(),
                            list.get(position).getProductPrice(), itemQuantiry, list.get(position).getProductImage());
                    db.InsertProduct(list.get(position).getCategoryId(), list.get(position).getSubCategoryId(), list.get(position).getProductID(), list.get(position).getProductName().replaceAll("'", "''"),
                            list.get(position).getProductPrice(), itemQuantiry, list.get(position).getProductImage().replaceAll("'", "''"));

//                    itemQuantiry= ((SubCategory)context).countTotalItem;
                    ((SubCategory) context).countTotalItem ++;
                    ((SubCategory) context).sumTotalPrice += sumTotalPrice;
                    ((SubCategory) context).showCheckOutOption();

//                    sumTotalPrice=((SubCategory)context).sumTotalPrice;


                }
            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                float sumTotalPrice = 0.0f;
                try {
                    itemQuantiry = Integer.parseInt(list.get(position).getQuantity());
                } catch (NumberFormatException e) {
                    L.m(e.toString());
                }


                if (itemQuantiry > 0) {
                    itemQuantiry--;

//					AppTokens.countTotalItem--;
                    try {
                        sumTotalPrice = Float.parseFloat(list.get(position).getProductPrice());
                    } catch (NumberFormatException e) {
                        L.m(e.toString());
                }
//					AppTokens.sumTotalPrice-=Float.parseFloat(list.get(position).getU_subcat_price());

                    dtInterface.setValues(itemQuantiry, sumTotalPrice);

                    replaceItemAt(position, list.get(position).getCategoryId(), list.get(position).getSubCategoryId(),
                            list.get(position).getProductID(), list.get(position).getProductName(),
                            list.get(position).getProductPrice(), itemQuantiry, list.get(position).getProductImage());
                    db.InsertProduct(list.get(position).getCategoryId(), list.get(position).getSubCategoryId(), list.get(position).getProductID(), list.get(position).getProductName().replaceAll("'", "''"),
                            list.get(position).getProductPrice(), itemQuantiry, list.get(position).getProductImage().replaceAll("'", "''"));

                    ((SubCategory)context).sumTotalPrice-=sumTotalPrice;
                    ((SubCategory)context).countTotalItem--;
                    ((SubCategory) context).showCheckOutOption();
                    if (itemQuantiry == 0) {
                        ((SubCategory) context).hideCheckOutOption();
                        db.DeleteProduct(list.get(position).getCategoryId(), list.get(position).getSubCategoryId(), list.get(position).getProductID());

                    }
                }

            }
        });

        // image.setImageResource(R.drawable.frouts_vegitable);

        return v;
    }

    public void replaceItemAt(int position, String StoreId, String SubCatId, String id, String name, String price,
                              int quantity, String imageSource) {
        // Replace the item in the array list
        DailyNeedsModel ob = new DailyNeedsModel();
        ob.setCategoryId(StoreId);
        ob.setSubCategoryId(SubCatId);
        ob.setProductID(id);
        ob.setProductName(name);
        ob.setProductPrice(String.valueOf(price));
        ob.setQuantity(String.valueOf(quantity));
        ob.setProductImage(imageSource);
        this.list.set(position, ob);
        ArrayList<DailyNeedsModel> tempCatList = ((SubCategory) context).dailyNeedsList;
        ArrayList<DailyNeedsModel> tempSubCatList = tempCatList.get(subCatListPos).getSubCategoryList();
        ArrayList<DailyNeedsModel> tempProductList = tempSubCatList.get(tabPos).getProductlist();
        tempProductList.set(position, ob);
//		AppTokens.globalList.set(position, ob);		// Let the custom adapter know it needs to refresh the view
        this.notifyDataSetChanged();
    }

}