package com.gennext.rwa.model;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.gennext.rwa.R;

import java.util.ArrayList;

/**
 * Created by Abhijit on 11-Oct-16.
 */
public class SPReviewAdapter extends ArrayAdapter<ServiceProviderModel> {
    private final ArrayList<ServiceProviderModel> list;

    private final Activity context;

    public SPReviewAdapter(Activity context, int textViewResourceId, ArrayList<ServiceProviderModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
    }

    class ViewHolder {
        TextView tvReview, tvDate;
        RatingBar rbRating;

        public ViewHolder(View v) {
            tvReview = (TextView) v.findViewById(R.id.tv_slot_1);
            tvDate = (TextView) v.findViewById(R.id.tv_slot_2);
            rbRating = (RatingBar) v.findViewById(R.id.ratingBar1);

        }
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_review_popup, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }


        String feedback=list.get(position).getFeedback();
        if(feedback!=null){
            holder.tvReview.setText(feedback);
        }
        String date=list.get(position).getDate();
        if(date!=null){
            holder.tvDate.setText(date);
        }
        String rating=list.get(position).getRating();
        if(!TextUtils.isEmpty(rating)){
            try {
                holder.rbRating.setRating(Float.parseFloat(rating));
            }catch (NumberFormatException e){
            }
        }else{
            holder.rbRating.setRating(0.0f);
        }

        return v;
    }


}