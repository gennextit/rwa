package com.gennext.rwa.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 17-Sep-16.
 */
public class ImpContactModel {

    private String name;
    private String image;
    private String designation;
    private String mobile;
    private String category;
    private String detail;
    private String availableFrom;

    private ArrayList<ImpContactModel>childList;

    public String getAvailableFrom() {
        return availableFrom;
    }

    public void setAvailableFrom(String availableFrom) {
        this.availableFrom = availableFrom;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public ArrayList<ImpContactModel> getChildList() {
        return childList;
    }

    public void setChildList(ArrayList<ImpContactModel> childList) {
        this.childList = childList;
    }
}
