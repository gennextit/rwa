package com.gennext.rwa.model;

/**
 * Created by Abhijit on 29-Sep-16.
 */


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.gennext.rwa.R;

import java.util.ArrayList;
import java.util.Random;

public class CircularAdapter extends ArrayAdapter<CircularModel> {
    private ArrayList<CircularModel> list;

    private Activity context;
    public CircularAdapter(Activity context, int textViewResourceId, ArrayList<CircularModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
    }




    @Override
    public int getCount() {
        return super.getCount();
    }

    class ViewHolder {
        TextView tvTitle,tvDate,tvDescription;
        LinearLayout slot,llCircular;
        ImageView ivCircular;

        public ViewHolder(View v) {
            tvTitle = (TextView) v.findViewById(R.id.tv_slot_circular_title);
            tvDate = (TextView) v.findViewById(R.id.tv_slot_circular_date);
            tvDescription = (TextView) v.findViewById(R.id.tv_slot_circular_description);
            slot = (LinearLayout) v.findViewById(R.id.ll_slot);
            llCircular = (LinearLayout) v.findViewById(R.id.llcircular);
            ivCircular = (ImageView) v.findViewById(R.id.iv_circular);
        }
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_circular, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        setColor(holder.slot);
        String detail=list.get(position).getDetail();
        String cImage=list.get(position).getCircularUrl();
        if(TextUtils.isEmpty(cImage)) {
            holder.llCircular.setVisibility(View.VISIBLE);
            if (list.get(position).getTitle() != null) {
                holder.tvTitle.setText(list.get(position).getTitle());
            }
            if (list.get(position).getDateOfCreation() != null) {
                holder.tvDate.setText(list.get(position).getDateOfCreation());
            }
            if(!TextUtils.isEmpty(detail)) {
                holder.tvDescription.setText(detail);
            }
        }else{
            holder.llCircular.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(cImage)){
            holder.ivCircular.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(cImage)
                    .placeholder(R.drawable.image8)
                    .error(R.mipmap.error_connection)
                    .into(holder.ivCircular);
        }else {
            holder.ivCircular.setVisibility(View.GONE);
        }
        return v;
    }

    public void setColor(LinearLayout tv) {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        tv.setBackgroundColor(color);

    }


}
