package com.gennext.rwa.model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;


import com.gennext.rwa.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by akshay on 1/2/15.
 */
public class CountryCodeAdapter extends ArrayAdapter<CountryCodeModel> {

    Context context;
    int resource, textViewResourceId;
    List<CountryCodeModel> items, tempItems, suggestions;

    public CountryCodeAdapter(Activity context, int resource, int textViewResourceId, List<CountryCodeModel> items) {
        super(context, resource, textViewResourceId, items);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<CountryCodeModel>(items); // this makes the difference.
        suggestions = new ArrayList<CountryCodeModel>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.custom_slot, parent, false);
        }
        CountryCodeModel people = items.get(position);
        if (people != null) {
            TextView lblName = (TextView) view.findViewById(R.id.lbl_name);
            if (lblName != null)
                lblName.setText(people.getPhoneCode());
        }
        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((CountryCodeModel) resultValue).getPhoneCode();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
        	String charText=String.valueOf(constraint);
            if (constraint != null) {
                suggestions.clear();
                for (CountryCodeModel people : tempItems) {
                    if (people.getPhoneCode().contains(charText)) {
                        suggestions.add(people);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<CountryCodeModel> filterList = (ArrayList<CountryCodeModel>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (CountryCodeModel people : filterList) {
                    add(people);
                    notifyDataSetChanged();
                }
            }
        }
    };
}