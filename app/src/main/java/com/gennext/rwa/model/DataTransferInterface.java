package com.gennext.rwa.model;

public interface DataTransferInterface {
    public void setValues(int count, float sum);
}