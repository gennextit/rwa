package com.gennext.rwa.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 17-Oct-16.
 */

public class CircularModel {

    private String circularId;
    private String title;
    private String detail;
    private String dateOfCreation;
    private String circularUrl;

    private String output;
    private String outputMsg;

    private ArrayList<CircularModel>list;

    public String getCircularUrl() {
        return circularUrl;
    }

    public void setCircularUrl(String circularUrl) {
        this.circularUrl = circularUrl;
    }

    public String getCircularId() {
        return circularId;
    }

    public void setCircularId(String circularId) {
        this.circularId = circularId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(String dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<CircularModel> getList() {
        return list;
    }

    public void setList(ArrayList<CircularModel> list) {
        this.list = list;
    }
}
