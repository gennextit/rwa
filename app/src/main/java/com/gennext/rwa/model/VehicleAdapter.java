package com.gennext.rwa.model;

/**
 * Created by Abhijit on 29-Sep-16.
 */


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.rwa.R;

import java.util.ArrayList;
import java.util.Random;

public class VehicleAdapter extends ArrayAdapter<VehicleModel> {
    private ArrayList<VehicleModel> list;

    private Activity context;

    public VehicleAdapter(Activity context, int textViewResourceId, ArrayList<VehicleModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
    }




    @Override
    public int getCount() {
        return super.getCount();
    }

    class ViewHolder {
        TextView tvVehicleNo,tvVehicleModel,tvVehicleType;
        LinearLayout slot;

        public ViewHolder(View v) {
            tvVehicleNo = (TextView) v.findViewById(R.id.tv_slot_vehicle_no);
            tvVehicleType = (TextView) v.findViewById(R.id.tv_slot_vehicle_type);
            tvVehicleModel = (TextView) v.findViewById(R.id.tv_slot_vehicle_model);
            slot = (LinearLayout) v.findViewById(R.id.ll_slot);
        }
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_vehicle_list, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        if (list.get(position).getVehicleNo() != null) {
            holder.tvVehicleNo.setText(list.get(position).getVehicleNo());
        }
        if (list.get(position).getVehicleType() != null) {
            holder.tvVehicleType.setText(list.get(position).getVehicleType());
        }
        if (list.get(position).getVehicleModel() != null) {
            holder.tvVehicleModel.setText(list.get(position).getVehicleModel());
        }


        return v;
    }
    public void setColor(LinearLayout tv) {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        tv.setBackgroundColor(color);

    }

}
