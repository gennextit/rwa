package com.gennext.rwa.model;

/**
 * Created by Abhijit on 17-Sep-16.
 */

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.rwa.R;
import com.gennext.rwa.fragment.serviceProvider.ServiceProvider;
import com.gennext.rwa.util.L;

import java.util.ArrayList;

public class ServiceProviderAdapter extends BaseExpandableListAdapter {

    private final ServiceProvider parentRef;
    private Activity context;
    private ArrayList<ServiceProviderModel> groups;
    //    public ImageLoader imageLoader;
    private int lastExpandedGroupPosition;
    ExpandableListView listView;
    private AlertDialog dialog = null;


    public ServiceProviderAdapter(Activity context, ArrayList<ServiceProviderModel> groups, ExpandableListView listView,ServiceProvider parentRef) {
        this.context = context;
        this.groups = groups;
        this.parentRef = parentRef;
        this.listView = listView;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<ServiceProviderModel> chList = groups.get(groupPosition).getChildList();
        return chList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    class ChildHolder {
        TextView tvName, tvDesignation;
        ImageView ivImage;
        LinearLayout llCall, llDesignation;
        RatingBar ratingBar;

        public ChildHolder(View v) {
            tvName = (TextView) v.findViewById(R.id.child_name);
            ivImage = (ImageView) v.findViewById(R.id.child_icon);
            llCall = (LinearLayout) v.findViewById(R.id.ll_child_call);
            ratingBar = (RatingBar) v.findViewById(R.id.ratingBar1);
            tvDesignation = (TextView) v.findViewById(R.id.child_designation);
            llDesignation = (LinearLayout) v.findViewById(R.id.ll_designation);

        }
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        //collapse the old expanded group, if not the same
        //as new group to expand
        if (groupPosition != lastExpandedGroupPosition) {
            listView.collapseGroup(lastExpandedGroupPosition);
        }

        super.onGroupExpanded(groupPosition);
        lastExpandedGroupPosition = groupPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final ServiceProviderModel child = (ServiceProviderModel) getChild(groupPosition, childPosition);
        View v = convertView;
        ChildHolder childHolder = null;
        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.child_item, parent, false);
            childHolder = new ChildHolder(v);
            v.setTag(childHolder);
        } else {
            childHolder = (ChildHolder) v.getTag();
        }


        childHolder.tvName.setText(child.getName());
        if (!child.getRating().equals("") && !child.getRating().equals("0")) {
            try {
                childHolder.ratingBar.setRating(Float.parseFloat(child.getRating()));
            } catch (NumberFormatException e) {
                L.m(e.toString());
            }
        } else {
            childHolder.ratingBar.setRating(0.0f);
        }

        String designation = child.getDesignation().toString();
        if (!TextUtils.isEmpty(designation)) {
            childHolder.llDesignation.setVisibility(View.VISIBLE);
            childHolder.tvDesignation.setText(designation);
        } else {
            childHolder.llDesignation.setVisibility(View.GONE);
        }

        childHolder.llCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobile = child.getMobile();
                if (!mobile.contains(",")) {
                    parentRef.showCallAlertBox(context, child.getName(), mobile);
                } else {
                    String[] mobArray = mobile.split(",");
                    parentRef.showBulkCallAlertBox(context, child.getName(), mobArray);
                }
            }
        });
//        imageLoader.DisplayImage(child.getImage(), childHolder.ivImage,"Blank",false);
//        imageLoader.displayImage(child.getImage(), childHolder.ivImage, options);
        Glide.with(context)
                .load(child.getImage())
                .placeholder(R.drawable.profile)
                .error(R.drawable.profile)
                .into(childHolder.ivImage);
        return v;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<ServiceProviderModel> chList = groups.get(groupPosition).getChildList();
        return chList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    class GroupHolder {
        TextView tvName;
        ImageView ivGroup;

        public GroupHolder(View v) {
            tvName = (TextView) v.findViewById(R.id.group_name);
            ivGroup = (ImageView) v.findViewById(R.id.iv_group_icon);

        }
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        ServiceProviderModel group = (ServiceProviderModel) getGroup(groupPosition);
        View v = convertView;
        GroupHolder groupHolder = null;
        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.group_item, parent, false);
            groupHolder = new GroupHolder(v);
            v.setTag(groupHolder);
        } else {
            groupHolder = (GroupHolder) v.getTag();
        }
        if (isExpanded) {
            lastExpandedGroupPosition = groupPosition;
//            groupHolder.ivGroup.setImageResource(R.mipmap.ic_group_up);
        } else {
//            groupHolder.ivGroup.setImageResource(R.mipmap.ic_group_down);
        }
        int rotationAngle = isExpanded ? 180 : 0;  //toggle
        groupHolder.ivGroup.animate().rotation(rotationAngle).setDuration(300).start();
        groupHolder.tvName.setText(group.getCategory());
        return v;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void showCallAlertBox(Activity activity, String pernsonName, final String mobileNo) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_dialog, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
        LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
        LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);

        tvTitle.setText("Alert");
        tvDescription.setText("Are you sure to call " + pernsonName);

        button1.setText("Call");
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                Intent ob = new Intent(Intent.ACTION_CALL);
                ob.setData(Uri.parse("tel:" + mobileNo));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                context.startActivity(ob);
            }
        });
        button2.setText("Cancel");
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

    }

    public void showBulkCallAlertBox(Activity activity, String pernsonName, final String[] mobileArr) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_bulk_call_option, null);
        dialogBuilder.setView(v);
        TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
        ListView lvMain = (ListView) v.findViewById(R.id.lv_main);

        tvDescription.setText("Select number to call");

        ArrayAdapter adapter = new ArrayAdapter(activity, R.layout.slot_bulk_call, mobileArr);
        lvMain.setAdapter(adapter);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                dialog.dismiss();
                if (mobileArr != null) {
                    Intent ob = new Intent(Intent.ACTION_CALL);
                    ob.setData(Uri.parse("tel:" + mobileArr[pos]));
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    context.startActivity(ob);
                }
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

    }
}
