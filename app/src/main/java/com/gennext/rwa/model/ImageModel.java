package com.gennext.rwa.model;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by Abhijit on 20-Oct-16.
 */
public class ImageModel {

    private Bitmap imageBitmap;
    private Uri imagePath;

    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }

    public Uri getImagePath() {
        return imagePath;
    }

    public void setImagePath(Uri imagePath) {
        this.imagePath = imagePath;
    }
}
