package com.gennext.rwa.model;

/**
 * Created by Abhijit on 29-Sep-16.
 */


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.rwa.R;

import java.util.ArrayList;
import java.util.Random;

public class TicketHistoryAdapter extends ArrayAdapter<TicketHistoryModel> {
    private ArrayList<TicketHistoryModel> list;

    private Activity context;

    public TicketHistoryAdapter(Activity context, int textViewResourceId, ArrayList<TicketHistoryModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
    }




    @Override
    public int getCount() {
        return super.getCount();
    }

    class ViewHolder {
        TextView tvTicketNo,tvCategory,tvStatus,tvDate,tvTime;
        LinearLayout slot;

        public ViewHolder(View v) {
            tvTicketNo = (TextView) v.findViewById(R.id.tv_slot_ticket_number);
            tvCategory = (TextView) v.findViewById(R.id.tv_slot_ticket_cat);
            tvStatus = (TextView) v.findViewById(R.id.tv_slot_ticket_status);
            tvDate = (TextView) v.findViewById(R.id.tv_slot_ticket_date);
            tvTime = (TextView) v.findViewById(R.id.tv_slot_ticket_time);
            slot = (LinearLayout) v.findViewById(R.id.ll_slot);
        }
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_ticket_history, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        setColor(holder.slot);

        if (list.get(position).getTicketNumber() != null) {
            holder.tvTicketNo.setText(list.get(position).getTicketNumber());
        }
        if (list.get(position).getCategory() != null) {
            holder.tvCategory.setText(list.get(position).getCategory());
        }
        if (list.get(position).getCurrentStatus() != null) {
            holder.tvStatus.setText(list.get(position).getCurrentStatus());
        }
        if (list.get(position).getAssignDate() != null) {
            holder.tvDate.setText(list.get(position).getTicketRaisedDate());
        }
        if (list.get(position).getAssignTime() != null) {
            holder.tvTime.setText(list.get(position).getTicketRaisedTime());
        }

        return v;
    }

    public void setColor(LinearLayout tv) {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        tv.setBackgroundColor(color);

    }


}
