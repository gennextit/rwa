package com.gennext.rwa.model;

/**
 * Created by Abhijit on 29-Sep-16.
 */


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.rwa.R;

import java.util.ArrayList;
import java.util.Random;

public class DirectoryAdapter extends ArrayAdapter<DirectoryModel> {
    private ArrayList<DirectoryModel> list;

    private Activity context;

    public DirectoryAdapter(Activity context, int textViewResourceId, ArrayList<DirectoryModel> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
    }


    @Override
    public int getCount() {
        return super.getCount();
    }

    class ViewHolder {
        TextView tvName, tvHouse, tvVehicle;
        LinearLayout slot;

        public ViewHolder(View v) {
            tvName = (TextView) v.findViewById(R.id.tv_slot_dir_name);
            tvHouse = (TextView) v.findViewById(R.id.tv_slot_dir_house);
            tvVehicle = (TextView) v.findViewById(R.id.tv_slot_dir_vehicle);
            slot = (LinearLayout) v.findViewById(R.id.ll_slot);
        }
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_directory, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        setColor(holder.slot);

        if (list.get(position).getPersonName() != null) {
            holder.tvName.setText(list.get(position).getPersonName());
        }
        if (list.get(position).getHouseNo() != null) {
            holder.tvHouse.setText(list.get(position).getHouseNo());
        }
        if (list.get(position).getVehicleNo() != null) {
            holder.tvVehicle.setText(list.get(position).getVehicleNo());
        }

        return v;
    }

    public void setColor(LinearLayout tv) {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        tv.setBackgroundColor(color);

    }
}
