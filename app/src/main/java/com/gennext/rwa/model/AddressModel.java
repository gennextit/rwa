package com.gennext.rwa.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 13-Oct-16.
 */

public class AddressModel {
;
    private String societyId;
    private String society;

    private String output;;
    private ArrayList<String>list;

    private ArrayList<AddressModel>addressList;

    public ArrayList<AddressModel> getAddressList() {
        return addressList;
    }

    public void setAddressList(ArrayList<AddressModel> addressList) {
        this.addressList = addressList;
    }

    public String getSocietyId() {
        return societyId;
    }

    public void setSocietyId(String societyId) {
        this.societyId = societyId;
    }

    public String getSociety() {
        return society;
    }

    public void setSociety(String society) {
        this.society = society;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public ArrayList<String> getList() {
        return list;
    }

    public void setList(ArrayList<String> list) {
        this.list = list;
    }
}
