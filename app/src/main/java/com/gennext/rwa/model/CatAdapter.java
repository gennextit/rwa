package com.gennext.rwa.model;

/**
 * Created by Abhijit on 29-Sep-16.
 */


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gennext.rwa.R;
import com.gennext.rwa.util.imagecache.ImageLoader;

import java.util.ArrayList;

public class CatAdapter extends ArrayAdapter<DailyNeedsModel> {
	private ArrayList<DailyNeedsModel> list;

	private Activity context;
	public ImageLoader imageLoader;

	public CatAdapter(Activity context, int textViewResourceId, ArrayList<DailyNeedsModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
		imageLoader = new ImageLoader(context.getApplicationContext());
	}

	@Override
	public int getCount() {
		return super.getCount();
	}

	class ViewHolder {
		TextView tvCatName,tvDescription;
		ImageView ivCat;

		public ViewHolder(View v) {
			tvCatName = (TextView) v.findViewById(R.id.tv_slot_dn_cat_name);
//			tvDescription = (TextView) v.findViewById(R.id.tv_slot_dn_cat_description);
			ivCat = (ImageView) v.findViewById(R.id.iv_slot_dn_category);
		}
	}


	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder = null;

		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = inflater.inflate(R.layout.slot_daily_needs, parent, false);
			holder = new ViewHolder(v);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		if (list.get(position).getCategoryName() != null) {
			holder.tvCatName.setText(list.get(position).getCategoryName());
		}
		if (list.get(1).getUrl() != null) {
			imageLoader.DisplayImage(list.get(position).getUrl(), holder.ivCat, "blank",false);
		}
		return v;
	}

}
