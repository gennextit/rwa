package com.gennext.rwa;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.gennext.rwa.util.AppTokens;


/**
 * Created by Ravi on 09/07/15.
 */
public class SmsReceiver extends BroadcastReceiver {
    private static final String TAG = SmsReceiver.class.getSimpleName();
    public static final String ACTION = "com.gennext.rwa.SmsReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (Object aPdusObj : pdusObj) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) aPdusObj);
                    String senderAddress = currentMessage.getDisplayOriginatingAddress();
                    String message = currentMessage.getDisplayMessageBody();

                    Log.e(TAG, "Received SMS: " + message + ", Sender: " + senderAddress);

                    // if the SMS is not from our gateway, ignore the message
                    if (!senderAddress.toLowerCase().contains(AppTokens.SMS_ORIGIN.toLowerCase())) {
                        Log.e(TAG, "SMS is not for our app!");
                        return;
                    }

                    // verification code from sms
                    String verificationCode = getVerificationCode(message);

                    Log.e(TAG, "OTP received: " + verificationCode);

                    Intent in = new Intent(ACTION);
                    in.putExtra("CSResultCode", Activity.RESULT_OK);
                    in.putExtra("otp", verificationCode);
                    in.putExtra("status", "otp");
                    LocalBroadcastManager.getInstance(context).sendBroadcast(in);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Getting the OTP from sms message body
     * ':' is the separator of OTP from the message
     *
     * @param message
     * @return
     */
    private String getVerificationCode(String message) {
        String code = null;
        //int index = message.indexOf(AppTokens.OTP_DELIMITER);
        int index = 0;

        if (index != -1) {
            int start = index;
            int length = 6;
            code = message.substring(start, start + length);
//            L.m(code);
            return code;
        }

        return code;
    }
}
