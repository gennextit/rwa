package com.gennext.rwa;

/**
 * Created by Abhijit on 14-Jul-16.
 */


import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.rwa.fragment.About;
import com.gennext.rwa.fragment.AppFeedback;
import com.gennext.rwa.fragment.dailyNeeds.OrderHistory;
import com.gennext.rwa.fragment.raiseIssue.TicketHistory;
import com.gennext.rwa.fragment.vehicle.VehicleDetail;
import com.gennext.rwa.fragment.vehicle.VehicleList;
import com.gennext.rwa.model.SideMenu;
import com.gennext.rwa.model.SideMenuAdapter;
import com.gennext.rwa.util.L;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class BaseActivity extends AppCompatActivity {
    boolean conn = false;
    Builder alertDialog;
    ProgressDialog progressDialog;
    AlertDialog dialog = null;
    ImageView ActionBack;
    TextView ActionBarHeading;

    DrawerLayout dLayout;
    ListView dList;
    List<SideMenu> sideMenuList;
    SideMenuAdapter slideMenuAdapter;

    static int COACH = 1, VENUE = 2;
    protected int FINISH_ACTIVITY = 1, FINISH_FRAGMENT = 2;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    public void closeFragmentDialog(String dialogName) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(dialogName);
        if (fragment != null) {
            fragment.dismiss();
        }
    }

    public SpannableString setBoldFont(int colorId, int typeFaceStyle, String title) {
        SpannableString s = new SpannableString(title);
        Typeface externalFont = Typeface.createFromAsset(BaseActivity.this.getAssets(), "fonts/segoeui.ttf");
        s.setSpan(externalFont, 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new ForegroundColorSpan(getResources().getColor(colorId)), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new StyleSpan(typeFaceStyle), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return s;
    }


    public void setActionBarOption(String Title, final int Option) {
        LinearLayout ActionBack;
        ActionBack = (LinearLayout) findViewById(R.id.ll_actionbar_back);
        TextView tvTitle = (TextView) findViewById(R.id.actionbar_title);
//
        tvTitle.setText(Title);
        ActionBack.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View arg0) {
                //mannager.popBackStack();
                switch (Option) {
                    case 1://FINISH_ACTIVITY:
                        finish();
                        break;
                    case 2://FINISH_FRAGMENT
                        FragmentManager manager = getSupportFragmentManager();
                        manager.popBackStack();
                        break;
                }
            }
        });

    }

    public void showPDialog(Context context, String msg) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(msg);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void hideActionBar() {
        getSupportActionBar().hide();
    }

    public SpannableString setFont(String title) {
        SpannableString s = new SpannableString(title);
//		s.setSpan(new TypefaceSpan("CircularStd-Book.otf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return s;
    }

    public void hideKeybord(Activity act) {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(act.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }



    public SpannableStringBuilder subScr(int text) {

        SpannableStringBuilder cs = new SpannableStringBuilder(String.valueOf(text));
        cs.setSpan(new SubscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        cs.setSpan(new RelativeSizeSpan(0.75f), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return cs;
    }

    public void showPDialog(String msg) {
        progressDialog = new ProgressDialog(BaseActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // progressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.dialog_animation));
        progressDialog.setMessage(msg);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void dismissPDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    public static String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

    public boolean isConnectionAvailable() {

        if (isOnline() == false) {
            return false;
        } else {
            return true;
        }
    }

    public String LoadPref(String key) {
        return LoadPref(BaseActivity.this, key);
    }

    public String LoadPref(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String data = sharedPreferences.getString(key, "");
        return data;
    }

    public void SavePref(String key, String value) {
        SavePref(BaseActivity.this, key, value);
    }

    public void SavePref(Context context, String key, String value) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }

    public String convert(String res) {
        String UrlEncoding = null;
        try {
            UrlEncoding = URLEncoder.encode(res, "UTF-8");

        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        return UrlEncoding;
    }

    public boolean isOnline() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(BaseActivity.this.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        if (haveConnectedWifi == true || haveConnectedMobile == true) {
            L.m("Log-Wifi" + String.valueOf(haveConnectedWifi));
            L.m("Log-Mobile" + String.valueOf(haveConnectedMobile));
            conn = true;
        } else {
            L.m("Log-Wifi" + String.valueOf(haveConnectedWifi));
            L.m("Log-Mobile" + String.valueOf(haveConnectedMobile));
            conn = false;
        }

        return conn;
    }


    public String getSt(int id) {

        return getResources().getString(id);
    }

    private void EnableMobileIntent() {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.provider.Settings.ACTION_SETTINGS);
        startActivity(intent);

    }

    public String viewTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("h:mm a");
        df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return df.format(c.getTime());

    }

    public String getDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public String getTime12() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("h:mm a");
        df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return df.format(c.getTime());

    }

    public String getTime24() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df1 = new SimpleDateFormat("H:mm:ss");
        df1.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return df1.format(c.getTime());
    }

    public String getTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("H:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return df.format(c.getTime());
    }

    // give format like dd-MMM-yyyy,dd/MMM/yyyy
    public String viewDate(String format) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(format);
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public String vDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    // Enter Format eg.dd-MMM-yyyy,dd/MM/yyyy
    public String viewFormatDate(String format) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(format);
        String formattedDate = df.format(c.getTime());
        return formattedDate;

    }

    // missing 0 issue resolve
    // enter Date eg. dd-MM-yyyy
    // enter dateFormat eg. dd-MM-yyyy
    public String currectFormateDate(String Date, String dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        Date cDate = null;
        try {
            cDate = sdf.parse(Date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String formattedDate = sdf.format(cDate);
        return formattedDate;
    }

    public void showToast(String txt) {
        // Inflate the Layout
        Toast toast = Toast.makeText(BaseActivity.this, txt, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
        toast.show();
    }

    // enter fix Date eg.type 1- dd-MM-yyyy ,type 2- yyyy-MM-dd
    // enter dateFormat eg. dd-MM-yyyy,yyyy-MM-dd,
    public String convertFormateDate(String Date, int type, String dateFormat) {
        String Day, middle, Month, Year;
        String finalDate = Date;
        if (type == 1) {
            Day = Date.substring(0, 2);
            middle = Date.substring(2, 3);
            Month = Date.substring(3, 5);
            Year = Date.substring(6, 10);

        } else {
            Day = Date.substring(0, 4);
            middle = Date.substring(4, 5);
            Month = Date.substring(5, 7);
            Year = Date.substring(8, 10);
        }

        switch (dateFormat) {
            case "dd-MM-yyyy":
                finalDate = Day + middle + Month + middle + Year;
                break;
            case "yyyy-MM-dd":
                finalDate = Year + middle + Month + middle + Day;
                break;
            case "MM-dd-yyyy":
                finalDate = Month + middle + Day + middle + Year;
                break;
            default:
                finalDate = "Date Format Incorrest";
        }
        return finalDate;
    }


    protected void SetDrawer(final Activity act, LinearLayout iv) {
        // TODO Auto-generated method stub
        iv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean drawerOpen = dLayout.isDrawerOpen(dList);
                if (!drawerOpen) {
                    dLayout.openDrawer(dList);
                } else {
                    dLayout.closeDrawer(dList);
                }

            }
        });

        dLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        dList = (ListView) findViewById(R.id.left_drawer);
        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.side_menu_header, null, false);
        TextView tvName = (TextView) listHeaderView.findViewById(R.id.tv_slide_menu_header_name);
        ImageView ivProfile = (ImageView) listHeaderView.findViewById(R.id.iv_slide_menu_header_profile);

        dList.addHeaderView(listHeaderView);

        SideMenu s1 = new SideMenu("My Profile", R.drawable.ic_menu_profile);
        SideMenu s2 = new SideMenu("Add vehicle", R.drawable.ic_menu_vehicle);
        SideMenu s3 = new SideMenu("Ticket History", R.drawable.ic_menu_ticket);
        SideMenu s4 = new SideMenu("Feedback", R.drawable.ic_menu_feedback);
        SideMenu s5 = new SideMenu("About Us", R.drawable.ic_menu_info2);

//        SideMenu s4 = new SideMenu("Invite Friends", R.mipmap.ic_launcher);
//        SideMenu s4 = new SideMenu("User guide/T&C", R.mipmap.ic_launcher);
        // SideMenu s7 = new SideMenu("Logout", R.mipmap.menu8);

        sideMenuList = new ArrayList<>();
        sideMenuList.add(s1);
        sideMenuList.add(s2);
        sideMenuList.add(s3);
        sideMenuList.add(s4);
        sideMenuList.add(s5);
        // sideMenuList.add(s6);
        // sideMenuList.add(s7);

        slideMenuAdapter = new SideMenuAdapter(act, R.layout.side_menu_list_slot, sideMenuList);
        // adapter = new ArrayAdapter<String>(this, R.layout.listlayout, menu);

        dList.setAdapter(slideMenuAdapter);
        // dList.setSelector(android.R.color.holo_blue_dark);

        dList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {

                dLayout.closeDrawers();


                Intent intent;
                FragmentManager mannager;
                FragmentTransaction transaction;

                switch (position) {

                    case 1:
                        Intent i = new Intent(BaseActivity.this, UpdateProfileActivity.class);
                        i.putExtra(UpdateProfileActivity.UPDATE_PROFILE, UpdateProfileActivity.MAIN_ACTIVITY);
                        startActivity(i);
                        break;
                    case 2:
//                        OrderHistory orderHistory = new OrderHistory();
//                        mannager = getSupportFragmentManager();
//                        transaction = mannager.beginTransaction();
//                        transaction.replace(android.R.id.content, orderHistory, "orderHistory");
//                        transaction.addToBackStack("orderHistory");
//                        transaction.commit();
                        addFragment(new VehicleList(), android.R.id.content, "vehicleList");

//                        TicketHistory ticketHistory = new TicketHistory();
//                        mannager = getSupportFragmentManager();
//                        transaction = mannager.beginTransaction();
//                        transaction.replace(android.R.id.content, ticketHistory, "ticketHistory");
//                        transaction.addToBackStack("ticketHistory");
//                        transaction.commit();
                        break;
                    case 3:
                        setFragment(new TicketHistory(), android.R.id.content, "TicketHistory");

//                        AppFeedback appFeedback = new AppFeedback();
//                        mannager = getSupportFragmentManager();
//                        transaction = mannager.beginTransaction();
//                        transaction.replace(android.R.id.content, appFeedback, "appFeedback");
//                        transaction.addToBackStack("appFeedback");
//                        transaction.commit();

                        break;
                    case 4:
                        setFragment(new AppFeedback(), android.R.id.content, "AppFeedback");

                       /* try {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(Intent.EXTRA_SUBJECT, "sbuddy");
                            i.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.invite_friends_message));
                            startActivity(Intent.createChooser(i, "choose one"));
                        } catch (Exception e) { // e.toString();
                        }*/
                        break;
                    case 5:
                        setFragment(new About(), android.R.id.content, "about");
                        /*  UserGuide userGuide = new UserGuide();
                        mannager = getFragmentManager();
                        transaction = mannager.beginTransaction();
                        transaction.replace(android.R.id.content, userGuide, "userGuide");
                        transaction.addToBackStack("userGuide");
                        transaction.commit();*/

                        break;

                }

            }

        });
    }



    protected void replaceFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void setFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void addFragment(Fragment fragment, String tag) {
        addFragment(fragment, android.R.id.content, tag);
    }

    protected void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void addFragmentWithoutBackstack(Fragment fragment, String tag) {
        addFragmentWithoutBackstack(fragment,android.R.id.content, tag);
    }
    protected void addFragmentWithoutBackstack(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.commit();
    }

    // for 2 tabs
    public void setTabFragment(int containerId, Fragment addFragment, String fragTag
            , Fragment rFrag1, Fragment rFrag2, Fragment rFrag3) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (addFragment != null) {
            if (addFragment.isAdded()) { // if the fragment is already in container
                ft.show(addFragment);
            } else { // fragment needs to be added to frame container
                ft.add(containerId, addFragment, fragTag);
            }
        }

        // Hide 1st fragments
        if (rFrag1 != null && rFrag1.isAdded()) {
            ft.hide(rFrag1);
        }
        // Hide 2nd fragments
        if (rFrag2 != null && rFrag2.isAdded()) {
            ft.hide(rFrag2);
        }
        // Hide 2nd fragments
        if (rFrag3 != null && rFrag3.isAdded()) {
            ft.hide(rFrag3);
        }

        ft.commit();
    }
}