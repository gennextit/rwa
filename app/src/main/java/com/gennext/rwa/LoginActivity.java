package com.gennext.rwa;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;

import com.gennext.rwa.fragment.SplashWindow;
import com.gennext.rwa.fragment.login.SignUpMobile;
import com.gennext.rwa.fragment.login.SignUpMobileVerify;
import com.gennext.rwa.fragment.login.SignUpPassword;
import com.gennext.rwa.util.AppTokens;
import com.gennext.rwa.util.DetailsTransition;
import com.gennext.rwa.util.Utility;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements SignUpMobile.OnVerifyMobile, SignUpMobileVerify.OnSuccessVerifyedMobile {

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    final int MOBILE_SCREEN = 1, MOBILE_VERIFY_SCREEN = 2, PASSWORD_LOGIN_SCREEN = 3;
    FragmentManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        manager = getSupportFragmentManager();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_login, SplashWindow.newInstance(), "splashWindow")
                .commit();

    }

    public void setInitialScreen(View view) {
//        setScreenDynamically(MOBILE_SCREEN, view);
        setScreenDynamically(PASSWORD_LOGIN_SCREEN, view);
    }


    private void setScreenDynamically(int position, View view) {
        FragmentTransaction transaction;
        FragmentManager mannager = getSupportFragmentManager();
        switch (position) {
            case MOBILE_SCREEN:
                SignUpMobile signUpMobile = SignUpMobile.newInstance(LoginActivity.this);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && view != null) {
                    showTargetFragmentLollipop(signUpMobile, view);
                    return;
                }
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container_login, signUpMobile)
//                        .addToBackStack("signUpMobile")
                        .commit();

                break;
            case MOBILE_VERIFY_SCREEN:
                SignUpMobileVerify signUpMobileVerify = new SignUpMobileVerify();
                signUpMobileVerify.setCommunicator(LoginActivity.this);
                transaction = mannager.beginTransaction();
                transaction.replace(R.id.container_login, signUpMobileVerify, "signUpMobileVerify");
                transaction.addToBackStack("signUpMobileVerify");
                transaction.commit();
                break;
            case PASSWORD_LOGIN_SCREEN:
                SignUpPassword signUpPassword = SignUpPassword.newInstance();
                transaction = mannager.beginTransaction();
                transaction.replace(R.id.container_login, signUpPassword, "signUpPassword");
                transaction.addToBackStack("signUpPassword");
                transaction.commit();
                break;

        }
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void showTargetFragmentLollipop(SignUpMobile fragment, View forImageView) {
        fragment.setSharedElementEnterTransition(new DetailsTransition());
//        fragment.setEnterTransition(new Fade());
//        fragment.setExitTransition(new Fade());
        fragment.setSharedElementReturnTransition(new DetailsTransition());

        String transitionName = ViewCompat.getTransitionName(forImageView);
        getSupportFragmentManager()
                .beginTransaction()
                .addSharedElement(forImageView, transitionName)
                .replace(R.id.container_login, fragment, "signUpMobile")
                .commit();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    @Override
    public void onSuccessVerifyedMobile(Boolean status) {
        if (status) {
            Utility.SavePref(getApplicationContext(), AppTokens.SessionSignup, "success");
            Intent intent = new Intent(LoginActivity.this, UpdateProfileActivity.class);
            intent.putExtra(UpdateProfileActivity.UPDATE_PROFILE, UpdateProfileActivity.LOGIN_PROCESS);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onVerifyMobile(String sportId) {
        setScreenDynamically(MOBILE_VERIFY_SCREEN, null);
    }

    @Override
    public void onBackPressed() {
        int count = manager.getBackStackEntryCount();
        if (count > 0) {
            manager.popBackStack();

        } else {
            super.onBackPressed();
            finish();
        }

    }
}

