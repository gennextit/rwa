package com.gennext.rwa;


import android.animation.Animator;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;

import com.gennext.rwa.util.AppTokens;
import com.gennext.rwa.util.WidgetAnimation;

public class SplashScreen extends AppCompatActivity {
    final Context context = this;
    static String TAG = "splAshAcreen";
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;
    private ImageView ivSplashBackground;
    private ImageView ivsplashlogo;

    // ImageView iv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        this.ivsplashlogo = (ImageView) findViewById(R.id.iv_splash_logo);
        this.ivSplashBackground = (ImageView) findViewById(R.id.iv_splash_background);


        WidgetAnimation.zoomInForSplash(ivSplashBackground, new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                startTimerForMainActivity2(ivsplashlogo);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    private void startTimerForMainActivity2(View view) {
        if (!LoadPref(AppTokens.SessionSignup).equals("")) {
            if (!LoadPref(AppTokens.SessionProfile).equals("")) {
                Intent intent = new Intent(SplashScreen.this, LoadData.class);
                startService(intent);
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);
                finish();
            } else {
                Intent i = new Intent(SplashScreen.this, UpdateProfileActivity.class);
                i.putExtra(UpdateProfileActivity.UPDATE_PROFILE,UpdateProfileActivity.LOGIN_PROCESS);
                startActivity(i);
                finish();
            }
        } else {
            Intent it = new Intent(SplashScreen.this, LoginActivity.class);
            startActivity(it);
            finish();
        }
    }


    private void startTimerForMainActivity() {
        // TODO Auto-generated method stub
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                if (!LoadPref(AppTokens.SessionSignup).equals("")) {
                    if (!LoadPref(AppTokens.SessionProfile).equals("")) {
                        Intent intent = new Intent(SplashScreen.this, LoadData.class);
                        startService(intent);
                        Intent i = new Intent(SplashScreen.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        Intent i = new Intent(SplashScreen.this, UpdateProfileActivity.class);
                        i.putExtra(UpdateProfileActivity.UPDATE_PROFILE,UpdateProfileActivity.LOGIN_PROCESS);
                        startActivity(i);
                        finish();
                    }
                } else {
                    Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }

            }
        }, SPLASH_TIME_OUT);
    }

    public String LoadPref(String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String data = sharedPreferences.getString(key, "");
        return data;
    }


}
