package com.gennext.rwa;

/**
 * Created by Abhijit on 20-Sep-16.
 */


import android.app.Service;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.IBinder;

import com.gennext.rwa.model.JsonModel;
import com.gennext.rwa.util.AppSettings;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.HttpReq;
import com.gennext.rwa.util.JsonParser;
import com.gennext.rwa.util.L;
import com.gennext.rwa.util.internet.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class LoadData extends Service {

    private boolean conn = false;
    private ConnectivityManager cm;
    private AsyncRequest asyncRequest;
    private String msg = "LoadDataServices ";
    private String GlobalData;
    private int timeInterval = 60;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        cm = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        //refreshPage();
        if (isOnline()) {
            asyncRequest=new AsyncRequest();
//            if (Build.VERSION.SDK_INT >= 11) {
//                //--post GB use serial executor by default --
//                asyncRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,AppSettings.Login);
//            } else {
                //--GB uses ThreadPoolExecutor by default--
                asyncRequest.execute(AppSettings.Login);
//            }
//            new AsyncRequest().execute(AppSettings.Login);
        } else {
            L.m("Internet Error "+ getResources().getString(R.string.internet_error_tag));
        }
        return START_STICKY;
    }

    public class AsyncRequest extends AsyncTask<String, Void, JsonModel> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected JsonModel doInBackground(String... urls) {
            L.m(msg+ "doInBackground");
            String response=null;
            List<BasicNameValuePair> params=new ArrayList<>();
            params.add(new BasicNameValuePair("mobile", AppUser.getMOBILE(getApplicationContext())));
            params.add(new BasicNameValuePair("userId", AppUser.getRegistrationId(getApplicationContext())));


            HttpReq ob = new HttpReq();
            response = ob.makeConnection(urls[0], HttpReq.POST, params);
            JsonParser jsonParser = new JsonParser();
            return jsonParser.SaveTempData(getApplicationContext(),response);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(JsonModel result) {
                if (result != null) {
                    if (result.getOutput().equalsIgnoreCase("success")) {
                       // Toast.makeText(getApplicationContext(),"load "+ result, Toast.LENGTH_SHORT).show();
                        L.m("load "+ result);
                    } else {
                        L.m("Error "+ JsonParser.ERRORMESSAGE);
                        //Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    L.m("Error "+ JsonParser.ERRORMESSAGE);
                    //Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
                }
        }

        @Override
        protected void onCancelled() {

        }

    }


    public boolean isOnline() {
       return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        L.m(msg+ "onDestroy");

    }
}
