package com.gennext.rwa.admin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.util.AppAnimation;

/**
 * Created by Admin on 7/4/2017.
 */

public class IssueList extends CompactFragment {

    public static Fragment newInstance() {
        IssueList fragment=new IssueList();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.admin_issue_list,container,false);
        initUi(v);
        return v;
    }

    private void initUi(View v) {

    }


}