package com.gennext.rwa;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.transition.Explode;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.rwa.fragment.RatingPopupAlert;
import com.gennext.rwa.fragment.circular.Circular;
import com.gennext.rwa.fragment.dailyNeeds.SubCategory;
import com.gennext.rwa.fragment.directory.Directory;
import com.gennext.rwa.fragment.gallery.Gallery;
import com.gennext.rwa.fragment.homeScreen.OptionMenu1;
import com.gennext.rwa.fragment.impContact.ImpContact;
import com.gennext.rwa.fragment.raiseIssue.RaiseIssue;
import com.gennext.rwa.fragment.serviceProvider.ServiceProvider;
import com.gennext.rwa.notification.AdvertiseNotification;
import com.gennext.rwa.notification.NotificationDialog;
import com.gennext.rwa.notification.PushNotification;
import com.gennext.rwa.util.AppUser;
import com.gennext.rwa.util.DBManager;
import com.gennext.rwa.util.GCMTokens;
import com.gennext.rwa.util.L;
import com.gennext.rwa.util.Rating;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

public class MainActivity extends BaseActivity implements NotificationDialog.NotificationDialogListener {
    FragmentManager manager;
    LinearLayout llActionBack;
    TextView tvTitle;
    DBManager dbManager;
    private BroadcastReceiver mHandleMessageReceiver;

    // [START declare_analytics]
    private FirebaseAnalytics mFirebaseAnalytics;
    // [END declare_analytics]

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // [START shared_app_measurement]
        // Obtain the FirebaseAnalytics instance.
        AppCenter.start(getApplication(), "7e1a521b-c5c6-479a-aab6-2205f77f6e6c", Analytics.class, Crashes.class);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        // [END shared_app_measurement]

        if (Build.VERSION.SDK_INT >= 21) {
            // set an enter transition
            getWindow().setEnterTransition(new Explode());
            // set an exit transition
            getWindow().setExitTransition(new Explode());
        }
//        setActionBarOption("RWA", FINISH_ACTIVITY);
        manager = getSupportFragmentManager();
        dbManager = new DBManager(this);
        LinearLayout llHomeMenu = (LinearLayout) findViewById(R.id.ll_actionbar_home);
        setDrawer(llHomeMenu);
        initUi();
        setHomeScreen();
        setAppRating();
        // Send initial screen screen view hit.
    }

    private void setAppRating() {
        if(Rating.showRating(getApplicationContext())){
            addFragment(new RatingPopupAlert(),"ratingPopupAlert");
        }
    }


    /**
     * This sample has a single Activity, so we need to manually record "screen views" as
     * we change fragments.
     */
    public void recordScreenView(String OpenedScreenName) {
        // This string must be <= 36 characters long in order for setCurrentScreen to succeed.
        String screenName = getResources().getString(R.string.app_name) + "-"+ AppUser.getRegistrationId(this) +"-"+ OpenedScreenName;
        // [START set_current_screen]
        mFirebaseAnalytics.setCurrentScreen(this, screenName, null /* class override */);
        // [END set_current_screen]
    }

    public void logCustomEvent(String logTitle,String logDetailKey,String logDetail) {
        // [START custom_event]
        Bundle params = new Bundle();
        params.putString(logDetailKey, logDetail);
//        params.putString("full_text", text);
        mFirebaseAnalytics.logEvent(logTitle, params);
        // [END custom_event]
    }

    public void recordMenuView(String sltMenu) {

        // [START image_view_event]
        Bundle bundle = new Bundle();
        String id=AppUser.getMOBILE(getApplicationContext());
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, sltMenu);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE,sltMenu);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        // [END image_view_event]
    }

    private void setHomeScreen() {
        setActionBarTitle("RWA");
        OptionMenu1 optionMenu1 = new OptionMenu1();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.container_main, optionMenu1, "optionMenu1");
        transaction.commit();

        recordMenuView("Home Menu");
    }

    public void setDrawer(LinearLayout llHome) {
        SetDrawer(MainActivity.this, llHome);
    }


    public void setDailyNeeds() {
//        setActionBarTitle("DAILY NEEDS");
        dbManager.DropTable();
        Intent intent = new Intent(MainActivity.this, SubCategory.class);
        startActivity(intent);
    }

    private void startActivityToSetImage() {
        RaiseIssue mainCreateEvent = (RaiseIssue) manager.findFragmentByTag("raiseIssue");
        if (mainCreateEvent != null) {
            if (RaiseIssue.mTempCropImageUri != null) {
                mainCreateEvent.setCropedImageAndUri(null, RaiseIssue.mTempCropImageUri);
            }
        }
    }

    public void setServiceProvider() {
        addFragment(ServiceProvider.newInstance(),"serviceProvider");
//        FragmentTransaction transaction = manager.beginTransaction();
//        transaction.add(android.R.id.content, serviceProvider, "serviceProvider");
//        transaction.addToBackStack("serviceProvider");
//        transaction.commit();

        recordMenuView("Service Provider");
    }
    public void setImpContact() {
        addFragment(ImpContact.newInstance(),"impContact");

//        ImpContact impContact = new ImpContact();
//        FragmentTransaction transaction = manager.beginTransaction();
//        transaction.add(android.R.id.content, impContact, "impContact");
//        transaction.addToBackStack("impContact");
//        transaction.commit();
        recordMenuView("Imp Contact");
    }

    public void setRaiseIssue() {
        addFragment(RaiseIssue.newInstance(),"raiseIssue");
//        RaiseIssue raiseIssue = new RaiseIssue();
//        FragmentTransaction transaction = manager.beginTransaction();
//        transaction.add(android.R.id.content, raiseIssue, "raiseIssue");
//        transaction.addToBackStack("raiseIssue");
//        transaction.commit();

        recordMenuView("Raise Issue");
    }


    public void setGallery() {
        addFragment(Gallery.newInstance(),"gallery");
//        Gallery gallery = new Gallery();
//        FragmentTransaction transaction = manager.beginTransaction();
//        transaction.add(android.R.id.content, gallery, "gallery");
//        transaction.addToBackStack("gallery");
//        transaction.commit();

        recordMenuView("Gallery");
    }

    public void setDirectory() {
        addFragment(Directory.newInstance(),"directory");
//        Directory directory = new Directory();
//        FragmentTransaction transaction = manager.beginTransaction();
//        transaction.add(android.R.id.content, directory, "directory");
//        transaction.addToBackStack("directory");
//        transaction.commit();
        recordMenuView("Directory");
    }

    public void setCircular() {
        addFragment(Circular.newInstance(),"circular");
//        Circular circular = new Circular();
//        FragmentTransaction transaction = manager.beginTransaction();
//        transaction.add(android.R.id.content, circular, "circular");
//        transaction.addToBackStack("circular");
//        transaction.commit();
        recordMenuView("Circular");
    }

    private void initUi() {
        llActionBack = (LinearLayout) findViewById(R.id.ll_actionbar_back);
        tvTitle = (TextView) findViewById(R.id.actionbar_title);
//        setActionBarOption();

        Bundle intent = getIntent().getExtras();

        if (intent != null) {
            String rec = intent.getString("state");
            if (rec != null)
                if (rec.equals(GCMTokens.PUSH_NOTIFICATION)) {
                    String message = intent.getString("message");
                    showNotification(GCMTokens.PUSH_NOTIFICATION, "RWA Notification", message);
                } else if (rec.equals(GCMTokens.CIRCULAR_NOTIFICATION)) {
                    String message = intent.getString("message");
                    showNotification(GCMTokens.CIRCULAR_NOTIFICATION, "Circular Notification", message);
                } else if (rec.equals(GCMTokens.ADVERTISE_NOTIFICATION)) {
                    String message = intent.getString("message");
                    String url = intent.getString("url");
                    if (!TextUtils.isEmpty(url)) {
                        AdvertiseNotification advertiseNotification = new AdvertiseNotification();
                        advertiseNotification.setUrl("RWA", url, AdvertiseNotification.URL);
                        addFragment(advertiseNotification, android.R.id.content, "advertiseNotification");
                    } else {
                        showNotification(GCMTokens.ADVERTISE_NOTIFICATION, "RWA", message);
                    }
                }
            L.m(rec);
        }

        mHandleMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                String noti = intent.getAction();
                if (noti.equals(GCMTokens.PUSH_NOTIFICATION)) {
                    String rec = intent.getStringExtra("state");
                    if (rec != null)
                        if (rec.equals(GCMTokens.PUSH_NOTIFICATION)) {
                            String message = intent.getStringExtra("message");
                            showNotification(GCMTokens.PUSH_NOTIFICATION, "RWA Notification", message);
                        } else if (rec.equals(GCMTokens.CIRCULAR_NOTIFICATION)) {
                            String message = intent.getStringExtra("message");
                            showNotification(GCMTokens.CIRCULAR_NOTIFICATION, "Circular Notification", message);
                        } else if (rec.equals(GCMTokens.ADVERTISE_NOTIFICATION)) {
                            String message = intent.getStringExtra("message");
                            String url = intent.getStringExtra("url");
                            if (!TextUtils.isEmpty(url)) {
                                AdvertiseNotification advertiseNotification = new AdvertiseNotification();
                                advertiseNotification.setUrl("RWA", url, AdvertiseNotification.URL);
                                addFragment(advertiseNotification, android.R.id.content, "advertiseNotification");
                            } else {
                                showNotification(GCMTokens.ADVERTISE_NOTIFICATION, "RWA", message);
                            }
                        }
                }
            }
        };

    }

    private void showNotification(String rec, String title, String message) {
        if (rec.equals(GCMTokens.PUSH_NOTIFICATION)) {
            PushNotification pushNotification = new PushNotification();
            pushNotification.set(title, message, GCMTokens.PUSH_NOTIFICATION);
            addFragment(pushNotification, android.R.id.content, "pushNotification");
//            NotificationDialog.newInstance(title, message, GCMTokens.PUSH_NOTIFICATION, this).
//                    show(getSupportFragmentManager(), "notificationDialog");
        } else if (rec.equals(GCMTokens.CIRCULAR_NOTIFICATION)) {
            PushNotification pushNotification = new PushNotification();
            pushNotification.set(title, message, GCMTokens.CIRCULAR_NOTIFICATION);
            addFragment(pushNotification, android.R.id.content, "pushNotification");
//            NotificationDialog.newInstance(title, message, GCMTokens.CIRCULAR_NOTIFICATION, this).
//                    show(getSupportFragmentManager(), "notificationDialog");
        } else if (rec.equals(GCMTokens.ADVERTISE_NOTIFICATION)) {
            NotificationDialog.newInstance(title, message, GCMTokens.ADVERTISE_NOTIFICATION, this).
                    show(getSupportFragmentManager(), "notificationDialog");
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        recordScreenView("Home Screen   ");

        startActivityToSetImage();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(mHandleMessageReceiver,
                new IntentFilter(GCMTokens.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(mHandleMessageReceiver,
                new IntentFilter(GCMTokens.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
//        NotificationUtils.clearNotifications(MainActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(mHandleMessageReceiver);
        closeFragmentDialog("notificationDialog");
    }

    //    @Override
//    public void onPouse() {
//        super.onPouse();
//        LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(mHandleMessageReceiver);
//    }


    private void setActionBarTitle(String title) {
        tvTitle.setText(title);
    }
//    public void setActionBarOption() {
//        llActionBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//                if (manager.getBackStackEntryCount() > 0) {
//                    //L.m("MainActivity" + "popping backstack: "+mannager.getBackStackEntryCount());
//                    int count = manager.getBackStackEntryCount();
//                    //mannager.popBackStack("mainRSSFeed", 0);
//                    manager.popBackStack();
//                    if (count <= 1) {
//                        tvTitle.setText("RWA");
//                    }
////			for(int i=count-1;i>=0;i--){
////				FragmentManager.BackStackEntry entry=mannager.getBackStackEntryAt(i);
////				L.m("BackButton pressed "+entry.getName());
////			}
//                } else {
//                    finish();
//                }
//
//            }
//        });
//
//    }

    @Override
    public void onBackPressed() {
        if (manager.getBackStackEntryCount() > 0) {
            //L.m("MainActivity" + "popping backstack: "+mannager.getBackStackEntryCount());
            int count = manager.getBackStackEntryCount();
            //mannager.popBackStack("mainRSSFeed", 0);
            manager.popBackStack();
            if (count <= 1) {
                tvTitle.setText("RWA");
            }
//			for(int i=count-1;i>=0;i--){
//				FragmentManager.BackStackEntry entry=mannager.getBackStackEntryAt(i);
//				L.m("BackButton pressed "+entry.getName());
//			}
        } else {
            L.m("MainActivity" + "nothing on backstack, calling super");
            super.onBackPressed();
            finish();
        }
    }


    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String category) {
        if (category.equals(GCMTokens.PUSH_NOTIFICATION)) {

        } else if (category.equals(GCMTokens.CIRCULAR_NOTIFICATION)) {
            setCircular();
        } else if (category.equals(GCMTokens.ADVERTISE_NOTIFICATION)) {

        }
    }


//    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String newMessage = intent.getExtras().getString(GCMTokens.EXTRA_MESSAGE);
//            // Waking up mobile if it is sleeping
//            WakeLocker.acquire(getApplicationContext());
//
//            /**
//             * Take appropriate action on this message
//             * depending upon your app requirement
//             * For now i am just displaying it on the screen
//             * */
//            Toast.makeText(getApplicationContext(), "New Message recieved from GCM: \n" + newMessage, Toast.LENGTH_LONG)
//                    .show();
//
//            // Showing received message
//            //Toast.makeText(getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();
//            // Releasing wake lock
//            WakeLocker.release();
//        }
//    };
//
//    @Override
//    public void onDestroy() {
//
//        try {
//            unregisterReceiver(mHandleMessageReceiver);
//            GCMRegistrar.onDestroy(this);
//        } catch (Exception e) {
//            L.m("UnRegister Receiver Error" + "> " + e.getMessage());
//        }
//        super.onDestroy();
//    }


}
