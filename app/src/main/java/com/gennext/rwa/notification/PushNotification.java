package com.gennext.rwa.notification;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.rwa.MainActivity;
import com.gennext.rwa.R;
import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.util.AnimationClass;
import com.gennext.rwa.util.GCMTokens;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class PushNotification extends CompactFragment {
    private Button btnOK;
    private FragmentManager manager;
    private LinearLayout RLView;
    private AnimationClass anim;


    private String mTitle;
    private String mMessage,category;

    public void set(String title, String message, String category) {
        this.mTitle = title;
        this.mMessage = message;
        this.category=category;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_push_notification, container, false);
        manager = getFragmentManager();
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        btnOK = (Button) v.findViewById(R.id.btn_noti);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_noti_title);
        TextView tvDescription = (TextView) v.findViewById(R.id.tv_noti_detail);
        RLView = (LinearLayout) v.findViewById(R.id.llnotidialog);
        ImageView ivImage = (ImageView) v.findViewById(R.id.iv_profile);
        LinearLayout llClose = (LinearLayout) v.findViewById(R.id.ll_whitespace);

        tvTitle.setText(mTitle);
        tvDescription.setText(mMessage);
        btnOK.setText("Ok");

        anim=new AnimationClass();
        anim.setPopupAnimation(RLView);

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.popBackStack();
            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                if(category.equals(GCMTokens.PUSH_NOTIFICATION)){
                    manager.popBackStack();
                }else if(category.equals(GCMTokens.CIRCULAR_NOTIFICATION)){
                    manager.popBackStack();
                    ((MainActivity)getActivity()).setCircular();
                }
            }
        });
    }


}