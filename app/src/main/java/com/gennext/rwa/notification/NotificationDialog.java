package com.gennext.rwa.notification;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.rwa.R;
import com.gennext.rwa.util.AnimationClass;

public class NotificationDialog extends DialogFragment {
    private AlertDialog dialog = null;
    private String category;
    private LinearLayout RLView;
    private AnimationClass anim;

    public interface NotificationDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog,String field);
    }

    private String mTitle;
    private String mMessage;
    private NotificationDialogListener mListener;

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static NotificationDialog newInstance(String title, String message, String category, NotificationDialogListener listener) {
        NotificationDialog fragment = new NotificationDialog();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.mListener = listener;
        fragment.category=category;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.notification_dialog, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_noti);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_noti_title);
        TextView tvDescription = (TextView) v.findViewById(R.id.tv_noti_detail);

        tvTitle.setText(mTitle);
        tvDescription.setText(mMessage);
        button1.setText("Ok");
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if(mListener != null) {
                    mListener.onDialogPositiveClick(NotificationDialog.this,category);
                }
            }
        });
        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
