package com.gennext.rwa.util;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import java.util.List;

/**
 * Created by Admin on 3/8/2018.
 *
 * How to use
 */
//        if (taskReq != null) {
//            taskReq.onAttach(context);
//        }
//    private void updateUi() {
//        taskReq = ApiRequest.newInstance(getActivity(),getFragmentManager(), new ApiRequest.Listener() {
//            @Override
//            public void onPreExecute() {
//                progressDialog = PopupProgress.newInstance(getContext()).show();
//            }
//
//            @Override
//            public void onResponse(String response) {
//                progressDialog.dismiss();
//                ProfileModel result = JsonParser.loadProfile(response);
//                if (result.getOutput().equals(Const.SUCCESS)) {
//                    updateProfile(result);
//                } else if (result.getOutput().equals(Const.FAILURE)) {
//                    PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
//                            .show(getFragmentManager(), "popupAlert");
//                }else{
//                    taskReq.showErrorAlertBox(result.getOutput(),result.getOutputMsg());
//                }
//            }
//
//            @Override
//            public void onRetryClick() {
//                progressDialog.dismiss();
//                updateUi();
//            }
//        });
//        String userId = AppUser.getUserId(getContext());
//        taskReq.execute(ApiRequest.POST, AppSettings.GET_PROFILE, RequestBuilder.DefaultUser(userId));
//
//    }

public class ApiRequest {

    private Listener mListener;
    private Context context;
    private String url;
    private int method;
    public static final int GET=1,POST=2;
    private List<KeyValuePair> params;
    private FragmentManager fragmentManager;


    public interface Listener {
        void onPreExecute();
        void onResponse(String response);
        void onRetryClick();
    }

    public static ApiRequest newInstance(Activity context, FragmentManager fragmentManager, Listener listener) {
        ApiRequest fragment = new ApiRequest();
        fragment.context = context;
        fragment.mListener = listener;
        fragment.fragmentManager = fragmentManager;
        return fragment;
    }


    public void execute(int method, String url, List<KeyValuePair> params) {
        this.method = method;
        this.url = url;
        this.params = params;
        new ApiRequestTask().execute(url);
    }

    public void execute(String url) {
        this.method = GET;
        this.url = url;
        new ApiRequestTask().execute(url);
    }

    public void onAttach(Context context) {
        this.context = context;
    }

    public void onDetach() {
        this.context = null;
    }


    private class ApiRequestTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mListener.onPreExecute();
        }

        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            if(method==GET){
                if(params==null){
                    return ApiCall.GET(url[0]);
                }else{
                    return ApiCall.GET(url[0],params);
                }
            }else{
                return ApiCall.POST(url[0],params);
            }
        }

        @Override
        protected void onPostExecute(final String response) {
            // TODO Auto-generated method stub
            super.onPostExecute(response);
            if (context != null) {
                if (response != null) {
                    if (response.contains(ApiCall.IO_EXCEPTION)) {
                        showErrorAlertBox("Server Error" ,response);
                    } else {
                        mListener.onResponse(response);
                    }
                }
            }
        }

    }

    public void showErrorAlertBox(String title, String message) {
        ApiCallError.newInstance(title, message, new ApiCallError.ErrorListener() {
            @Override
            public void onErrorRetryClick(DialogFragment dialog) {
                mListener.onRetryClick();
            }

            @Override
            public void onErrorCancelClick(DialogFragment dialog) {

            }
        })
                .show(fragmentManager, "apiCallError");
    }

}
