//package com.gennext.rwa.util;
//
//import android.app.Activity;
//import android.os.AsyncTask;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ProgressBar;
//
//import org.apache.http.HttpEntity;
//import org.apache.http.HttpResponse;
//import org.apache.http.client.ClientProtocolException;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.client.methods.HttpPut;
//import org.apache.http.client.utils.URLEncodedUtils;
//import org.apache.http.impl.client.DefaultHttpClient;
//import com.gennext.rwa.util.internet.BasicNameValuePair;
//import org.apache.http.params.BasicHttpParams;
//import org.apache.http.params.HttpConnectionParams;
//import org.apache.http.params.HttpParams;
//import org.apache.http.util.EntityUtils;
//
//import java.io.IOException;
//import java.util.List;
//
//;
//
//public class AsyncRequest extends AsyncTask<String, Void, String> {
//
//	Activity activity;
//	static String response = null;
//	public final static int GET = 1;
//	public final static int PUT = 2;
//	public final static int POST = 3;
//	private List<BasicNameValuePair> params;
//	private int method;
//	OnTaskComplete caller;
//	ProgressBar pBar;
//	Button btn;
//
//	public AsyncRequest(Activity a, int method) {
//		this.activity = a;
//		this.method = method;
//		this.caller=(OnTaskComplete) a;
//	}
//
//	public AsyncRequest(Activity a, ProgressBar pBar, Button btn, int method, List<BasicNameValuePair> params) {
//		this.activity = a;
//		this.method = method;
//		this.params = params;
//		this.pBar= pBar;
//		this.btn=btn;
//		this.caller=(OnTaskComplete)a;
//	}
//
//	public void onAttach(Activity activity) {
//		this.activity = activity;
//	}
//
//	public void onDetach() {
//		activity = null;
//	}
//
//	// Interface to be implemented by calling activity
//	public interface OnTaskComplete {
//		public void asyncResponseServer(String result);
//	}
//
//	@Override
//	protected void onPreExecute() {
//		L.m("AsyncRequest_onPreExecute"+ "executed");
//		if(pBar!=null && btn!=null){
//			pBar.setVisibility(View.VISIBLE);
//			btn.setVisibility(View.GONE);
//		}
//		if(pBar!=null){
//			pBar.setVisibility(View.VISIBLE);
//		}
//	}
//
//	@Override
//	protected String doInBackground(String... urls) {
//		L.m("AsyncRequest_doInBackground"+ "execute");
//
//		String result = "";
//		try {
//
//			// timeout method start
//			HttpParams httpParameters = new BasicHttpParams();
//			int timeoutConnection = 10000;
//			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
//			// Set the default socket timeout (SO_TIMEOUT)
//			// in milliseconds which is the timeout for waiting for data.
//			int timeoutSocket = 15000;
//			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
//			// timeout method end
//
//			// create HttpClient
//			HttpClient httpclient = new DefaultHttpClient(httpParameters);
//			// HttpGet get=new HttpGet(url.toString());
//			HttpEntity httpEntity = null;
//			HttpResponse httpResponse = null;
//			// Checking http request method type
//			if (method == POST) {
//				HttpPost httpPost = new HttpPost(urls[0]);
//				// adding post params
//				if (params != null) {
//					httpPost.setEntity(new UrlEncodedFormEntity(params));
//				}
//				httpResponse = httpclient.execute(httpPost);
//			} else if (method == GET) {
//				// appending params to url
//				if (params != null) {
//					String paramString = URLEncodedUtils.format(params, "utf-8");
//					urls[0] += "?" + paramString;
//				}
//				HttpGet httpGet = new HttpGet(urls[0]);
//				httpResponse = httpclient.execute(httpGet);
//			} else if (method == PUT) {
//				HttpPut httpPut = new HttpPut(urls[0]);
//				// adding put params
//				if (params != null) {
//					httpPut.setEntity(new UrlEncodedFormEntity(params));
//				}
//				httpResponse = httpclient.execute(httpPut);
//			}
//			/*
//			 * give the status code for this request. These are grouped as
//			 * follows: 1: 1xx-Informational(new in HTTP/1.1) 2: 2xx-Success 3:
//			 * 3xx-Redirection 4: 4xx-Client Error 5: 5xx-Server Error
//			 */
//
//			/******************************/
//			httpEntity = httpResponse.getEntity();
//			int statusCode = httpResponse.getStatusLine().getStatusCode();
//			if (statusCode == 200) {
//				// Server response
//				result = EntityUtils.toString(httpEntity);
//			} else {
//				result = statusCode + " response: " + EntityUtils.toString(httpEntity);
//			}
//			/******************************/
//		} catch (ClientProtocolException e) {
//			L.m("GETURL"+e.toString());
//			result = "Server Exception " + result;
//		} catch (IOException e) {
//			L.m("GETURL"+ e.toString());
//			result = "Input/Output Exception " + result;
//		}
//
//		return result;
//
//	}
//
//	// onPostExecute displays the results of the AsyncTask.
//	@Override
//	protected void onPostExecute(String result) {
//		L.m("AsyncRequest_post"+ "executed");
//		if(activity!=null){
//			if(pBar!=null && btn!=null){
//				pBar.setVisibility(View.GONE);
//				btn.setVisibility(View.VISIBLE);
//			}
//			if(pBar!=null){
//				pBar.setVisibility(View.GONE);
//			}
//			caller.asyncResponseServer(result);
//
//		}
//
//	}
//
//}
