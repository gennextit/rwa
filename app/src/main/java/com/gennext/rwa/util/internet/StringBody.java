package com.gennext.rwa.util.internet;

/**
 * Created by Admin on 2/6/2018.
 */

public class StringBody {

    private final String text;

    public StringBody(String text){
        this.text=text;
    }

    public StringBody(String text, String type) {
        this.text=text;
    }

    public String getText() {
        return text;
    }
}
