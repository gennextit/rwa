package com.gennext.rwa.util;

/**
 * Created by Abhijit on 09-Sep-16.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.gennext.rwa.fragment.circular.Circular;

import org.json.JSONArray;
import org.json.JSONObject;

public class RWA {

    public static final String COMMON = "rwa";

    public static final String MY_PROFILE = "myProfileArr" + COMMON;
    public static final String ADDRESS = "address" + COMMON;
    public static final String IMPORTANT_CONTACT_NUMBER = "inc" + COMMON;
    public static final String SERVICE_PROVIDER = "sp" + COMMON;
    public static final String BANNER = "Banner" + COMMON;
    public static final String DAILY_NEEDS = "dn" + COMMON;
    public static final String GALARRAY = "galArray" + COMMON;
    public static final String NOTIFICATIONOBJ = "notificationObj" + COMMON;
    public static final String TICKETRAISED = "ticketRaised" + COMMON;
    public static final String PURCHASEDITEM = "purchasedItem" + COMMON;
    public static final String CIRCULARARRAY = "circularArray" + COMMON;
    public static final String SOCIETY = "society" + COMMON;
    public static final String VEHICLE = "vehiclelist" + COMMON;
    public static final String DIRECTORY = "rwadirectory" + COMMON;

    public static final String ABOUTUS_JSON = "aboutUs" + COMMON;
    public static final String ABOUT_APP = "aboutApp" + COMMON;
    public static final String ISSUE_CATEGORIES = "issueCategories" + COMMON;



    public static void setRWAData(Context context, JSONObject aboutUs, String aboutApp, JSONArray myProfileArr, JSONArray vehicleArr, JSONObject addressObj,
                                  JSONArray inc, JSONArray sp, JSONArray banner, JSONArray ss,
                                  JSONArray galArray, JSONObject notificationObj, JSONArray ticketRaised
            , JSONArray purchasedItem, JSONArray circularArray, JSONArray issueCategories) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        if(aboutUs!=null)
            editor.putString(ABOUTUS_JSON, aboutUs.toString());
        if(aboutApp!=null)
            editor.putString(ABOUT_APP, aboutApp.toString());
        if(myProfileArr!=null)
            editor.putString(MY_PROFILE, myProfileArr.toString());
        if(vehicleArr!=null && !vehicleArr.equals(""))
            editor.putString(VEHICLE, vehicleArr.toString());
        if(addressObj!=null)
            editor.putString(ADDRESS, addressObj.toString());
        if(inc!=null)
            editor.putString(IMPORTANT_CONTACT_NUMBER, inc.toString());
        if(sp!=null)
            editor.putString(SERVICE_PROVIDER, sp.toString());
        if(banner!=null)
            editor.putString(BANNER, banner.toString());
        if(ss!=null)
            editor.putString(DAILY_NEEDS, ss.toString());
        if(galArray!=null)
            editor.putString(GALARRAY, galArray.toString());
        if(notificationObj!=null)
            editor.putString(NOTIFICATIONOBJ, notificationObj.toString());
        if(ticketRaised!=null)
            editor.putString(TICKETRAISED, ticketRaised.toString());
        if(purchasedItem!=null)
            editor.putString(PURCHASEDITEM, purchasedItem.toString());
        if(circularArray!=null)
            editor.putString(CIRCULARARRAY, circularArray.toString());
        if(issueCategories!=null)
            editor.putString(ISSUE_CATEGORIES, issueCategories.toString());
        editor.commit();

    }

    public static String getSOCIETY(Context context) {
        return LoadPref(context, SOCIETY);
    }
    public static String getABOUTUS_JSON(Context context) {
        return LoadPref(context, ABOUTUS_JSON);
    }
    public static String getABOUTAPP(Context context) {
        return LoadPref(context, ABOUT_APP);
    }
    public static void setSociety(Context context, String society) {
        Utility.SavePref(context, SOCIETY, society);
    }

    public static String getDIRECTORY(Context context) {
        return LoadPref(context, DIRECTORY);
    }
    public static void setDIRECTORY(Context context, String directory) {
        Utility.SavePref(context, DIRECTORY, directory);
    }

    public static String getVEHICLE(Context context) {
        return LoadPref(context, VEHICLE);
    }
    public static void setVEHICLE(Context context, String vehicleList) {
        Utility.SavePref(context, VEHICLE, vehicleList);
    }


    public static String LoadPref(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String data = sharedPreferences.getString(key, "");
        return data;
    }

    public static String getImportantContactNumber(Context context) {
        return LoadPref(context, IMPORTANT_CONTACT_NUMBER);
    }

    public static String getServiceProvider(Context context) {
        return LoadPref(context, SERVICE_PROVIDER);
    }
    public static String getIssueCategories(Context context) {
        return LoadPref(context, ISSUE_CATEGORIES);
    }

    public static String getBanner(Context context) {
        return LoadPref(context, BANNER);
    }
    public static String getCircularInfo(Context context) {
        return LoadPref(context, CIRCULARARRAY);
    }
    public static String getDailyNeeds(Context context) {
        return LoadPref(context, DAILY_NEEDS);
    }

    public static String getAddress(Context context) {
        return LoadPref(context, ADDRESS);
    }

    public static String getPurchasedItem(Context context) {
        return LoadPref(context,  PURCHASEDITEM);
    }

    public static String getTicketRaised(Context context) {
        return LoadPref(context,  TICKETRAISED);
    }

    public static String getNotification(Context context) {
        return LoadPref(context,  NOTIFICATIONOBJ);
    }

    public static String getGalarryInfo(Context context) {
        return LoadPref(context,  GALARRAY);
    }
}
