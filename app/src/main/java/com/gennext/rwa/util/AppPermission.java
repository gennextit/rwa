package com.gennext.rwa.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;

/**
 * Created by Abhijit on 29-Jul-16.
 */
public class AppPermission {

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkNotificationPermission(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>=android.os.Build.VERSION_CODES.JELLY_BEAN)
        {
            return true;
        } else {
            return false;
        }
    }


    /*public static boolean isStoragePermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                L.m("Permission is granted");
                return true;
            } else {

                L.m("Permission is revoked");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            L.m("Permission is granted");
            return true;
        }


    }*/

}
