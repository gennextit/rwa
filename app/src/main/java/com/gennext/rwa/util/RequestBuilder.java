package com.gennext.rwa.util;


import com.gennext.rwa.util.internet.BasicNameValuePair;

import java.util.List;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class RequestBuilder {

    public static RequestBody ErrorReport(String report) {
        return new FormBody.Builder()
                .addEncoded("reportLog", report)
                .build();
    }

    public static RequestBody Default(String consultantId) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("consultantId", consultantId)
                .build();
        return formBody;
    }


    public static RequestBody getParams(List<BasicNameValuePair> params) {
        FormBody.Builder builder = new FormBody.Builder();
        for (BasicNameValuePair model: params){
            builder.add(model.getKey(), model.getValue());
        }
        RequestBody formBody = builder.build();
        return formBody;
    }
}
