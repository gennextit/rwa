package com.gennext.rwa.util;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.gennext.rwa.model.CheckoutModel;

import java.util.ArrayList;

/**
 * Created by Abhijit on 15-Oct-16.
 */

public class RWAdb {
    private int id = 0;
    SQLiteDatabase db;
    Context act;
    ArrayList<CheckoutModel> tableList;

    public RWAdb(Context activity) {
        this.act = activity;
        CreateDataBase();
    }

    public void CreateDataBase() {
        db = act.openOrCreateDatabase("RWADB", Context.MODE_PRIVATE, null);
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS rwa(id INTEGER,myProfileArr VARCHAR,address VARCHAR,incNumber VARCHAR,sp VARCHAR,Banner VARCHAR,dn VARCHAR,galArray VARCHAR,notificationObj VARCHAR,ticketRaised VARCHAR,purchasedItem VARCHAR);");

        //id ,myProfileArr ,address ,incNumber ,sp ,Banner ,dn ,galArray ,notificationObj ,ticketRaised ,purchasedItem
        // db.execSQL(
        // "CREATE TABLE IF NOT EXISTS user(name VARCHAR,mobile VARCHAR,email
        // VARCHAR,password VARCHAR,state VARCHAR,city VARCHAR,locality
        // VARCHAR,street VARCHAR,houseNo VARCHAR,pinCode VARCHAR);");
    }

    public void DropTable() {
        db.execSQL("DROP TABLE IF EXISTS rwa");
    }

    public void UpdateUserData(String myProfileArr, String address, String incNumber, String sp, String Banner
            , String dn, String galArray, String notificationObj, String ticketRaised, String purchasedItem) {
        // Searching token number
        Cursor c = db.rawQuery("SELECT * FROM rwa WHERE id='" + id + "'", null);
        if (c.moveToFirst()) {
            // Modifying record if found
            db.execSQL("UPDATE rwa SET myProfileArr='" + myProfileArr + "',address='" + address +
                    "',incNumber='" + incNumber + "',sp='" + sp + "',Banner='" + Banner + "',dn='" + dn +
                    "',galArray='" + galArray +"',notificationObj='" + notificationObj +"',ticketRaised='" + ticketRaised +
                    "',purchasedItem='" + purchasedItem +"' WHERE id='" + id + "'");
            L.m("Success Record Updated");
        } else {
            db.execSQL("INSERT INTO rwa ('id','myProfileArr','address','incNumber','sp','Banner','dn','galArray','notificationObj','ticketRaised','purchasedItem') " +
                    "VALUES('" + id + "','" + myProfileArr + "','" + address + "','" + incNumber + "','" + sp + "','" + Banner + "','" + dn + "','" + galArray + "','" + notificationObj +"','" + ticketRaised +"','" + purchasedItem + "');");
            L.m("Success Record Inserted");
        }
    }

    public String getMyProfileArray() {
        String res;
        // Retrieving all records
        Cursor c = db.rawQuery("SELECT myProfileArr FROM rwa WHERE id='" + id + "'", null);
        // Checking if no records found
        if (c.getCount() == 0) {
            L.m("Error No records found");
            return null;
        }
        // Appending records to a string buffer
//        StringBuffer buffer = new StringBuffer();
//        while (c.moveToNext()) {
//            CheckoutModel ob = new CheckoutModel();
//            ob.setStore_category_id(c.getString(0));
//            ob.setSubcat_id(c.getString(1));
//            ob.setU_subcat_id(c.getString(2));
//            ob.setU_subcat_name(c.getString(3));
//            ob.setU_subcat_price(c.getString(4));
//            ob.setU_subcat_quantity(c.getString(5));
//            ob.setU_subcat_image(c.getString(6));
//            tableList.add(ob);
////			 buffer.append("tokenId: " + c.getString(0) + "\n");
////			 buffer.append("tokenName: " + c.getString(1) + "\n");
////			 buffer.append("category: " + c.getString(2) + "\n");
////			 buffer.append("remarks: " + c.getString(3) + "\n");
////			 buffer.append("date: " + c.getString(4) + "\n");
////			 buffer.append("time: " + c.getString(5) + "\n\n");
//        }
////		 Displaying all records
////		 L.m("Student Details : "+ buffer.toString());
        res=c.getString(0);
        return res;
    }
}
