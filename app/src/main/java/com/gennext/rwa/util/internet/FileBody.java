package com.gennext.rwa.util.internet;

import java.io.File;

/**
 * Created by Admin on 2/6/2018.
 */

public class FileBody {
    private final File file;

    public FileBody(File sourceFile) {
        this.file=sourceFile;
    }

    public File getFile() {
        return file;
    }
}
