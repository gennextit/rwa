package com.gennext.rwa.util;

import android.app.Activity;
import android.content.Context;

import com.gennext.rwa.R;
import com.gennext.rwa.model.AddressModel;
import com.gennext.rwa.model.CircularModel;
import com.gennext.rwa.model.DailyNeedsModel;
import com.gennext.rwa.model.DirectoryModel;
import com.gennext.rwa.model.GalleryModel;
import com.gennext.rwa.model.ImpContactModel;
import com.gennext.rwa.model.JsonModel;
import com.gennext.rwa.model.OrderHistoryModel;
import com.gennext.rwa.model.RaiseTicketModel;
import com.gennext.rwa.model.ResponseModel;
import com.gennext.rwa.model.ServiceProviderModel;
import com.gennext.rwa.model.SliderModel;
import com.gennext.rwa.model.TicketHistoryModel;
import com.gennext.rwa.model.VehicleModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Rishabh on 7/29/2016.
 */
public class JsonParser {

    //    Activity activity;
    public static String ERRORMESSAGE = "Not Available";

    public JsonParser() {
//        this.activity = activity;
    }

    public JsonModel SaveTempData(Context act, String response) {
        JsonModel jsonModel = new JsonModel();
        jsonModel.setOutput("NA");
        jsonModel.setOutputMsg("No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    JSONObject msgObj = mainObject.optJSONObject("message");
                    jsonModel.setOutput("success");
                    JSONArray myProfileArr = msgObj.optJSONArray("myProfile");
                    JSONObject aboutUs = msgObj.optJSONObject("aboutUs");
                    String aboutApp=aboutUs.optString("about");
                    JSONArray vehicleArr = msgObj.optJSONArray("vehicleInfo");
                    JSONObject addressObj = msgObj.optJSONObject("address");
                    JSONArray ICNObj = msgObj.optJSONArray("importantContactNumber");
                    JSONArray SPObj = msgObj.optJSONArray("serviceProvider");
                    JSONArray BannerObj = msgObj.optJSONArray("banner");
                    JSONArray circularArray = msgObj.optJSONArray("circular");
                    JSONArray DNObj = msgObj.optJSONArray("dailyNeeds");
                    JSONArray galArray = msgObj.optJSONArray("gallery");
                    JSONObject notificationObj = msgObj.optJSONObject("notification");
                    JSONArray ticketRaised = msgObj.optJSONArray("ticketRaised");
                    JSONArray purchasedItem = msgObj.optJSONArray("purchasedItem");
                    JSONArray issueCategories = msgObj.optJSONArray("issueCategories");

                    if (myProfileArr != null) {
                        JSONObject profilObj = myProfileArr.getJSONObject(0);
                        AppUser.setRWAData(act, profilObj.optString("registrationId"), profilObj.optString("name")
                                , profilObj.optString("mobile"), profilObj.optString("emailAddress"), profilObj.optString("image")
                                , profilObj.optString("houseNumber"), profilObj.optString("floor"), profilObj.optString("society")
                                , profilObj.optString("address"), profilObj.optString("accountType"));
                    }

                    RWA.setRWAData(act,aboutUs,aboutApp, myProfileArr,vehicleArr, addressObj, ICNObj, SPObj, BannerObj, DNObj, galArray, notificationObj
                            , ticketRaised, purchasedItem, circularArray,issueCategories);
//                    RWAdb rwAdb=new RWAdb(act);
//                    rwAdb.UpdateUserData(myProfileArr.toString(),addressObj.toString(),ICNObj.toString(), SPObj.toString(), BannerObj.toString(), DNObj.toString(),galArray.toString(),notificationObj
//                            .toString(),ticketRaised.toString(),purchasedItem.toString());

                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("success");
                    jsonModel.setOutputMsg("message");
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString() + response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }


//    public String setSchoolInfo(Context context, String response) {
//
//        if (response.contains("{")) {
//            try {
//                JSONObject obj = new JSONObject(response);
//                response = "success";
//                School.setSchoolData(context, obj.optString("name"), obj.optString("logo"), obj.optString("address"), obj.optString("eMail"),
//                        obj.optString("openingTime"), obj.optString("closingTime"), obj.optString("website"),
//                        obj.optString("facebookUrl"), obj.optString("twitterUrl"),
//                        obj.optString("importantContactNumbers"), obj.optString("aboutUs"), obj.optString("directions")
//                        , obj.optString("financialYear"));
//
//
//            } catch (JSONException e) {
//                ERRORMESSAGE = e.toString();
//                return null;
//            }
//        } else {
//            ERRORMESSAGE = response;
//            return null;
//        }
//
//        return response;
//    }


    public ServiceProviderModel getServiceProviderInfo(String response) {

        ServiceProviderModel serviceProviderModel = new ServiceProviderModel();
        serviceProviderModel.setOutput("failure");
        serviceProviderModel.setList(new ArrayList<ServiceProviderModel>());
        if (response.contains("[")) {
            try {
                JSONArray spArray = new JSONArray(response);
                for (int i = 0; i < spArray.length(); i++) {
                    JSONObject spObj = spArray.getJSONObject(i);
                    serviceProviderModel.setOutput("success");
                    ServiceProviderModel model = new ServiceProviderModel();
                    model.setCategoryId(spObj.optString("categoryId"));
                    model.setCategory(spObj.optString("category"));
                    model.setChildList(new ArrayList<ServiceProviderModel>());

                    JSONArray detailArray = spObj.getJSONArray("details");
                    for (int j = 0; j < detailArray.length(); j++) {
                        JSONObject subOption = detailArray.getJSONObject(j);
                        ServiceProviderModel child = new ServiceProviderModel();
                        child.setServiceProviderId(subOption.optString("serviceProviderId"));
                        child.setName(subOption.optString("name"));
                        child.setImage(subOption.optString("image"));
                        child.setDescription(subOption.optString("description").trim());
                        child.setDesignation(subOption.optString("designation"));
                        child.setMobile(subOption.optString("mobile"));
                        child.setRating(subOption.optString("rating"));


                        JSONArray reviewArray = subOption.optJSONArray("review");
                        if(reviewArray!=null) {
                            child.setChildReviewList(new ArrayList<ServiceProviderModel>());
                            for (int k = 0; k < reviewArray.length(); k++) {
                                JSONObject reviewObj = reviewArray.getJSONObject(k);
                                ServiceProviderModel reviewchild = new ServiceProviderModel();
                                reviewchild.setFeedback(reviewObj.optString("feedback"));
                                reviewchild.setDate(DateTimeUtility.convertDateMMM(reviewObj.optString("date")));
                                reviewchild.setRating(reviewObj.optString("rating"));
                                child.getChildReviewList().add(reviewchild);
                            }
                        }
                            //Add Child class object to parent class object
                        model.getChildList().add(child);
                    }


                    serviceProviderModel.getList().add(model);

                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                serviceProviderModel.setOutput("failure");
                serviceProviderModel.setOutputMsg(e.toString()+response);

            }
        } else {
            ERRORMESSAGE = response;
            serviceProviderModel.setOutput("failure");
            serviceProviderModel.setOutputMsg(response);
        }
        return serviceProviderModel;
    }

    public ArrayList<ImpContactModel> getImpContactInfo(String response) {

        ArrayList<ImpContactModel> rwaList = new ArrayList<>();
        if (response.contains("[")) {
            try {
                JSONArray spArray = new JSONArray(response);
                for (int i = 0; i < spArray.length(); i++) {
                    JSONObject spObj = spArray.getJSONObject(i);
                    response = "success";
                    ImpContactModel model = new ImpContactModel();
                    model.setCategory(spObj.optString("category"));
                    model.setChildList(new ArrayList<ImpContactModel>());

                    JSONArray detailArray = spObj.getJSONArray("details");
                    for (int j = 0; j < detailArray.length(); j++) {
                        JSONObject subOption = detailArray.getJSONObject(j);
                        ImpContactModel child = new ImpContactModel();
                        child.setName(subOption.optString("name"));
                        child.setImage(subOption.optString("image"));
                        child.setDesignation(subOption.optString("designation"));
                        child.setMobile(subOption.optString("mobile"));
                        child.setDetail(subOption.optString("detail"));
                        child.setAvailableFrom(subOption.optString("availableFrom"));

                        //Add Child class object to parent class object
                        model.getChildList().add(child);
                    }


                    rwaList.add(model);

                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }

        if (response.equals("success")) {
            return rwaList;
        } else {
            ERRORMESSAGE = "NA";
            return null;
        }
    }


    public ArrayList<String> getEventCalenderInfo(String response) {
        ArrayList<String> eventList = new ArrayList<>();
        if (response.contains("[")) {
            try {
                JSONArray eventAndSportArray = new JSONArray(response);
                for (int i = 0; i < eventAndSportArray.length(); i++) {
                    JSONObject obj = eventAndSportArray.getJSONObject(i);
                    //eventList.add(CompactFragment.convertDate(obj.optString("startDate")));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return eventList;
    }


    public DailyNeedsModel getDailyNeedsInfo(String response) {
        DailyNeedsModel dailyNeedsModel = new DailyNeedsModel();
        dailyNeedsModel.setCatList(new ArrayList<DailyNeedsModel>());
        dailyNeedsModel.setOutput("NA");

        if (response.contains("[")) {
            try {
                JSONArray dnArray = new JSONArray(response);
                for (int i = 0; i < dnArray.length(); i++) {
                    JSONObject spObj = dnArray.getJSONObject(i);
                    dailyNeedsModel.setOutput("success");
                    DailyNeedsModel model = new DailyNeedsModel();
                    String catId = spObj.optString("categoryID");
                    model.setCategoryID(catId);
                    model.setCategoryName(spObj.optString("categoryName"));
                    model.setUrl(spObj.optString("url"));
                    model.setDescription(spObj.optString("description"));

                    JSONArray subCatArray = spObj.optJSONArray("subcategory");
                    if (subCatArray != null) {
                        model.setSubCategoryList(new ArrayList<DailyNeedsModel>());
                        model.setOutputSubCat("NA");
//                        model.setSubCategoryJSON(subCatArray.toString());
                        for (int j = 0; j < subCatArray.length(); j++) {
                            JSONObject subCatObj = subCatArray.getJSONObject(j);
                            dailyNeedsModel.setOutputSubCat("success");
                            DailyNeedsModel child = new DailyNeedsModel();
                            String subCatId = subCatObj.optString("subCategoryId");
                            child.setSubCategoryId(subCatId);
                            child.setSubCategoryName(subCatObj.optString("subCategoryName"));

                            JSONArray productArray = subCatObj.optJSONArray("product");
                            if (productArray != null) {
                                child.setProductlist(new ArrayList<DailyNeedsModel>());
                                for (int k = 0; k < productArray.length(); k++) {
                                    JSONObject productObj = productArray.getJSONObject(k);
                                    DailyNeedsModel prodModel = new DailyNeedsModel();
                                    prodModel.setCategoryId(catId);
                                    prodModel.setSubCategoryId(subCatId);
                                    prodModel.setProductName(productObj.optString("productName"));
                                    prodModel.setProductID(productObj.optString("productID"));
                                    prodModel.setQuantity("0");
                                    prodModel.setProductPrice(productObj.optString("productPrice"));
                                    prodModel.setProductImage(productObj.optString("productImage"));
                                    prodModel.setProductShortDesc(productObj.optString("productShortDesc"));
                                    child.getProductlist().add(prodModel);
                                }
                            }


                            //Add Child class object to parent class object
                            model.getSubCategoryList().add(child);
                        }
                    }
                    dailyNeedsModel.getCatList().add(model);

                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return dailyNeedsModel;
    }

    public AddressModel getAddressDropDownInfo(String response) {
        AddressModel addressModel = new AddressModel();
        addressModel.setOutput("NA");
        addressModel.setList(new ArrayList<String>());
        addressModel.setAddressList(new ArrayList<AddressModel>());
        if (response.contains("[")) {
            try {
                JSONArray societyArray = new JSONArray(response);
                if (societyArray != null) {
                    for (int i = 0; i < societyArray.length(); i++) {
                        JSONObject colObj = societyArray.getJSONObject(i);
                        addressModel.setOutput("success");
                        AddressModel model=new AddressModel();
                        model.setSocietyId(colObj.optString("societyid"));
                        model.setSociety(colObj.optString("society"));
                        addressModel.getList().add(colObj.optString("society"));
                        addressModel.getAddressList().add(model);
                    }
                }

            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return addressModel;

    }

    public RaiseTicketModel getCategoryDropDownInfo(String response) {
        RaiseTicketModel raiseTicketModel = new RaiseTicketModel();
        raiseTicketModel.setOutput("NA");
        raiseTicketModel.setList(new ArrayList<String>());
        if (response.contains("[")) {
            try {
                JSONArray spArray = new JSONArray(response);
                for (int i = 0; i < spArray.length(); i++) {
                    JSONObject spObj = spArray.getJSONObject(i);
                    raiseTicketModel.setOutput("success");
                    raiseTicketModel.getList().add(spObj.optString("category"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return raiseTicketModel;
    }

    public GalleryModel getGalleryInfo(String response) {
        GalleryModel galleryModel = new GalleryModel();
        galleryModel.setOutput("failure");
        galleryModel.setEventList(new ArrayList<GalleryModel>());

        if (response.contains("[")) {
            try {
                JSONArray galleryArray = new JSONArray(response);
                for (int i = 0; i < galleryArray.length(); i++) {
                    JSONObject galleryObj = galleryArray.optJSONObject(i);
                    if (galleryObj != null) {
                        galleryModel.setOutput("success");
                        GalleryModel model = new GalleryModel();
                        model.setEventName(galleryObj.optString("eventName"));

                        JSONArray detArray = galleryObj.optJSONArray("details");
                        JSONObject detailObj = detArray.optJSONObject(0);
                        JSONArray urlArray = detailObj.optJSONArray("url");
                        if (urlArray != null) {
                            model.setEventListDetail(new ArrayList<GalleryModel>());
                            for (int j = 0; j < urlArray.length(); j++) {
                                JSONObject galleryDetailObj = urlArray.optJSONObject(j);
                                GalleryModel detailModel = new GalleryModel();
                                detailModel.setImageUrl(galleryDetailObj.optString("imageLink"));
                                model.getEventListDetail().add(detailModel);
                            }

                        }

                        galleryModel.getEventList().add(model);
                    }

                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return galleryModel;
    }

    public SliderModel getBannerInfo(String response) {
        SliderModel sliderModel = new SliderModel();
        sliderModel.setOutput("failure");
        sliderModel.setList(new ArrayList<SliderModel>());

        if (response.contains("[")) {
            try {
                JSONArray galleryArray = new JSONArray(response);
                for (int i = 0; i < galleryArray.length(); i++) {
                    JSONObject galleryObj = galleryArray.optJSONObject(i);
                    if (galleryObj != null) {
                        sliderModel.setOutput("success");
                        SliderModel model = new SliderModel();
                        model.setBannerUrl(galleryObj.optString("bannerUrl"));
                        model.setImgUrl(galleryObj.optString("image"));
                        sliderModel.getList().add(model);
                    }

                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return sliderModel;
    }

    public CircularModel getCircularInfo(Activity activity,String response) {
        CircularModel circularModel = new CircularModel();
        circularModel.setOutput("failure");
        circularModel.setOutputMsg(activity.getResources().getString(R.string.circular_msg));
        circularModel.setList(new ArrayList<CircularModel>());

        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                for (int i = 0; i < mainArray.length(); i++) {
                    JSONObject mainObj = mainArray.optJSONObject(i);
                    if (mainObj != null) {
                        circularModel.setOutput("success");
                        CircularModel model = new CircularModel();
                        model.setTitle(mainObj.optString("title"));
                        model.setDetail(mainObj.optString("detail"));
                        model.setDateOfCreation(mainObj.optString("dateOfCreation"));
                        model.setCircularUrl(mainObj.optString("circularUrl"));
                        circularModel.getList().add(model);
                    }

                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                circularModel.setOutput("failure");
                circularModel.setOutputMsg(e.toString()+response);

            }
        } else {
            ERRORMESSAGE = response;
            circularModel.setOutput("failure");
            circularModel.setOutputMsg(response);

        }
        return circularModel;
    }

    public OrderHistoryModel getOrderHistoryInfo(String response) {
        OrderHistoryModel orderHistoryModel = new OrderHistoryModel();
        orderHistoryModel.setOutput("NA");
        orderHistoryModel.setOutputMsg("No shopping detail available");
        orderHistoryModel.setList(new ArrayList<OrderHistoryModel>());

        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    JSONObject msgObj = mainObject.optJSONObject("message");
                    JSONArray purchasedItemArr = msgObj.optJSONArray("purchasedItem");
                    if (purchasedItemArr != null) {
                        for (int i = 0; i < purchasedItemArr.length(); i++) {
                            JSONObject purchaseItemObj = purchasedItemArr.optJSONObject(i);
                            if (purchaseItemObj != null) {
                                orderHistoryModel.setOutput("success");
                                OrderHistoryModel model = new OrderHistoryModel();
                                model.setOrderId(purchaseItemObj.optString("orderId"));
                                model.setOrderTrackId(purchaseItemObj.optString("orderTrackId"));
                                model.setUserId(purchaseItemObj.optString("userId"));
                                model.setOrderTotalAmount(purchaseItemObj.optString("orderTotalAmount"));
                                model.setPaymentMethod(purchaseItemObj.optString("paymentMethod"));
                                model.setOrderShipName(purchaseItemObj.optString("orderShipName"));
                                model.setOrderShipAddress(purchaseItemObj.optString("orderShipAddress"));
                                model.setOrderPhone(purchaseItemObj.optString("orderPhone"));
                                model.setOrderDate(DateTimeUtility.convertDateMMM(purchaseItemObj.optString("orderDate")));
                                model.setOrderTime(DateTimeUtility.convertTime24to12Hours(purchaseItemObj.optString("orderTime")));
                                model.setDetails(purchaseItemObj.optString("details"));
                                orderHistoryModel.getList().add(model);
                            } else {
                                orderHistoryModel.setOutputMsg("No shopping detail available");
                            }
                        }
                    } else {
                        orderHistoryModel.setOutputMsg("No shopping detail available");
                    }
                } else if (mainObject.getString("status").equals("failure")) {
                    orderHistoryModel.setOutput("failure");
                    orderHistoryModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return orderHistoryModel;
    }

    public OrderHistoryModel getOrderHistoryInfo2(String response) {
        OrderHistoryModel orderHistoryModel = new OrderHistoryModel();
        orderHistoryModel.setOutput("NA");
        orderHistoryModel.setOutputMsg("No shopping detail available");
        orderHistoryModel.setList(new ArrayList<OrderHistoryModel>());

        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    JSONArray messageArr = mainObject.optJSONArray("message");
                    if (messageArr != null) {
                        for (int i = 0; i < messageArr.length(); i++) {
                            JSONObject purchaseItemObj = messageArr.optJSONObject(i);
                            if (purchaseItemObj != null) {
                                orderHistoryModel.setOutput("success");
                                OrderHistoryModel model = new OrderHistoryModel();
                                model.setOrderId(purchaseItemObj.optString("orderId"));
                                model.setOrderTrackId(purchaseItemObj.optString("orderTrackId"));
                                model.setUserId(purchaseItemObj.optString("userId"));
                                model.setOrderTotalAmount(purchaseItemObj.optString("orderTotalAmount"));
                                model.setPaymentMethod(purchaseItemObj.optString("paymentMethod"));
                                model.setOrderShipName(purchaseItemObj.optString("orderShipName"));
                                model.setOrderShipAddress(purchaseItemObj.optString("orderShipAddress"));
                                model.setOrderPhone(purchaseItemObj.optString("orderPhone"));
                                model.setOrderDate(DateTimeUtility.convertDateMMM(purchaseItemObj.optString("orderDate")));
                                model.setOrderTime(DateTimeUtility.convertTime24to12Hours(purchaseItemObj.optString("orderTime")));
//                                model.setDetails(purchaseItemObj.optString("details"));
                                JSONArray detailArr = purchaseItemObj.optJSONArray("details");
                                if (detailArr != null) {
                                    model.setList(new ArrayList<OrderHistoryModel>());
                                    for (int j = 0; j < detailArr.length(); j++) {
                                        JSONObject detailObj = detailArr.optJSONObject(j);
                                        OrderHistoryModel detailModel = new OrderHistoryModel();
                                        detailModel.setProductName(detailObj.optString("productName"));
                                        detailModel.setDetailPrice(detailObj.optString("detailPrice"));
                                        detailModel.setImageUrl(detailObj.optString("imageUrl"));
                                        detailModel.setDetailQuantity(detailObj.optString("detailQuantity"));
                                        model.getList().add(detailModel);
                                    }
                                }
                                orderHistoryModel.getList().add(model);
                            } else {
                                orderHistoryModel.setOutputMsg("No shopping detail available");
                            }
                        }
                    } else {
                        orderHistoryModel.setOutputMsg("No shopping detail available");
                    }
                } else if (mainObject.getString("status").equals("failure")) {
                    orderHistoryModel.setOutput("failure");
                    orderHistoryModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return orderHistoryModel;
    }

    public TicketHistoryModel getTicketHistoryInfo(String response) {
        TicketHistoryModel ticketHistoryModel = new TicketHistoryModel();
        ticketHistoryModel.setOutput("NA");
        ticketHistoryModel.setOutputMsg("No ticket history available");
        ticketHistoryModel.setList(new ArrayList<TicketHistoryModel>());

        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    JSONObject msgObj = mainObject.optJSONObject("message");
                    JSONArray ticketRaisedArr = msgObj.optJSONArray("ticketRaised");
                    if (ticketRaisedArr != null) {
                        for (int i = 0; i < ticketRaisedArr.length(); i++) {
                            JSONObject ticketRaisedObj = ticketRaisedArr.optJSONObject(i);
                            if (ticketRaisedObj != null) {
                                ticketHistoryModel.setOutput("success");
                                TicketHistoryModel model = new TicketHistoryModel();
                                model.setTicketNumber(ticketRaisedObj.optString("ticketNumber"));
                                model.setReason(ticketRaisedObj.optString("reason"));
                                model.setImageUrl(ticketRaisedObj.optString("imageUrl"));
                                model.setCurrentStatus(ticketRaisedObj.optString("currentStatus"));
                                model.setAllocatedPerson(ticketRaisedObj.optString("allocatedPerson"));
                                model.setRemarks(ticketRaisedObj.optString("remarks"));
                                model.setCategory(ticketRaisedObj.optString("category"));
                                model.setTicketRaisedDate(DateTimeUtility.convertDateMMM2(ticketRaisedObj.optString("ticketRaisedDate")));
                                model.setTicketRaisedTime(DateTimeUtility.convertTime24to12Hours(ticketRaisedObj.optString("ticketRaisedTime")));
                                model.setAssignDate(ticketRaisedObj.optString("assignDate"));
                                model.setAssignTime(ticketRaisedObj.optString("assignTime"));
                                model.setProblemSolvedDate(ticketRaisedObj.optString("problemSolvedDate"));
                                model.setProblemSolvedTime(ticketRaisedObj.optString("problemSolvedTime"));
                                model.setTicketClosedDate(ticketRaisedObj.optString("ticketClosedDate"));
                                model.setTicketClosedTime(ticketRaisedObj.optString("ticketClosedTime"));
                                ticketHistoryModel.getList().add(model);
                            } else {
                                ticketHistoryModel.setOutputMsg("No shopping detail available");
                            }
                        }
                    } else {
                        ticketHistoryModel.setOutputMsg("No shopping detail available");
                    }
                } else if (mainObject.getString("status").equals("failure")) {
                    ticketHistoryModel.setOutput("failure");
                    ticketHistoryModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return ticketHistoryModel;
    }

    public TicketHistoryModel getTicketHistoryInfo2(String response) {
        TicketHistoryModel ticketHistoryModel = new TicketHistoryModel();
        ticketHistoryModel.setOutput("NA");
        ticketHistoryModel.setOutputMsg("No ticket history available");
        ticketHistoryModel.setList(new ArrayList<TicketHistoryModel>());

        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    JSONArray ticketRaisedArr = mainObject.optJSONArray("message");
                    if (ticketRaisedArr != null) {
                        for (int i = 0; i < ticketRaisedArr.length(); i++) {
                            JSONObject ticketRaisedObj = ticketRaisedArr.optJSONObject(i);
                            if (ticketRaisedObj != null) {
                                ticketHistoryModel.setOutput("success");
                                TicketHistoryModel model = new TicketHistoryModel();
                                model.setTicketNumber(ticketRaisedObj.optString("ticketNumber"));
                                model.setReason(ticketRaisedObj.optString("reason"));
                                model.setImageUrl(ticketRaisedObj.optString("imageUrl"));
                                model.setCurrentStatus(ticketRaisedObj.optString("currentStatus"));
                                model.setAllocatedPerson(ticketRaisedObj.optString("allocatedPerson"));
                                model.setRemarks(ticketRaisedObj.optString("remarks"));
                                model.setCategory(ticketRaisedObj.optString("category"));
                                model.setTicketRaisedDate(DateTimeUtility.convertDateMMM2(ticketRaisedObj.optString("ticketRaisedDate")));
                                model.setTicketRaisedTime(DateTimeUtility.convertTime24to12Hours(ticketRaisedObj.optString("ticketRaisedTime")));
                                model.setAssignDate(ticketRaisedObj.optString("assignDate"));
                                model.setAssignTime(ticketRaisedObj.optString("assignTime"));
                                model.setProblemSolvedDate(ticketRaisedObj.optString("problemSolvedDate"));
                                model.setProblemSolvedTime(ticketRaisedObj.optString("problemSolvedTime"));
                                model.setTicketClosedDate(ticketRaisedObj.optString("ticketClosedDate"));
                                model.setTicketClosedTime(ticketRaisedObj.optString("ticketClosedTime"));
                                ticketHistoryModel.getList().add(model);
                            } else {
                                ticketHistoryModel.setOutputMsg("No shopping detail available");
                            }
                        }
                    } else {
                        ticketHistoryModel.setOutputMsg("No shopping detail available");
                    }
                } else if (mainObject.getString("status").equals("failure")) {
                    ticketHistoryModel.setOutput("failure");
                    ticketHistoryModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return ticketHistoryModel;
    }

    public DirectoryModel getTempDirectoryInfo(String dirInfo) {
        DirectoryModel directoryModel = new DirectoryModel();
        directoryModel.setOutput("success");
        directoryModel.setList(new ArrayList<DirectoryModel>());
        for (int i = 0; i < 10; i++) {
            DirectoryModel model = new DirectoryModel();
            model.setData("10" + i, "User-" + i, "987654321" + i, "UP-20610" + i, "NA", "NA");
            directoryModel.getList().add(model);
        }
        DirectoryModel model2 = new DirectoryModel();
        model2.setData("9B" , "Amit Gupta" , "7840079095", "DL-001100" , "NA", "NA");
        directoryModel.getList().add(model2);
        for (int i = 10; i < 60; i++) {
            DirectoryModel model = new DirectoryModel();
            model.setData("20" + i, "MP User-" + i, "97865432" + i, "MP-20610" + i, "NA", "NA");
            directoryModel.getList().add(model);
        }
        DirectoryModel model3 = new DirectoryModel();
        model3.setData("105f" , "Abhijit Kumar Rao" , "9560039205", "N-0786" , "NA", "NA");
        directoryModel.getList().add(model3);
        for (int i = 10; i < 600; i++) {
            DirectoryModel model = new DirectoryModel();
            model.setData("30" + i, "DL User-" + i, "97865432" + i, "DL-20610" + i, "NA", "NA");
            directoryModel.getList().add(model);
        }
        return directoryModel;
    }

    public DirectoryModel getDirectoryInfo(String response) {
        DirectoryModel directoryModel = new DirectoryModel();
        directoryModel.setOutput("NA");
        directoryModel.setList(new ArrayList<DirectoryModel>());

        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    JSONArray messageArray = mainObject.optJSONArray("message");
                    for (int i = 0; i < messageArray.length(); i++) {
                        JSONObject mainObj = messageArray.optJSONObject(i);
                        if (mainObj != null) {
                            directoryModel.setOutput("success");
                            DirectoryModel model = new DirectoryModel();
                            model.setPersonName(mainObj.optString("name"));
                            model.setHouseNo(mainObj.optString("houseNumber"));
                            model.setPhoneNo(mainObj.optString("mobile"));
                            String vehicle="";
                            JSONArray vehicleArray=mainObj.optJSONArray("vehicleInfo");
                            if(vehicleArray!=null){
                                for (int j=0;j<vehicleArray.length();j++){
                                    vehicle+=vehicleArray.get(j).toString();
                                    vehicle+=",";
                                }
                            }
                            model.setVehicleNo(vehicle.substring(0,vehicle.length()-1));
                            model.setOccupation(mainObj.optString("occupation"));

                            directoryModel.getList().add(model);
                        } else {
                            directoryModel.setOutput("failure");
                        }
                    }
                }else if (mainObject.getString("status").equals("failure")) {
                    directoryModel.setOutput("failure");
                    directoryModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return directoryModel;
    }

    public ResponseModel parseFeedInfo(String response) {

        ResponseModel responseModel = new ResponseModel();
        responseModel.setOutput("NA");
        responseModel.setOutputMsg("No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    responseModel.setOutput("success");
                    responseModel.setOutputMsg(mainObject.getString("message"));
                } else if (mainObject.getString("status").equals("failure")) {
                    responseModel.setOutput("failure");
                    responseModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return responseModel;
    }

    public VehicleModel parseVehicleData(String response) {
        VehicleModel vehicleModel = new VehicleModel();
        vehicleModel.setOutput("NA");
        vehicleModel.setOutputMsg("No msg available");
        vehicleModel.setList(new ArrayList<VehicleModel>());
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                if(mainArray!=null) {
                    for (int i = 0; i < mainArray.length(); i++) {
                        JSONObject msgObj = mainArray.optJSONObject(i);
                        vehicleModel.setOutput("success");
                        VehicleModel model=new VehicleModel();
                        model.setVehicleId(msgObj.optString("vehicleId"));
                        model.setVehicleNo(msgObj.optString("vehicleNumber"));
                        model.setVehicleType(msgObj.optString("vehicleType"));
                        model.setVehicleModel(msgObj.optString("vehicleModel"));
                        vehicleModel.getList().add(model);
                    }
                }else {
                    vehicleModel.setOutput("failure");
                    vehicleModel.setOutputMsg("Empty vehicle list");
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return vehicleModel;
    }

    public VehicleModel parseVehicleDeleteJson(Activity activity,String response) {
        VehicleModel vehicleModel = new VehicleModel();
        vehicleModel.setOutput("NA");
        vehicleModel.setOutputMsg("No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    vehicleModel.setOutput("success");
                    JSONArray msgArray=mainObject.optJSONArray("message");
                    if(msgArray!=null) {
                        if(activity!=null) RWA.setVEHICLE(activity,msgArray.toString());
                    }
                } else if (mainObject.getString("status").equals("failure")) {
                    vehicleModel.setOutput("failure");
                    vehicleModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return vehicleModel;
    }

    public JsonModel parseLoginPassword(String response) {
        JsonModel jsonModel = new JsonModel();
        jsonModel.setOutput("NA");
        jsonModel.setOutputMsg("No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    jsonModel.setOutput("success");
                    JSONArray message = mainObject.getJSONArray("message");
                    JSONObject msgObj = message.getJSONObject(0);
                    jsonModel.setOutputMsg(msgObj.optString("registrationId"));
                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("failure");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }

    public JsonModel parseRegJson(String response) {
        JsonModel jsonModel = new JsonModel();
        jsonModel.setOutput("NA");
        jsonModel.setOutputMsg("No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    jsonModel.setOutput("success");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("failure");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }

    public JsonModel parseSocietyListJson(Activity activity, String response) {
        JsonModel jsonModel = new JsonModel();
        jsonModel.setOutput("NA");
        jsonModel.setOutputMsg("No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    jsonModel.setOutput("success");
                    JSONArray msgArray=mainObject.optJSONArray("message");
                    if(msgArray!=null){
                        if(activity!=null){
                            RWA.setSociety(activity,msgArray.toString());
                        }
                    }
                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("failure");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }

    public JsonModel parseVerifyOtpJson(Activity activity, String response) {
        JsonModel jsonModel = new JsonModel();
        jsonModel.setOutput("NA");
        jsonModel.setOutputMsg("No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    jsonModel.setOutput("success");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("failure");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }

    public JsonModel parseUpdateProfileJson(String response) {
        JsonModel jsonModel = new JsonModel();
        jsonModel.setOutput("NA");
        jsonModel.setOutputMsg("No msg available");
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    jsonModel.setOutput("success");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                } else if (mainObject.getString("status").equals("failure")) {
                    jsonModel.setOutput("failure");
                    jsonModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }

    public VehicleModel parseVehicleInsertUpdate(Activity activity, String response) {
        VehicleModel vehicleModel = new VehicleModel();
        vehicleModel.setOutput("NA");
        vehicleModel.setOutputMsg("No msg available");
        vehicleModel.setList(new ArrayList<VehicleModel>());
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                JSONObject mainObject = mainArray.getJSONObject(0);
                if (mainObject.getString("status").equals("success")) {
                    vehicleModel.setOutput("success");
                    JSONArray msgArray=mainObject.optJSONArray("message");
                    if(msgArray!=null) {
                        if(activity!=null) RWA.setVEHICLE(activity,msgArray.toString());
                    }
                } else if (mainObject.getString("status").equals("failure")) {
                    vehicleModel.setOutput("failure");
                    vehicleModel.setOutputMsg(mainObject.getString("message"));
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return vehicleModel;
    }

//    public UCatModel getSubCatList(String response) {
//        UCatModel uCatModel = new UCatModel();
//        uCatModel.setList(new ArrayList<UCatModel>());
//        uCatModel.setProductlist(new ArrayList<UCatModel>());
//        uCatModel.setOutput("NA");
//        if (response.contains("[")) {
//            try {
//                JSONArray data = new JSONArray(response);
//                for (int i = 0; i < data.length(); i++) {
//                    JSONObject obj = data.getJSONObject(i);
//                    uCatModel.setOutput("success");
//                    UCatModel model = new UCatModel();
//                    model.setSubCategoryName(obj.optString("subCategoryName"));
//                    model.setSubCategoryId(obj.optString("subCategoryId"));
//
//                    model.setProductlist(new ArrayList<UCatModel>());
//                    JSONArray productArray = obj.getJSONArray("product");
//                    for (int j = 0; j < productArray.length(); j++) {
//                        JSONObject productObj = productArray.getJSONObject(j);
//                        UCatModel prodModel = new UCatModel();
//                        prodModel.setProductName(productObj.optString("productName"));
//                        prodModel.setProductID(productObj.optString("productID"));
//                        prodModel.setQuantity("0");
//                        prodModel.setProductPrice(productObj.optString("productPrice"));
//                        prodModel.setProductImage(productObj.optString("productImage"));
//                        prodModel.setProductShortDesc(productObj.optString("productShortDesc"));
//                        model.getProductlist().add(prodModel);
//                    }
//
//                    uCatModel.getList().add(model);
//                }
//
//            } catch (JSONException e) {
//                JsonParser.ERRORMESSAGE = e.toString();
//                return null;
//            }
//        } else {
//            JsonParser.ERRORMESSAGE = response;
//            return null;
//        }
//        return uCatModel;
//    }
}
