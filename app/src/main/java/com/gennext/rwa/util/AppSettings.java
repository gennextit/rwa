package com.gennext.rwa.util;

public class AppSettings {

//	public static final String _ServerHost = "http://trip.esy.es/";

	public static final String IP_ADDRESS = "http://www.gennextit.com/";


	public static final String REPORT_SERVER_ERROR = IP_ADDRESS + "serverErrorReporting";
	// Change IP_Address with test ip or production
	public static final String WebServiceAPI = IP_ADDRESS + "rwa/index.php/";
	public static final String Registration = WebServiceAPI+"Registration/registration";
	public static final String verifyOtp = WebServiceAPI+"Registration/verifyOtp";
	public static final String getSocietyList = WebServiceAPI+"Registration/getSocietyList";

	public static final String TandC = WebServiceAPI+"Registration/tandc.php";
	public static final String LoginPass = WebServiceAPI+"Registration/loginPassword";//username,password
	public static final String Login = WebServiceAPI+"Registration/login";
	public static final String UpdateProfile = WebServiceAPI+"Registration/updateProfile";
	public static final String ticketRaise = WebServiceAPI+"Registration/ticketRaise";
	public static final String feedbackForSP = WebServiceAPI+"Registration/feedbackForSP";
	public static final String order = WebServiceAPI+"Registration/order";
	public static final String orderedList = WebServiceAPI+"Registration/orderedList";
	public static final String ticketRaisedList = WebServiceAPI+"Registration/getTicketsRaised";
	public static final String getDirectory = WebServiceAPI+"Registration/getDirectory";
	public static final String sendFeedback = WebServiceAPI+"Registration/feedbackForApp";
	public static final String vehicleInsertUpdate = WebServiceAPI+"Registration/vehicleInsertUpdate";
	public static final String vehicleDelete = WebServiceAPI+"Registration/vehicleDelete";

}
