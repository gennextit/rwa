package com.gennext.rwa.util;

import android.content.Context;
import android.content.Intent;

import com.gennext.rwa.model.UCatModel;

import java.util.ArrayList;

/**
 * Created by Abhijit on 3/5/2016.
 */
public class AppTokens {

	public static final String COMMON = "rwa";

	public static final String APP_USER = "appuser" + COMMON;


	public static final String DOWNLOAD_DIRECTORY_NAME = COMMON;

	// special character to prefix the otp. Make sure this character appears
	// only once in the sms
	public static final String SessionIntro = "SessionIntro" + COMMON;
	public static final String SessionSignup = "SessionSignup" + COMMON;
	public static final String SessionProfile = "SessionProfile" + COMMON;


	public static final String PROFILE_IMAGE_DIRECTORY_NAME = "rwa";
	public static final String FolderDirectory = "rwa";


	public static final String AppData="AppDataSabseSasta";
	public static final String UnderSubCatArray="UnderSubCatArraySabseSasta";

	public static final String StoreCatId="StoreCatIdSabseSasta";
	public static final String SubCatId="SubCatIdSabseSasta";

	public static final String Mobile="MobileSabseSasta";
	public static final String OrderConfiremd="OrderConfiremdSabseSasta";
	public static final String AddressStatus="AddressStatusSabseSasta";
	public static ArrayList<UCatModel> globalList=null;


	public static int countTotalItem=0;
	public static float sumTotalPrice=0.0f;

	public static final String SMS_ORIGIN = "-GENRWA";

}
