package com.gennext.rwa.util;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Abhijit on 25-Nov-16.
 */

public class GCMTokens {

    public static final String COMMON = "rwa";
    // Google User Register id
    public static final String GCM_ID = "gcmid" + COMMON;

//    // Google project id
//    public static final String SENDER_ID = "541906816790";
//
//    public static final String DISPLAY_MESSAGE_ACTION = "com.gennext.rwa.util.DISPLAY_MESSAGE";
//
//    public static final String EXTRA_MESSAGE = "message", EXTRA_RESULT = "result";
//    public static final String EXTRA_MESSAGE_SID = "senderId";
//    public static final String EXTRA_MESSAGE_SNAME = "senderName";
//    public static final int CHAT = 1, MESSAGE = 2, NOTIFICATION = 3;

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static final String CIRCULAR_NOTIFICATION = "circularNotification";
    public static final String ADVERTISE_NOTIFICATION = "advertiseNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

//    public static final String SHARED_PREF = "ah_firebase";


    public static void setGCM(Context context, String mobile) {
        Utility.SavePref(context, GCM_ID, mobile);
    }

    public static String getGCM(Context context) {
        return Utility.LoadPref(context, GCM_ID);
    }


//    public static void displayMessage(Context context, String message) {
//        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
//        intent.putExtra(EXTRA_MESSAGE, message);
//        context.sendBroadcast(intent);
//    }
//
//    public static void displayChatMessage(Context context,String senderId,String senderName, String message) {
//        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
//        intent.putExtra(EXTRA_RESULT, CHAT);
//        intent.putExtra(EXTRA_MESSAGE, message);
//        intent.putExtra(EXTRA_MESSAGE_SID, senderId);
//        intent.putExtra(EXTRA_MESSAGE_SNAME, senderName);
//        context.sendBroadcast(intent);
//    }
//
//    public static void displayNotificationMessage(Context context, String message) {
//        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
//        intent.putExtra(EXTRA_RESULT, NOTIFICATION);
//        intent.putExtra(EXTRA_MESSAGE, message);
//        context.sendBroadcast(intent);
//    }
//
//    public static void displayMsgMessage(Context context, String message) {
//        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
//        intent.putExtra(EXTRA_RESULT, MESSAGE);
//        intent.putExtra(EXTRA_MESSAGE, message);
//        context.sendBroadcast(intent);
//    }

}
