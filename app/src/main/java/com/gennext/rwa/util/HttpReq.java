package com.gennext.rwa.util;


import com.gennext.rwa.fragment.CompactFragment;
import com.gennext.rwa.util.internet.BasicNameValuePair;
import com.gennext.rwa.util.internet.MultipartEntity;
import com.gennext.rwa.util.internet.URLEncodedUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.client.methods.HttpPut;
//import org.apache.http.client.utils.URLEncodedUtils;

public class HttpReq {
	static String response = null;
	public final static int GET = 1;
	public final static int POST = 3;
	public final static int EXECUTE_TASK = 4;
	// We don't use namespaces
    private static String ns = null;

	public HttpReq() {

	}

	/**
	 * Making service call
	 * 
	 * @url - url to make request
	 * @method - http request method
	 */
	public String makeConnection(String url, int method) {
		return this.makeConnection(url, method, null, null, false, 0);
	}

	public String makeConnection(String url, int method, List<BasicNameValuePair> params) {
		return this.makeConnection(url, method, params, null, false, 0);
	}

	public String makeConnection(String url, int method, List<BasicNameValuePair> params, int executeTask) {
		return this.makeConnection(url, method, params, null, false, EXECUTE_TASK);
	}

	public String makeConnection(String url, int method, MultipartEntity params, int executeTask) {
		return this.makeConnection(url, method, null, params, true, executeTask);
	}

	/**
	 * Making service call
	 * 
	 * @url - url to make request
	 * @method - http request method
	 * @params - http request params
	 */
	public String makeConnection(String url, int method, List<BasicNameValuePair> params, MultipartEntity mParams,
                                 Boolean type, int executeTask) {
		String result = "";
		//return null;
		if (method == POST) {
			// adding post params
			if (params != null && type == false) {
				result= ApiCall.POST(url,RequestBuilder.getParams(params));
			} else {
				result= ApiCall.POST(url,mParams.RequestBody());
			}
		}
//		else if (method == GET) {
		else{
			if (params != null) {
				String paramString = URLEncodedUtils.format(params,"utf-8");
				url += "?" + paramString;
			}
			result= ApiCall.GET(url);
		}
		if (executeTask != EXECUTE_TASK) {
			return result;
		} else {
			return getSimpleJsonTask(result);
		}
	}

	public String getSimpleJsonTask(String response) {
		String output = null;
		if (response.contains("[")) {
			try {
				JSONArray json = new JSONArray(response);
				for (int i = 0; i < json.length(); i++) {
					JSONObject obj = json.getJSONObject(i);
					if (obj.optString("status").equals("success")) {
						output = "success";

					} else if (obj.optString("status").equals("failure")||obj.optString("status").equals("failed")) {
						output = obj.optString("message");
					}
				}

			} catch (JSONException e) {
				L.m("Json Error :" + e.toString());
				CompactFragment.ErrorMessage = e.toString() + response;
				return null;
				// return e.toString();
			}
		} else {
			L.m("Invalid JSON found : " + response);
			CompactFragment.ErrorMessage = response;
			return null;
		}

		return output;
	}
	public String[] makeSimpleJsonTask(String response) {
		String[] output = new String[2];
		if (response.contains("[")) {
			try {
				JSONArray json = new JSONArray(response);
				for (int i = 0; i < json.length(); i++) {
					JSONObject obj = json.getJSONObject(i);
					if (obj.optString("status").equals("success")) {
						output[0] = "success";
						output[1] = obj.optString("message");

					} else if (obj.optString("status").equals("failure")) {
						output[0] = "failure";
						output[1] = obj.optString("message");
					}
				}

			} catch (JSONException e) {
				L.m("Json Error :" + e.toString());
				output[0] = "Json Error";
				output[1] = e.toString() + response;
				return output;
				// return e.toString();
			}
		} else {
			L.m("Invalid JSON found : " + response);
			output[0] = "Invalid JSON found";
			output[1] = response;
			return output;
		}

		return output;
	}
	/***********************************************/

	/************** Get JSON from Assets folder ***********/
	/***********************************************/

	// Method that will parse the JSON file and will return a JSONObject
	public String parseAssetsData(InputStream data) {
		String JSONString = null;
		try {

			// open the inputStream to the file
			InputStream inputStream = data;

			int sizeOfJSONFile = inputStream.available();

			// array that will store all the data
			byte[] bytes = new byte[sizeOfJSONFile];

			// reading data into the array from the file
			inputStream.read(bytes);

			// close the input stream
			inputStream.close();

			JSONString = new String(bytes, "UTF-8");

		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		return JSONString;
	}
}