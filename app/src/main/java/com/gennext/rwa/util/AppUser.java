package com.gennext.rwa.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.json.JSONArray;

/**
 * Created by Abhijit on 13-Oct-16.
 */

public class AppUser {

    public static final String COMMON = "rwa";

    public static final String VEHICLE_NO = "vehicleno" + COMMON;
    public static final String VEHICLE_TYPE = "vehicletype" + COMMON;
    public static final String VEHICLE_MODEL = "vehiclemodel" + COMMON;

    public static final String SPOUSE_NAME = "spousename" + COMMON;
    public static final String SPOUSE_MOBILE = "spousemobile" + COMMON;
    public static final String SPOUSE_EMAIL = "spouseemail" + COMMON;
    public static final String SPOUSE_OCCUPATION = "spouseoccupation" + COMMON;

    public static final String REGISTRATION_ID = "registrationId" + COMMON;
    public static final String NAME = "name" + COMMON;
    public static final String MOBILE = "mobile" + COMMON;
    public static final String EMAIL_ADDRESS = "emailAddress" + COMMON;
    public static final String IMAGE = "image" + COMMON;
    public static final String HOUSE_NO = "houseNo" + COMMON;
    public static final String FLOOR = "floor" + COMMON;
    public static final String POCKET = "pocketOrSocoety" + COMMON;
    public static final String BUSSINESS_ADDRESS = "bussinessAddress" + COMMON;
    public static final String ACCOUNT_TYPE = "accountType" + COMMON;

    public static final String ADDRESS = "userAddress" + COMMON;

    public static final String RATING_COUNTER = "ratingcounter" + COMMON;


    public static void setRWAData(Context context, String regNo, String name, String mobile, String email
            , String image, String houseNo, String floor, String society, String businessAddr, String accType) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (regNo != null)
//            editor.putString(REGISTRATION_ID, regNo);
        if (name != null)
            editor.putString(NAME, name);
        if (mobile != null)
            editor.putString(MOBILE, mobile);
        if (email != null)
            editor.putString(EMAIL_ADDRESS, email);
        if (image != null)
            editor.putString(IMAGE, image);
        if (houseNo != null)
            editor.putString(HOUSE_NO, houseNo);
        if (floor != null)
            editor.putString(FLOOR, floor);
        if (society != null && !society.equals(""))
            editor.putString(POCKET, society);
        if (businessAddr != null)
            editor.putString(BUSSINESS_ADDRESS, businessAddr);
        if (accType != null)
            editor.putString(ACCOUNT_TYPE, accType);
        if (accType != null)
            editor.putString(ADDRESS, society + " , " + businessAddr);
        editor.apply();
    }

    public static String LoadPref(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String data = sharedPreferences.getString(key, "");
        return data;
    }

    public static void setRegistrationId(Context context, String regNo) {
        Utility.SavePref(context, REGISTRATION_ID, regNo);
    }

    public static void setRatingCounter(Context context, String counter) {
        Utility.SavePref(context, RATING_COUNTER, counter);
    }
    public static String getRatingCounter(Context context) {
        return LoadPref(context, RATING_COUNTER);
    }

    public static void setMobile(Context context, String mobile) {
        Utility.SavePref(context, MOBILE, mobile);
    }

    public static String getRegistrationId(Context context) {
        return LoadPref(context, REGISTRATION_ID);
    }

    public static String getNAME(Context context) {
        return LoadPref(context, NAME);
    }

    public static String getMOBILE(Context context) {
        return LoadPref(context, MOBILE);
    }

    public static String getEmailAddress(Context context) {
        return LoadPref(context, EMAIL_ADDRESS);
    }

    public static String getIMAGE(Context context) {
        return LoadPref(context, IMAGE);
    }


    public static String getHOUSENO(Context context) {
        return LoadPref(context, HOUSE_NO);
    }

    public static String getFLOOR(Context context) {
        return LoadPref(context, FLOOR);
    }

    public static String getPOCKET(Context context) {
        return LoadPref(context, POCKET);
    }

    public static String getBussinessAddress(Context context) {
        return LoadPref(context, BUSSINESS_ADDRESS);
    }

    public static String getAccountType(Context context) {
        return LoadPref(context, ACCOUNT_TYPE);
    }

    public static String getAddress(Context context) {
        return LoadPref(context, ADDRESS);
    }

    public static String[] getProfileData(Context context) {
        String[] data = new String[7];
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        data[0] = sharedPreferences.getString(REGISTRATION_ID, "");
        data[1] = sharedPreferences.getString(NAME, "");
        data[2] = sharedPreferences.getString(EMAIL_ADDRESS, "");
        data[3] = sharedPreferences.getString(HOUSE_NO, "");
        data[4] = sharedPreferences.getString(FLOOR, "");
        data[5] = sharedPreferences.getString(POCKET, "");
        data[6] = sharedPreferences.getString(BUSSINESS_ADDRESS, "");
        return data;
    }

    public static String[] getSpouseData(Context context) {
        String[] data = new String[4];
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        data[0] = sharedPreferences.getString(SPOUSE_NAME, "");
        data[1] = sharedPreferences.getString(SPOUSE_MOBILE, "");
        data[2] = sharedPreferences.getString(SPOUSE_EMAIL, "");
        data[3] = sharedPreferences.getString(SPOUSE_OCCUPATION, "");
        return data;
    }
    public static void setSpouseData(Context context, String spName, String spMobile, String spEmail,String spOccupation) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (spName != null)
            editor.putString(SPOUSE_NAME, spName);
        if (spMobile != null)
            editor.putString(SPOUSE_MOBILE, spMobile);
        if (spEmail != null)
            editor.putString(SPOUSE_EMAIL, spEmail);
        if (spOccupation != null)
            editor.putString(SPOUSE_OCCUPATION, spOccupation);
        editor.apply();
    }


    public static String[] getVehicleData(Context context) {
        String[] data = new String[3];
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        data[0] = sharedPreferences.getString(VEHICLE_NO, "");
        data[1] = sharedPreferences.getString(VEHICLE_TYPE, "");
        data[2] = sharedPreferences.getString(VEHICLE_MODEL, "");
        return data;
    }

    public static void setVehicleData(Context context, String vehicleNo, String vehicleType, String vehicleModel) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (vehicleNo != null)
            editor.putString(VEHICLE_NO, vehicleNo);
        if (vehicleType != null)
            editor.putString(VEHICLE_TYPE, vehicleType);
        if (vehicleModel != null)
            editor.putString(VEHICLE_MODEL, vehicleModel);
        editor.apply();
    }


}
