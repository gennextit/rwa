package com.gennext.rwa.util.internet;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Admin on 2/6/2018.
 */

public class URLEncodedUtils {

    public static String format(List<BasicNameValuePair> parameters, String encoding) {
        StringBuilder result = new StringBuilder();
        Iterator i$ = parameters.iterator();

        while(i$.hasNext()) {
            BasicNameValuePair parameter = (BasicNameValuePair)i$.next();
            String encodedKey = encode(parameter.getKey(), encoding);
            String value = parameter.getValue();
            String encodedValue = value != null?encode(value, encoding):"";
            if(result.length() > 0) {
                result.append("&");
            }

            result.append(encodedKey);
            result.append("=");
            result.append(encodedValue);
        }

        return result.toString();
    }

    private static String decode(String content, String encoding) {
        try {
            return URLDecoder.decode(content, encoding != null?encoding:"ISO-8859-1");
        } catch (UnsupportedEncodingException var3) {
            throw new IllegalArgumentException(var3);
        }
    }

    private static String encode(String content, String encoding) {
        try {
            return URLEncoder.encode(content, encoding != null?encoding:"ISO-8859-1");
        } catch (UnsupportedEncodingException var3) {
            throw new IllegalArgumentException(var3);
        }
    }
}
